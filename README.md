# Fortify Client API
This Python package:

- Fortify Software Security Center version: 21.2+
- API version: 1:21.2.2.0002
- Package version: 4.0 (update the version number using [the version file](version.txt))
- Build package: io.swagger.codegen.v3.generators.python.PythonClientCodegen

## Requirements

Python 3.9+

```sh
pip install -U -r requirements.txt
```
(you may need to run `pip` with root permission: `sudo pip install -U -r requirements.txt`)

## Installation & Usage
### pip install

If the python package is hosted on Github/GitLab, you can install directly from Github/GitLab

```sh
pip install git+https://gitalb.example.com/GIT_NAMESPACE/GIT_REPO_ID.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com/GIT_USER_ID/GIT_REPO_ID.git`)

### Call & Import

Then import the package in your project:
```python
import fortifyclientapi 
```

Or call the module CLI:
```sh
python -m fortifyclientapi.cli.main [-hv] [options] [command] [subcommand] [arguments]
```

### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)

Then import the package:
```python
import fortifyclientapi
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```sh
python -m fortifyclientapi.cli.main \
    -s "SSC_URL" \
    -t "YOUR_FORTIFY_API_KEY" \
    appversion create \
    --app "fortifyclientapi"
    --appversion "2.0" \
    -a "TestTextAPI=test1234" \
    -a "TestMultipleAPI=test1" \
    -a "TestMultipleAPI=test2" \
    -r "com.fortify.manager.BLL.processingrules.BuildProjectProcessingRule=false"
```

```python
from __future__ import print_function
import time
import fortifyclientapi.sscclientapi
import fortifyclientapi.helper
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
configuration.api_key_prefix['Authorization'] = 'FortifyToken'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# Or use the ConfigurationHelper
configuration = fortifyclientapi.helper.ConfigurationHelper().configure(
    username="USERNAME",
    password="PASSWORD",
    api_key="YOUR_FORTIFY_API_KEY",
    api_url="SSC_URL",
    # Uncomment below to setup other options
    # ssl_cert_verification=True,
    # ssl_ca_cert="/my/path/to/bundle.cert",
    # ssl_client_cert="/my/path/to/client.cert",
    # ssl_client_key="/my/path/to/client.key",
    # ssl_hostname_verification=False,
    # logging_debug=False,
    # logging_logfile="/path/to/my/logfile.log"
)

# Uncommant below to setup a User/Pass authentication type
# configuration = fortifyclientapi.helper.ConfigurationHelper().configure(
#     username="USERNAME",
#     password="PASSWORD",
#     api_url="SSC_URL",
# )
# 
# authentication_controller = AuthenticationController(configuration)
# configuration = authentication_controller.authenticate()

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AaTrainingStatusOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_aa_training_status_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AaTrainingStatusOfProjectVersionControllerApi->list_aa_training_status_of_project_version: %s\n" % e)
```

## Documentation For Authorization

### Basic

- **Type**: HTTP basic authentication with **`username`:`password`**

### FortifyToken

- **Type**: API key
- **API key parameter name**: Authorization
- **Location**: HTTP header

### CLI API Key Authorization type

All Token types are referenced into the Fortify Software Security Center Documentation

|   Command    | Subcommand |    Token type     |
| :----------: | :--------: | :---------------: |
| `appversion` |  `create`  |      CIToken      |
| `appversion` |  `update`  |      CIToken      |
| `appversion` |`getIssues` |      CIToken      |
| `appversion` |   `copy`   | UnifiedLoginToken |
| `attribute`  |  `create`  | UnifiedLoginToken |
| `attribute`  |  `update`  | UnifiedLoginToken |


## Documentation for API Endpoints

[All URIs are relative to *https://ssc.example.com/api/v1*](fortifyclientapi/sscclientapi/README.md#documentation-for-api-endpoints)


## Documentation For Models

[All models are documented here](fortifyclientapi/sscclientapi/README.md#documentation-for-models)

## Documentation For Controllers

[All controllers are documented here](fortifyclientapi/docs/controller)

## Documentation For Helpers

[All helpers are documented here](fortifyclientapi/docs/helper)

## Documentation For the Command Line Interface

[All CLI commands are documented here](fortifyclientapi/docs/cli/Commands.md)

|   Command    | Subcommand |                                              Description                                              |
| :----------: | :--------: | :---------------------------------------------------------------------------------------------------: |
| `appversion` |     -      |   Create/Update/Copy/GetIssues Project Version: command line interface for the Project Version API Endpoints    |
| `appversion` |  `create`  |                 Create: command line interface for the Project Version API Endpoints                  |
| `appversion` |  `update`  |                 Update: command line interface for the Project Version API Endpoints                  |
| `appversion` |   `copy`   |                  Copy: command line interface for the Project Version API Endpoints                   |
| `appversion` |`getIssues` |                  GetIssues: command line interface for the Project Version API Endpoints                   |
| `attribute`  |     -      | Create/Update Attribute definition: command line interface for the Attribute definition API Endpoints |
| `attribute`  |  `create`  |               Create command line interface for the Attribute definition API Endpoints                |
| `attribute`  |  `update`  |               Update command line interface for the Attribute definition API Endpoints                |


## Author
SecDojo Team

## License

See [LICENSE.TXT](LICENSE.TXT)
