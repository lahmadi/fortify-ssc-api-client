# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class AlertHistoryEntry(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'active': 'bool',
        'alert_custom_message': 'str',
        'alert_definition_name': 'str',
        'alert_message': 'str',
        'id': 'int',
        'monitored_entity_type': 'str',
        'project_and_version_label': 'str',
        'project_version_id': 'int',
        'triggered_date': 'datetime',
        'user_name': 'str'
    }

    attribute_map = {
        'active': 'active',
        'alert_custom_message': 'alertCustomMessage',
        'alert_definition_name': 'alertDefinitionName',
        'alert_message': 'alertMessage',
        'id': 'id',
        'monitored_entity_type': 'monitoredEntityType',
        'project_and_version_label': 'projectAndVersionLabel',
        'project_version_id': 'projectVersionId',
        'triggered_date': 'triggeredDate',
        'user_name': 'userName'
    }

    def __init__(self, active=None, alert_custom_message=None, alert_definition_name=None, alert_message=None, id=None, monitored_entity_type=None, project_and_version_label=None, project_version_id=None, triggered_date=None, user_name=None):  # noqa: E501
        """AlertHistoryEntry - a model defined in Swagger"""  # noqa: E501
        self._active = None
        self._alert_custom_message = None
        self._alert_definition_name = None
        self._alert_message = None
        self._id = None
        self._monitored_entity_type = None
        self._project_and_version_label = None
        self._project_version_id = None
        self._triggered_date = None
        self._user_name = None
        self.discriminator = None
        if active is not None:
            self.active = active
        if alert_custom_message is not None:
            self.alert_custom_message = alert_custom_message
        if alert_definition_name is not None:
            self.alert_definition_name = alert_definition_name
        if alert_message is not None:
            self.alert_message = alert_message
        if id is not None:
            self.id = id
        if monitored_entity_type is not None:
            self.monitored_entity_type = monitored_entity_type
        if project_and_version_label is not None:
            self.project_and_version_label = project_and_version_label
        if project_version_id is not None:
            self.project_version_id = project_version_id
        if triggered_date is not None:
            self.triggered_date = triggered_date
        if user_name is not None:
            self.user_name = user_name

    @property
    def active(self):
        """Gets the active of this AlertHistoryEntry.  # noqa: E501


        :return: The active of this AlertHistoryEntry.  # noqa: E501
        :rtype: bool
        """
        return self._active

    @active.setter
    def active(self, active):
        """Sets the active of this AlertHistoryEntry.


        :param active: The active of this AlertHistoryEntry.  # noqa: E501
        :type: bool
        """

        self._active = active

    @property
    def alert_custom_message(self):
        """Gets the alert_custom_message of this AlertHistoryEntry.  # noqa: E501


        :return: The alert_custom_message of this AlertHistoryEntry.  # noqa: E501
        :rtype: str
        """
        return self._alert_custom_message

    @alert_custom_message.setter
    def alert_custom_message(self, alert_custom_message):
        """Sets the alert_custom_message of this AlertHistoryEntry.


        :param alert_custom_message: The alert_custom_message of this AlertHistoryEntry.  # noqa: E501
        :type: str
        """

        self._alert_custom_message = alert_custom_message

    @property
    def alert_definition_name(self):
        """Gets the alert_definition_name of this AlertHistoryEntry.  # noqa: E501


        :return: The alert_definition_name of this AlertHistoryEntry.  # noqa: E501
        :rtype: str
        """
        return self._alert_definition_name

    @alert_definition_name.setter
    def alert_definition_name(self, alert_definition_name):
        """Sets the alert_definition_name of this AlertHistoryEntry.


        :param alert_definition_name: The alert_definition_name of this AlertHistoryEntry.  # noqa: E501
        :type: str
        """

        self._alert_definition_name = alert_definition_name

    @property
    def alert_message(self):
        """Gets the alert_message of this AlertHistoryEntry.  # noqa: E501


        :return: The alert_message of this AlertHistoryEntry.  # noqa: E501
        :rtype: str
        """
        return self._alert_message

    @alert_message.setter
    def alert_message(self, alert_message):
        """Sets the alert_message of this AlertHistoryEntry.


        :param alert_message: The alert_message of this AlertHistoryEntry.  # noqa: E501
        :type: str
        """

        self._alert_message = alert_message

    @property
    def id(self):
        """Gets the id of this AlertHistoryEntry.  # noqa: E501

        alert history id  # noqa: E501

        :return: The id of this AlertHistoryEntry.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this AlertHistoryEntry.

        alert history id  # noqa: E501

        :param id: The id of this AlertHistoryEntry.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def monitored_entity_type(self):
        """Gets the monitored_entity_type of this AlertHistoryEntry.  # noqa: E501


        :return: The monitored_entity_type of this AlertHistoryEntry.  # noqa: E501
        :rtype: str
        """
        return self._monitored_entity_type

    @monitored_entity_type.setter
    def monitored_entity_type(self, monitored_entity_type):
        """Sets the monitored_entity_type of this AlertHistoryEntry.


        :param monitored_entity_type: The monitored_entity_type of this AlertHistoryEntry.  # noqa: E501
        :type: str
        """

        self._monitored_entity_type = monitored_entity_type

    @property
    def project_and_version_label(self):
        """Gets the project_and_version_label of this AlertHistoryEntry.  # noqa: E501


        :return: The project_and_version_label of this AlertHistoryEntry.  # noqa: E501
        :rtype: str
        """
        return self._project_and_version_label

    @project_and_version_label.setter
    def project_and_version_label(self, project_and_version_label):
        """Sets the project_and_version_label of this AlertHistoryEntry.


        :param project_and_version_label: The project_and_version_label of this AlertHistoryEntry.  # noqa: E501
        :type: str
        """

        self._project_and_version_label = project_and_version_label

    @property
    def project_version_id(self):
        """Gets the project_version_id of this AlertHistoryEntry.  # noqa: E501


        :return: The project_version_id of this AlertHistoryEntry.  # noqa: E501
        :rtype: int
        """
        return self._project_version_id

    @project_version_id.setter
    def project_version_id(self, project_version_id):
        """Sets the project_version_id of this AlertHistoryEntry.


        :param project_version_id: The project_version_id of this AlertHistoryEntry.  # noqa: E501
        :type: int
        """

        self._project_version_id = project_version_id

    @property
    def triggered_date(self):
        """Gets the triggered_date of this AlertHistoryEntry.  # noqa: E501


        :return: The triggered_date of this AlertHistoryEntry.  # noqa: E501
        :rtype: datetime
        """
        return self._triggered_date

    @triggered_date.setter
    def triggered_date(self, triggered_date):
        """Sets the triggered_date of this AlertHistoryEntry.


        :param triggered_date: The triggered_date of this AlertHistoryEntry.  # noqa: E501
        :type: datetime
        """

        self._triggered_date = triggered_date

    @property
    def user_name(self):
        """Gets the user_name of this AlertHistoryEntry.  # noqa: E501


        :return: The user_name of this AlertHistoryEntry.  # noqa: E501
        :rtype: str
        """
        return self._user_name

    @user_name.setter
    def user_name(self, user_name):
        """Sets the user_name of this AlertHistoryEntry.


        :param user_name: The user_name of this AlertHistoryEntry.  # noqa: E501
        :type: str
        """

        self._user_name = user_name

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(AlertHistoryEntry, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, AlertHistoryEntry):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
