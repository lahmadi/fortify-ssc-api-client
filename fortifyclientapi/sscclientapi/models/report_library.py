# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ReportLibrary(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'description': 'str',
        'file_doc_id': 'int',
        'guid': 'str',
        'id': 'int',
        'name': 'str',
        'object_version': 'int',
        'publish_version': 'int'
    }

    attribute_map = {
        'description': 'description',
        'file_doc_id': 'fileDocId',
        'guid': 'guid',
        'id': 'id',
        'name': 'name',
        'object_version': 'objectVersion',
        'publish_version': 'publishVersion'
    }

    def __init__(self, description=None, file_doc_id=None, guid=None, id=None, name=None, object_version=None, publish_version=None):  # noqa: E501
        """ReportLibrary - a model defined in Swagger"""  # noqa: E501
        self._description = None
        self._file_doc_id = None
        self._guid = None
        self._id = None
        self._name = None
        self._object_version = None
        self._publish_version = None
        self.discriminator = None
        if description is not None:
            self.description = description
        if file_doc_id is not None:
            self.file_doc_id = file_doc_id
        self.guid = guid
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if object_version is not None:
            self.object_version = object_version
        if publish_version is not None:
            self.publish_version = publish_version

    @property
    def description(self):
        """Gets the description of this ReportLibrary.  # noqa: E501


        :return: The description of this ReportLibrary.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ReportLibrary.


        :param description: The description of this ReportLibrary.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def file_doc_id(self):
        """Gets the file_doc_id of this ReportLibrary.  # noqa: E501

        Report library file doc identifier  # noqa: E501

        :return: The file_doc_id of this ReportLibrary.  # noqa: E501
        :rtype: int
        """
        return self._file_doc_id

    @file_doc_id.setter
    def file_doc_id(self, file_doc_id):
        """Sets the file_doc_id of this ReportLibrary.

        Report library file doc identifier  # noqa: E501

        :param file_doc_id: The file_doc_id of this ReportLibrary.  # noqa: E501
        :type: int
        """

        self._file_doc_id = file_doc_id

    @property
    def guid(self):
        """Gets the guid of this ReportLibrary.  # noqa: E501

        Report library unique identifier  # noqa: E501

        :return: The guid of this ReportLibrary.  # noqa: E501
        :rtype: str
        """
        return self._guid

    @guid.setter
    def guid(self, guid):
        """Sets the guid of this ReportLibrary.

        Report library unique identifier  # noqa: E501

        :param guid: The guid of this ReportLibrary.  # noqa: E501
        :type: str
        """
        if guid is None:
            raise ValueError("Invalid value for `guid`, must not be `None`")  # noqa: E501

        self._guid = guid

    @property
    def id(self):
        """Gets the id of this ReportLibrary.  # noqa: E501


        :return: The id of this ReportLibrary.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this ReportLibrary.


        :param id: The id of this ReportLibrary.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this ReportLibrary.  # noqa: E501


        :return: The name of this ReportLibrary.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this ReportLibrary.


        :param name: The name of this ReportLibrary.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def object_version(self):
        """Gets the object_version of this ReportLibrary.  # noqa: E501

        Object version  # noqa: E501

        :return: The object_version of this ReportLibrary.  # noqa: E501
        :rtype: int
        """
        return self._object_version

    @object_version.setter
    def object_version(self, object_version):
        """Sets the object_version of this ReportLibrary.

        Object version  # noqa: E501

        :param object_version: The object_version of this ReportLibrary.  # noqa: E501
        :type: int
        """

        self._object_version = object_version

    @property
    def publish_version(self):
        """Gets the publish_version of this ReportLibrary.  # noqa: E501

        Publish version  # noqa: E501

        :return: The publish_version of this ReportLibrary.  # noqa: E501
        :rtype: int
        """
        return self._publish_version

    @publish_version.setter
    def publish_version(self, publish_version):
        """Sets the publish_version of this ReportLibrary.

        Publish version  # noqa: E501

        :param publish_version: The publish_version of this ReportLibrary.  # noqa: E501
        :type: int
        """

        self._publish_version = publish_version

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ReportLibrary, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReportLibrary):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
