# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class UserSessionState(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'category': 'str',
        'id': 'int',
        'name': 'str',
        'project_version_id': 'int',
        'username': 'str',
        'value': 'str'
    }

    attribute_map = {
        'category': 'category',
        'id': 'id',
        'name': 'name',
        'project_version_id': 'projectVersionId',
        'username': 'username',
        'value': 'value'
    }

    def __init__(self, category=None, id=None, name=None, project_version_id=None, username=None, value=None):  # noqa: E501
        """UserSessionState - a model defined in Swagger"""  # noqa: E501
        self._category = None
        self._id = None
        self._name = None
        self._project_version_id = None
        self._username = None
        self._value = None
        self.discriminator = None
        self.category = category
        if id is not None:
            self.id = id
        self.name = name
        if project_version_id is not None:
            self.project_version_id = project_version_id
        self.username = username
        if value is not None:
            self.value = value

    @property
    def category(self):
        """Gets the category of this UserSessionState.  # noqa: E501


        :return: The category of this UserSessionState.  # noqa: E501
        :rtype: str
        """
        return self._category

    @category.setter
    def category(self, category):
        """Sets the category of this UserSessionState.


        :param category: The category of this UserSessionState.  # noqa: E501
        :type: str
        """
        if category is None:
            raise ValueError("Invalid value for `category`, must not be `None`")  # noqa: E501

        self._category = category

    @property
    def id(self):
        """Gets the id of this UserSessionState.  # noqa: E501


        :return: The id of this UserSessionState.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this UserSessionState.


        :param id: The id of this UserSessionState.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this UserSessionState.  # noqa: E501


        :return: The name of this UserSessionState.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this UserSessionState.


        :param name: The name of this UserSessionState.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    @property
    def project_version_id(self):
        """Gets the project_version_id of this UserSessionState.  # noqa: E501


        :return: The project_version_id of this UserSessionState.  # noqa: E501
        :rtype: int
        """
        return self._project_version_id

    @project_version_id.setter
    def project_version_id(self, project_version_id):
        """Sets the project_version_id of this UserSessionState.


        :param project_version_id: The project_version_id of this UserSessionState.  # noqa: E501
        :type: int
        """

        self._project_version_id = project_version_id

    @property
    def username(self):
        """Gets the username of this UserSessionState.  # noqa: E501


        :return: The username of this UserSessionState.  # noqa: E501
        :rtype: str
        """
        return self._username

    @username.setter
    def username(self, username):
        """Sets the username of this UserSessionState.


        :param username: The username of this UserSessionState.  # noqa: E501
        :type: str
        """
        if username is None:
            raise ValueError("Invalid value for `username`, must not be `None`")  # noqa: E501

        self._username = username

    @property
    def value(self):
        """Gets the value of this UserSessionState.  # noqa: E501


        :return: The value of this UserSessionState.  # noqa: E501
        :rtype: str
        """
        return self._value

    @value.setter
    def value(self, value):
        """Sets the value of this UserSessionState.


        :param value: The value of this UserSessionState.  # noqa: E501
        :type: str
        """

        self._value = value

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(UserSessionState, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, UserSessionState):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
