# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class OpenSourceIssue(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'category': 'str',
        'component': 'str',
        'controllable': 'bool',
        'cve': 'str',
        'cve_url': 'str',
        'cwe': 'str',
        'cwe_url': 'str',
        'engine_type': 'str',
        'filename': 'str',
        'hidden': 'bool',
        'id': 'int',
        'invoked': 'bool',
        'issue_instance_id': 'str',
        'license': 'str',
        'priority': 'str',
        'project_version_id': 'int',
        'removed': 'bool',
        'suppressed': 'bool',
        'type': 'str',
        'upgrade_to_version': 'str',
        'version': 'str'
    }

    attribute_map = {
        'category': 'category',
        'component': 'component',
        'controllable': 'controllable',
        'cve': 'cve',
        'cve_url': 'cveUrl',
        'cwe': 'cwe',
        'cwe_url': 'cweUrl',
        'engine_type': 'engineType',
        'filename': 'filename',
        'hidden': 'hidden',
        'id': 'id',
        'invoked': 'invoked',
        'issue_instance_id': 'issueInstanceId',
        'license': 'license',
        'priority': 'priority',
        'project_version_id': 'projectVersionId',
        'removed': 'removed',
        'suppressed': 'suppressed',
        'type': 'type',
        'upgrade_to_version': 'upgradeToVersion',
        'version': 'version'
    }

    def __init__(self, category=None, component=None, controllable=None, cve=None, cve_url=None, cwe=None, cwe_url=None, engine_type=None, filename=None, hidden=None, id=None, invoked=None, issue_instance_id=None, license=None, priority=None, project_version_id=None, removed=None, suppressed=None, type=None, upgrade_to_version=None, version=None):  # noqa: E501
        """OpenSourceIssue - a model defined in Swagger"""  # noqa: E501
        self._category = None
        self._component = None
        self._controllable = None
        self._cve = None
        self._cve_url = None
        self._cwe = None
        self._cwe_url = None
        self._engine_type = None
        self._filename = None
        self._hidden = None
        self._id = None
        self._invoked = None
        self._issue_instance_id = None
        self._license = None
        self._priority = None
        self._project_version_id = None
        self._removed = None
        self._suppressed = None
        self._type = None
        self._upgrade_to_version = None
        self._version = None
        self.discriminator = None
        self.category = category
        self.component = component
        self.controllable = controllable
        self.cve = cve
        self.cve_url = cve_url
        self.cwe = cwe
        self.cwe_url = cwe_url
        self.engine_type = engine_type
        self.filename = filename
        if hidden is not None:
            self.hidden = hidden
        if id is not None:
            self.id = id
        self.invoked = invoked
        self.issue_instance_id = issue_instance_id
        self.license = license
        self.priority = priority
        self.project_version_id = project_version_id
        if removed is not None:
            self.removed = removed
        if suppressed is not None:
            self.suppressed = suppressed
        self.type = type
        self.upgrade_to_version = upgrade_to_version
        self.version = version

    @property
    def category(self):
        """Gets the category of this OpenSourceIssue.  # noqa: E501

        Category - The category for this issue.  # noqa: E501

        :return: The category of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._category

    @category.setter
    def category(self, category):
        """Sets the category of this OpenSourceIssue.

        Category - The category for this issue.  # noqa: E501

        :param category: The category of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if category is None:
            raise ValueError("Invalid value for `category`, must not be `None`")  # noqa: E501

        self._category = category

    @property
    def component(self):
        """Gets the component of this OpenSourceIssue.  # noqa: E501

        Component - usually in the format <group>:<artifact>  # noqa: E501

        :return: The component of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._component

    @component.setter
    def component(self, component):
        """Sets the component of this OpenSourceIssue.

        Component - usually in the format <group>:<artifact>  # noqa: E501

        :param component: The component of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if component is None:
            raise ValueError("Invalid value for `component`, must not be `None`")  # noqa: E501

        self._component = component

    @property
    def controllable(self):
        """Gets the controllable of this OpenSourceIssue.  # noqa: E501

        Does the application send user input to the vulnerable open source code?  # noqa: E501

        :return: The controllable of this OpenSourceIssue.  # noqa: E501
        :rtype: bool
        """
        return self._controllable

    @controllable.setter
    def controllable(self, controllable):
        """Sets the controllable of this OpenSourceIssue.

        Does the application send user input to the vulnerable open source code?  # noqa: E501

        :param controllable: The controllable of this OpenSourceIssue.  # noqa: E501
        :type: bool
        """
        if controllable is None:
            raise ValueError("Invalid value for `controllable`, must not be `None`")  # noqa: E501

        self._controllable = controllable

    @property
    def cve(self):
        """Gets the cve of this OpenSourceIssue.  # noqa: E501

        CVE ID in the format CVE-xxxxx-xxxx  # noqa: E501

        :return: The cve of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._cve

    @cve.setter
    def cve(self, cve):
        """Sets the cve of this OpenSourceIssue.

        CVE ID in the format CVE-xxxxx-xxxx  # noqa: E501

        :param cve: The cve of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if cve is None:
            raise ValueError("Invalid value for `cve`, must not be `None`")  # noqa: E501

        self._cve = cve

    @property
    def cve_url(self):
        """Gets the cve_url of this OpenSourceIssue.  # noqa: E501

        CVE URL - URL to get more information about this CVE.  # noqa: E501

        :return: The cve_url of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._cve_url

    @cve_url.setter
    def cve_url(self, cve_url):
        """Sets the cve_url of this OpenSourceIssue.

        CVE URL - URL to get more information about this CVE.  # noqa: E501

        :param cve_url: The cve_url of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if cve_url is None:
            raise ValueError("Invalid value for `cve_url`, must not be `None`")  # noqa: E501

        self._cve_url = cve_url

    @property
    def cwe(self):
        """Gets the cwe of this OpenSourceIssue.  # noqa: E501

        CWE ID in the format CWE-xxxx  # noqa: E501

        :return: The cwe of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._cwe

    @cwe.setter
    def cwe(self, cwe):
        """Sets the cwe of this OpenSourceIssue.

        CWE ID in the format CWE-xxxx  # noqa: E501

        :param cwe: The cwe of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if cwe is None:
            raise ValueError("Invalid value for `cwe`, must not be `None`")  # noqa: E501

        self._cwe = cwe

    @property
    def cwe_url(self):
        """Gets the cwe_url of this OpenSourceIssue.  # noqa: E501

        CWE URL - URL to get more information about the related CWE  # noqa: E501

        :return: The cwe_url of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._cwe_url

    @cwe_url.setter
    def cwe_url(self, cwe_url):
        """Sets the cwe_url of this OpenSourceIssue.

        CWE URL - URL to get more information about the related CWE  # noqa: E501

        :param cwe_url: The cwe_url of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if cwe_url is None:
            raise ValueError("Invalid value for `cwe_url`, must not be `None`")  # noqa: E501

        self._cwe_url = cwe_url

    @property
    def engine_type(self):
        """Gets the engine_type of this OpenSourceIssue.  # noqa: E501

        Engine type  # noqa: E501

        :return: The engine_type of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._engine_type

    @engine_type.setter
    def engine_type(self, engine_type):
        """Sets the engine_type of this OpenSourceIssue.

        Engine type  # noqa: E501

        :param engine_type: The engine_type of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if engine_type is None:
            raise ValueError("Invalid value for `engine_type`, must not be `None`")  # noqa: E501

        self._engine_type = engine_type

    @property
    def filename(self):
        """Gets the filename of this OpenSourceIssue.  # noqa: E501

        Filename of the component where the CVE exists  # noqa: E501

        :return: The filename of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._filename

    @filename.setter
    def filename(self, filename):
        """Sets the filename of this OpenSourceIssue.

        Filename of the component where the CVE exists  # noqa: E501

        :param filename: The filename of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if filename is None:
            raise ValueError("Invalid value for `filename`, must not be `None`")  # noqa: E501

        self._filename = filename

    @property
    def hidden(self):
        """Gets the hidden of this OpenSourceIssue.  # noqa: E501

        Is this issue hidden?  # noqa: E501

        :return: The hidden of this OpenSourceIssue.  # noqa: E501
        :rtype: bool
        """
        return self._hidden

    @hidden.setter
    def hidden(self, hidden):
        """Sets the hidden of this OpenSourceIssue.

        Is this issue hidden?  # noqa: E501

        :param hidden: The hidden of this OpenSourceIssue.  # noqa: E501
        :type: bool
        """

        self._hidden = hidden

    @property
    def id(self):
        """Gets the id of this OpenSourceIssue.  # noqa: E501

        Application version issue identifier  # noqa: E501

        :return: The id of this OpenSourceIssue.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this OpenSourceIssue.

        Application version issue identifier  # noqa: E501

        :param id: The id of this OpenSourceIssue.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def invoked(self):
        """Gets the invoked of this OpenSourceIssue.  # noqa: E501

        Does the application call the vulnerable open source code?  # noqa: E501

        :return: The invoked of this OpenSourceIssue.  # noqa: E501
        :rtype: bool
        """
        return self._invoked

    @invoked.setter
    def invoked(self, invoked):
        """Sets the invoked of this OpenSourceIssue.

        Does the application call the vulnerable open source code?  # noqa: E501

        :param invoked: The invoked of this OpenSourceIssue.  # noqa: E501
        :type: bool
        """
        if invoked is None:
            raise ValueError("Invalid value for `invoked`, must not be `None`")  # noqa: E501

        self._invoked = invoked

    @property
    def issue_instance_id(self):
        """Gets the issue_instance_id of this OpenSourceIssue.  # noqa: E501

        Issue instance identifier  # noqa: E501

        :return: The issue_instance_id of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._issue_instance_id

    @issue_instance_id.setter
    def issue_instance_id(self, issue_instance_id):
        """Sets the issue_instance_id of this OpenSourceIssue.

        Issue instance identifier  # noqa: E501

        :param issue_instance_id: The issue_instance_id of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if issue_instance_id is None:
            raise ValueError("Invalid value for `issue_instance_id`, must not be `None`")  # noqa: E501

        self._issue_instance_id = issue_instance_id

    @property
    def license(self):
        """Gets the license of this OpenSourceIssue.  # noqa: E501

        Component license  # noqa: E501

        :return: The license of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._license

    @license.setter
    def license(self, license):
        """Sets the license of this OpenSourceIssue.

        Component license  # noqa: E501

        :param license: The license of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if license is None:
            raise ValueError("Invalid value for `license`, must not be `None`")  # noqa: E501

        self._license = license

    @property
    def priority(self):
        """Gets the priority of this OpenSourceIssue.  # noqa: E501

        Priority  # noqa: E501

        :return: The priority of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._priority

    @priority.setter
    def priority(self, priority):
        """Sets the priority of this OpenSourceIssue.

        Priority  # noqa: E501

        :param priority: The priority of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if priority is None:
            raise ValueError("Invalid value for `priority`, must not be `None`")  # noqa: E501

        self._priority = priority

    @property
    def project_version_id(self):
        """Gets the project_version_id of this OpenSourceIssue.  # noqa: E501

        Application version identifier  # noqa: E501

        :return: The project_version_id of this OpenSourceIssue.  # noqa: E501
        :rtype: int
        """
        return self._project_version_id

    @project_version_id.setter
    def project_version_id(self, project_version_id):
        """Sets the project_version_id of this OpenSourceIssue.

        Application version identifier  # noqa: E501

        :param project_version_id: The project_version_id of this OpenSourceIssue.  # noqa: E501
        :type: int
        """
        if project_version_id is None:
            raise ValueError("Invalid value for `project_version_id`, must not be `None`")  # noqa: E501

        self._project_version_id = project_version_id

    @property
    def removed(self):
        """Gets the removed of this OpenSourceIssue.  # noqa: E501

        Is this issue removed?  # noqa: E501

        :return: The removed of this OpenSourceIssue.  # noqa: E501
        :rtype: bool
        """
        return self._removed

    @removed.setter
    def removed(self, removed):
        """Sets the removed of this OpenSourceIssue.

        Is this issue removed?  # noqa: E501

        :param removed: The removed of this OpenSourceIssue.  # noqa: E501
        :type: bool
        """

        self._removed = removed

    @property
    def suppressed(self):
        """Gets the suppressed of this OpenSourceIssue.  # noqa: E501

        Is this issue suppressed?  # noqa: E501

        :return: The suppressed of this OpenSourceIssue.  # noqa: E501
        :rtype: bool
        """
        return self._suppressed

    @suppressed.setter
    def suppressed(self, suppressed):
        """Sets the suppressed of this OpenSourceIssue.

        Is this issue suppressed?  # noqa: E501

        :param suppressed: The suppressed of this OpenSourceIssue.  # noqa: E501
        :type: bool
        """

        self._suppressed = suppressed

    @property
    def type(self):
        """Gets the type of this OpenSourceIssue.  # noqa: E501

        Component type  # noqa: E501

        :return: The type of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this OpenSourceIssue.

        Component type  # noqa: E501

        :param type: The type of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")  # noqa: E501

        self._type = type

    @property
    def upgrade_to_version(self):
        """Gets the upgrade_to_version of this OpenSourceIssue.  # noqa: E501

        Component version to upgrade to  # noqa: E501

        :return: The upgrade_to_version of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._upgrade_to_version

    @upgrade_to_version.setter
    def upgrade_to_version(self, upgrade_to_version):
        """Sets the upgrade_to_version of this OpenSourceIssue.

        Component version to upgrade to  # noqa: E501

        :param upgrade_to_version: The upgrade_to_version of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if upgrade_to_version is None:
            raise ValueError("Invalid value for `upgrade_to_version`, must not be `None`")  # noqa: E501

        self._upgrade_to_version = upgrade_to_version

    @property
    def version(self):
        """Gets the version of this OpenSourceIssue.  # noqa: E501

        Component version containing the CVE vulnerability  # noqa: E501

        :return: The version of this OpenSourceIssue.  # noqa: E501
        :rtype: str
        """
        return self._version

    @version.setter
    def version(self, version):
        """Sets the version of this OpenSourceIssue.

        Component version containing the CVE vulnerability  # noqa: E501

        :param version: The version of this OpenSourceIssue.  # noqa: E501
        :type: str
        """
        if version is None:
            raise ValueError("Invalid value for `version`, must not be `None`")  # noqa: E501

        self._version = version

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(OpenSourceIssue, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, OpenSourceIssue):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
