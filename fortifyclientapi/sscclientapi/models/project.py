# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Project(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'created_by': 'str',
        'creation_date': 'datetime',
        'description': 'str',
        'id': 'int',
        'issue_template_id': 'str',
        'name': 'str'
    }

    attribute_map = {
        'created_by': 'createdBy',
        'creation_date': 'creationDate',
        'description': 'description',
        'id': 'id',
        'issue_template_id': 'issueTemplateId',
        'name': 'name'
    }

    def __init__(self, created_by=None, creation_date=None, description=None, id=None, issue_template_id=None, name=None):  # noqa: E501
        """Project - a model defined in Swagger"""  # noqa: E501
        self._created_by = None
        self._creation_date = None
        self._description = None
        self._id = None
        self._issue_template_id = None
        self._name = None
        self.discriminator = None
        self.created_by = created_by
        self.creation_date = creation_date
        if description is not None:
            self.description = description
        if id is not None:
            self.id = id
        if issue_template_id is not None:
            self.issue_template_id = issue_template_id
        self.name = name

    @property
    def created_by(self):
        """Gets the created_by of this Project.  # noqa: E501

        User that created this application  # noqa: E501

        :return: The created_by of this Project.  # noqa: E501
        :rtype: str
        """
        return self._created_by

    @created_by.setter
    def created_by(self, created_by):
        """Sets the created_by of this Project.

        User that created this application  # noqa: E501

        :param created_by: The created_by of this Project.  # noqa: E501
        :type: str
        """
        if created_by is None:
            raise ValueError("Invalid value for `created_by`, must not be `None`")  # noqa: E501

        self._created_by = created_by

    @property
    def creation_date(self):
        """Gets the creation_date of this Project.  # noqa: E501

        Date created  # noqa: E501

        :return: The creation_date of this Project.  # noqa: E501
        :rtype: datetime
        """
        return self._creation_date

    @creation_date.setter
    def creation_date(self, creation_date):
        """Sets the creation_date of this Project.

        Date created  # noqa: E501

        :param creation_date: The creation_date of this Project.  # noqa: E501
        :type: datetime
        """
        if creation_date is None:
            raise ValueError("Invalid value for `creation_date`, must not be `None`")  # noqa: E501

        self._creation_date = creation_date

    @property
    def description(self):
        """Gets the description of this Project.  # noqa: E501

        Description  # noqa: E501

        :return: The description of this Project.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this Project.

        Description  # noqa: E501

        :param description: The description of this Project.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def id(self):
        """Gets the id of this Project.  # noqa: E501

        Id  # noqa: E501

        :return: The id of this Project.  # noqa: E501
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Project.

        Id  # noqa: E501

        :param id: The id of this Project.  # noqa: E501
        :type: int
        """

        self._id = id

    @property
    def issue_template_id(self):
        """Gets the issue_template_id of this Project.  # noqa: E501

        Id of the Issue Template used  # noqa: E501

        :return: The issue_template_id of this Project.  # noqa: E501
        :rtype: str
        """
        return self._issue_template_id

    @issue_template_id.setter
    def issue_template_id(self, issue_template_id):
        """Sets the issue_template_id of this Project.

        Id of the Issue Template used  # noqa: E501

        :param issue_template_id: The issue_template_id of this Project.  # noqa: E501
        :type: str
        """
        if issue_template_id is None:
            raise ValueError("Invalid value for `issue_template_id`, must not be `None`")  # noqa: E501

        self._issue_template_id = issue_template_id

    @property
    def name(self):
        """Gets the name of this Project.  # noqa: E501

        Name  # noqa: E501

        :return: The name of this Project.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Project.

        Name  # noqa: E501

        :param name: The name of this Project.  # noqa: E501
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")  # noqa: E501

        self._name = name

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Project, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Project):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
