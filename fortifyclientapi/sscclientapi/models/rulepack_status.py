# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class RulepackStatus(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'code': 'int',
        'message': 'str',
        'rulepack_resource_id': 'int'
    }

    attribute_map = {
        'code': 'code',
        'message': 'message',
        'rulepack_resource_id': 'rulepackResourceId'
    }

    def __init__(self, code=None, message=None, rulepack_resource_id=None):  # noqa: E501
        """RulepackStatus - a model defined in Swagger"""  # noqa: E501
        self._code = None
        self._message = None
        self._rulepack_resource_id = None
        self.discriminator = None
        if code is not None:
            self.code = code
        if message is not None:
            self.message = message
        if rulepack_resource_id is not None:
            self.rulepack_resource_id = rulepack_resource_id

    @property
    def code(self):
        """Gets the code of this RulepackStatus.  # noqa: E501

        Numeric processing code.  # noqa: E501

        :return: The code of this RulepackStatus.  # noqa: E501
        :rtype: int
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this RulepackStatus.

        Numeric processing code.  # noqa: E501

        :param code: The code of this RulepackStatus.  # noqa: E501
        :type: int
        """

        self._code = code

    @property
    def message(self):
        """Gets the message of this RulepackStatus.  # noqa: E501

        Processing message.  # noqa: E501

        :return: The message of this RulepackStatus.  # noqa: E501
        :rtype: str
        """
        return self._message

    @message.setter
    def message(self, message):
        """Sets the message of this RulepackStatus.

        Processing message.  # noqa: E501

        :param message: The message of this RulepackStatus.  # noqa: E501
        :type: str
        """

        self._message = message

    @property
    def rulepack_resource_id(self):
        """Gets the rulepack_resource_id of this RulepackStatus.  # noqa: E501

        Identifier of successfully created rulepack.  # noqa: E501

        :return: The rulepack_resource_id of this RulepackStatus.  # noqa: E501
        :rtype: int
        """
        return self._rulepack_resource_id

    @rulepack_resource_id.setter
    def rulepack_resource_id(self, rulepack_resource_id):
        """Sets the rulepack_resource_id of this RulepackStatus.

        Identifier of successfully created rulepack.  # noqa: E501

        :param rulepack_resource_id: The rulepack_resource_id of this RulepackStatus.  # noqa: E501
        :type: int
        """

        self._rulepack_resource_id = rulepack_resource_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(RulepackStatus, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, RulepackStatus):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
