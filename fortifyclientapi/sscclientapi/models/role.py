# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class Role(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'all_application_role': 'bool',
        'assigned_to_non_users': 'bool',
        'built_in': 'bool',
        'default': 'bool',
        'deletable': 'bool',
        'description': 'str',
        'id': 'str',
        'name': 'str',
        'object_version': 'int',
        'permission_ids': 'list[str]',
        'publish_version': 'int'
    }

    attribute_map = {
        'all_application_role': 'allApplicationRole',
        'assigned_to_non_users': 'assignedToNonUsers',
        'built_in': 'builtIn',
        'default': 'default',
        'deletable': 'deletable',
        'description': 'description',
        'id': 'id',
        'name': 'name',
        'object_version': 'objectVersion',
        'permission_ids': 'permissionIds',
        'publish_version': 'publishVersion'
    }

    def __init__(self, all_application_role=None, assigned_to_non_users=None, built_in=None, default=None, deletable=None, description=None, id=None, name=None, object_version=None, permission_ids=None, publish_version=None):  # noqa: E501
        """Role - a model defined in Swagger"""  # noqa: E501
        self._all_application_role = None
        self._assigned_to_non_users = None
        self._built_in = None
        self._default = None
        self._deletable = None
        self._description = None
        self._id = None
        self._name = None
        self._object_version = None
        self._permission_ids = None
        self._publish_version = None
        self.discriminator = None
        self.all_application_role = all_application_role
        self.assigned_to_non_users = assigned_to_non_users
        self.built_in = built_in
        if default is not None:
            self.default = default
        if deletable is not None:
            self.deletable = deletable
        if description is not None:
            self.description = description
        if id is not None:
            self.id = id
        if name is not None:
            self.name = name
        if object_version is not None:
            self.object_version = object_version
        self.permission_ids = permission_ids
        if publish_version is not None:
            self.publish_version = publish_version

    @property
    def all_application_role(self):
        """Gets the all_application_role of this Role.  # noqa: E501

        True if a user with this Role has access to all Application Versions  # noqa: E501

        :return: The all_application_role of this Role.  # noqa: E501
        :rtype: bool
        """
        return self._all_application_role

    @all_application_role.setter
    def all_application_role(self, all_application_role):
        """Sets the all_application_role of this Role.

        True if a user with this Role has access to all Application Versions  # noqa: E501

        :param all_application_role: The all_application_role of this Role.  # noqa: E501
        :type: bool
        """
        if all_application_role is None:
            raise ValueError("Invalid value for `all_application_role`, must not be `None`")  # noqa: E501

        self._all_application_role = all_application_role

    @property
    def assigned_to_non_users(self):
        """Gets the assigned_to_non_users of this Role.  # noqa: E501

        True if this Role is currently assigned to entities that are not users(groups, organizations, etc.)  # noqa: E501

        :return: The assigned_to_non_users of this Role.  # noqa: E501
        :rtype: bool
        """
        return self._assigned_to_non_users

    @assigned_to_non_users.setter
    def assigned_to_non_users(self, assigned_to_non_users):
        """Sets the assigned_to_non_users of this Role.

        True if this Role is currently assigned to entities that are not users(groups, organizations, etc.)  # noqa: E501

        :param assigned_to_non_users: The assigned_to_non_users of this Role.  # noqa: E501
        :type: bool
        """
        if assigned_to_non_users is None:
            raise ValueError("Invalid value for `assigned_to_non_users`, must not be `None`")  # noqa: E501

        self._assigned_to_non_users = assigned_to_non_users

    @property
    def built_in(self):
        """Gets the built_in of this Role.  # noqa: E501

        True if this Role is a default Fortify Role  # noqa: E501

        :return: The built_in of this Role.  # noqa: E501
        :rtype: bool
        """
        return self._built_in

    @built_in.setter
    def built_in(self, built_in):
        """Sets the built_in of this Role.

        True if this Role is a default Fortify Role  # noqa: E501

        :param built_in: The built_in of this Role.  # noqa: E501
        :type: bool
        """
        if built_in is None:
            raise ValueError("Invalid value for `built_in`, must not be `None`")  # noqa: E501

        self._built_in = built_in

    @property
    def default(self):
        """Gets the default of this Role.  # noqa: E501


        :return: The default of this Role.  # noqa: E501
        :rtype: bool
        """
        return self._default

    @default.setter
    def default(self, default):
        """Sets the default of this Role.


        :param default: The default of this Role.  # noqa: E501
        :type: bool
        """

        self._default = default

    @property
    def deletable(self):
        """Gets the deletable of this Role.  # noqa: E501


        :return: The deletable of this Role.  # noqa: E501
        :rtype: bool
        """
        return self._deletable

    @deletable.setter
    def deletable(self, deletable):
        """Sets the deletable of this Role.


        :param deletable: The deletable of this Role.  # noqa: E501
        :type: bool
        """

        self._deletable = deletable

    @property
    def description(self):
        """Gets the description of this Role.  # noqa: E501


        :return: The description of this Role.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this Role.


        :param description: The description of this Role.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def id(self):
        """Gets the id of this Role.  # noqa: E501


        :return: The id of this Role.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this Role.


        :param id: The id of this Role.  # noqa: E501
        :type: str
        """

        self._id = id

    @property
    def name(self):
        """Gets the name of this Role.  # noqa: E501


        :return: The name of this Role.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this Role.


        :param name: The name of this Role.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def object_version(self):
        """Gets the object_version of this Role.  # noqa: E501


        :return: The object_version of this Role.  # noqa: E501
        :rtype: int
        """
        return self._object_version

    @object_version.setter
    def object_version(self, object_version):
        """Sets the object_version of this Role.


        :param object_version: The object_version of this Role.  # noqa: E501
        :type: int
        """

        self._object_version = object_version

    @property
    def permission_ids(self):
        """Gets the permission_ids of this Role.  # noqa: E501

        Ids of the permissions this Role has  # noqa: E501

        :return: The permission_ids of this Role.  # noqa: E501
        :rtype: list[str]
        """
        return self._permission_ids

    @permission_ids.setter
    def permission_ids(self, permission_ids):
        """Sets the permission_ids of this Role.

        Ids of the permissions this Role has  # noqa: E501

        :param permission_ids: The permission_ids of this Role.  # noqa: E501
        :type: list[str]
        """
        if permission_ids is None:
            raise ValueError("Invalid value for `permission_ids`, must not be `None`")  # noqa: E501

        self._permission_ids = permission_ids

    @property
    def publish_version(self):
        """Gets the publish_version of this Role.  # noqa: E501


        :return: The publish_version of this Role.  # noqa: E501
        :rtype: int
        """
        return self._publish_version

    @publish_version.setter
    def publish_version(self, publish_version):
        """Sets the publish_version of this Role.


        :param publish_version: The publish_version of this Role.  # noqa: E501
        :type: int
        """

        self._publish_version = publish_version

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Role, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Role):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
