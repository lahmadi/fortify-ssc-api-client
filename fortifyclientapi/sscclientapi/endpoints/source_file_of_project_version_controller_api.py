# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from fortifyclientapi.sscclientapi.api_client import ApiClient


class SourceFileOfProjectVersionControllerApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def list_source_file_of_project_version(self, parent_id, **kwargs):  # noqa: E501
        """list  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.list_source_file_of_project_version(parent_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int parent_id: parentId (required)
        :param str q: The url-encoded value of a source file query expression \"path:&lt;path_to_file_in_quotes&gt;+AND+scan_id:&lt;s_id&gt;\". For example, \"q=path:%22JavaSource%2Forg%2Fowasp%2Fwebgoat%2Flessons%2FCSRF.java%22%2Band%2Bscan_id:1\". If 'scan_id' is not provided, the search defaults to the latest scan of the application version.
        :param str fields: Output fields
        :return: ApiResultListSourceFileDto
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.list_source_file_of_project_version_with_http_info(parent_id, **kwargs)  # noqa: E501
        else:
            (data) = self.list_source_file_of_project_version_with_http_info(parent_id, **kwargs)  # noqa: E501
            return data

    def list_source_file_of_project_version_with_http_info(self, parent_id, **kwargs):  # noqa: E501
        """list  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.list_source_file_of_project_version_with_http_info(parent_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int parent_id: parentId (required)
        :param str q: The url-encoded value of a source file query expression \"path:&lt;path_to_file_in_quotes&gt;+AND+scan_id:&lt;s_id&gt;\". For example, \"q=path:%22JavaSource%2Forg%2Fowasp%2Fwebgoat%2Flessons%2FCSRF.java%22%2Band%2Bscan_id:1\". If 'scan_id' is not provided, the search defaults to the latest scan of the application version.
        :param str fields: Output fields
        :return: ApiResultListSourceFileDto
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['parent_id', 'q', 'fields']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_source_file_of_project_version" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'parent_id' is set
        if ('parent_id' not in params or
                params['parent_id'] is None):
            raise ValueError("Missing the required parameter `parent_id` when calling `list_source_file_of_project_version`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'parent_id' in params:
            path_params['parentId'] = params['parent_id']  # noqa: E501

        query_params = []
        if 'q' in params:
            query_params.append(('q', params['q']))  # noqa: E501
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['FortifyToken']  # noqa: E501

        return self.api_client.call_api(
            '/projectVersions/{parentId}/sourceFiles', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiResultListSourceFileDto',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
