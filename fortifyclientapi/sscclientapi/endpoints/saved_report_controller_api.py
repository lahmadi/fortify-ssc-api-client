# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from fortifyclientapi.sscclientapi.api_client import ApiClient


class SavedReportControllerApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def create_saved_report(self, body, **kwargs):  # noqa: E501
        """create  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_saved_report(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param SavedReport body: resource (required)
        :return: ApiResultSavedReport
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.create_saved_report_with_http_info(body, **kwargs)  # noqa: E501
        else:
            (data) = self.create_saved_report_with_http_info(body, **kwargs)  # noqa: E501
            return data

    def create_saved_report_with_http_info(self, body, **kwargs):  # noqa: E501
        """create  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.create_saved_report_with_http_info(body, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param SavedReport body: resource (required)
        :return: ApiResultSavedReport
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['body']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method create_saved_report" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'body' is set
        if ('body' not in params or
                params['body'] is None):
            raise ValueError("Missing the required parameter `body` when calling `create_saved_report`")  # noqa: E501

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        if 'body' in params:
            body_params = params['body']
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # HTTP header `Content-Type`
        header_params['Content-Type'] = self.api_client.select_header_content_type(  # noqa: E501
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['FortifyToken']  # noqa: E501

        return self.api_client.call_api(
            '/reports', 'POST',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiResultSavedReport',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def delete_saved_report(self, id, **kwargs):  # noqa: E501
        """delete  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_saved_report(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: id (required)
        :return: ApiResultVoid
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.delete_saved_report_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.delete_saved_report_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def delete_saved_report_with_http_info(self, id, **kwargs):  # noqa: E501
        """delete  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.delete_saved_report_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: id (required)
        :return: ApiResultVoid
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method delete_saved_report" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params or
                params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `delete_saved_report`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['FortifyToken']  # noqa: E501

        return self.api_client.call_api(
            '/reports/{id}', 'DELETE',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiResultVoid',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def list_saved_report(self, **kwargs):  # noqa: E501
        """list  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.list_saved_report(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str fields: Output fields
        :param int start: A start offset in object listing
        :param int limit: A maximum number of returned objects in listing, if '-1' or '0' no limit is applied
        :param str q: A search-spec of full text search query (see fulltextsearch parameter)
        :param bool fulltextsearch: If 'true', interpret 'q' parameter as full text search query, defaults to 'false'
        :param str embed: Fields to embed
        :param str orderby: Fields to order by
        :return: ApiResultListSavedReport
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.list_saved_report_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.list_saved_report_with_http_info(**kwargs)  # noqa: E501
            return data

    def list_saved_report_with_http_info(self, **kwargs):  # noqa: E501
        """list  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.list_saved_report_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str fields: Output fields
        :param int start: A start offset in object listing
        :param int limit: A maximum number of returned objects in listing, if '-1' or '0' no limit is applied
        :param str q: A search-spec of full text search query (see fulltextsearch parameter)
        :param bool fulltextsearch: If 'true', interpret 'q' parameter as full text search query, defaults to 'false'
        :param str embed: Fields to embed
        :param str orderby: Fields to order by
        :return: ApiResultListSavedReport
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['fields', 'start', 'limit', 'q', 'fulltextsearch', 'embed', 'orderby']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method list_saved_report" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501
        if 'start' in params:
            query_params.append(('start', params['start']))  # noqa: E501
        if 'limit' in params:
            query_params.append(('limit', params['limit']))  # noqa: E501
        if 'q' in params:
            query_params.append(('q', params['q']))  # noqa: E501
        if 'fulltextsearch' in params:
            query_params.append(('fulltextsearch', params['fulltextsearch']))  # noqa: E501
        if 'embed' in params:
            query_params.append(('embed', params['embed']))  # noqa: E501
        if 'orderby' in params:
            query_params.append(('orderby', params['orderby']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['FortifyToken']  # noqa: E501

        return self.api_client.call_api(
            '/reports', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiResultListSavedReport',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def read_saved_report(self, id, **kwargs):  # noqa: E501
        """read  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.read_saved_report(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: id (required)
        :param str fields: Output fields
        :param str embed: Fields to embed
        :return: ApiResultSavedReport
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.read_saved_report_with_http_info(id, **kwargs)  # noqa: E501
        else:
            (data) = self.read_saved_report_with_http_info(id, **kwargs)  # noqa: E501
            return data

    def read_saved_report_with_http_info(self, id, **kwargs):  # noqa: E501
        """read  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.read_saved_report_with_http_info(id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int id: id (required)
        :param str fields: Output fields
        :param str embed: Fields to embed
        :return: ApiResultSavedReport
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['id', 'fields', 'embed']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method read_saved_report" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'id' is set
        if ('id' not in params or
                params['id'] is None):
            raise ValueError("Missing the required parameter `id` when calling `read_saved_report`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'id' in params:
            path_params['id'] = params['id']  # noqa: E501

        query_params = []
        if 'fields' in params:
            query_params.append(('fields', params['fields']))  # noqa: E501
        if 'embed' in params:
            query_params.append(('embed', params['embed']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['FortifyToken']  # noqa: E501

        return self.api_client.call_api(
            '/reports/{id}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ApiResultSavedReport',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
