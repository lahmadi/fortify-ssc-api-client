# coding: utf-8

"""
    SecDojo Commands file

"""

from argparse import ArgumentParser, Namespace, FileType
import re


# from fortifyclientapi.cli.command_parsers.attributes_definition_commands import AttributeDefinitionCommands
# from fortifyclientapi.cli.command_parsers.project_version_commands import ProjectVersionCommands
# from fortifyclientapi.cli.command_parsers import project_version_commands_parser
# from fortifyclientapi.cli.command_parsers import attributes_definition_commands_parser


class SscApiCommands(object):
    """
    SecDojo Global Command Class for the Fortify Software Security Center API
    """

    def __init__(
            self,
            prog: str = "ssc-cli",
            version_file: str = "version.txt",
            usage: str = None,  # "%(prog)s [-hv] [options] [command] [arguments]",
            description: str = "Fortify Software Security Center API command line interface",
            epilog: str = None,
            add_help: bool = True,
            allow_abbrev: bool = False,
            exit_on_error: bool = False
    ) -> None:
        self.prog = prog
        self.version_file = version_file
        self.usage = usage
        self.description = description
        self.add_help = add_help
        self.allow_abbrev = allow_abbrev
        self.exit_on_error = exit_on_error

        self.main_parser: ArgumentParser = ArgumentParser(
            prog=prog,
            usage=usage,
            description=description,
            epilog=epilog,
            add_help=add_help,
            allow_abbrev=allow_abbrev,
            exit_on_error=exit_on_error,
            fromfile_prefix_chars='@'
        )

        try:
            version_number = open(self.version_file, "rt").read()
        except FileNotFoundError:
            version_number = "2.0.X"

        self.main_parser.add_argument("-v", "--version", action='version',
                                      version="{} - {}".format(self.prog, version_number))

        credentials_group = self.main_parser.add_argument_group(
            "authentication", "Username and Password or Api Token")
        credentials_group.add_argument("-u", "--username", type=str, default="",
                                       help="Specify the username to connect to SSC", dest="authentication_username")

        credentials_group.add_argument("--password", type=str, default="",
                                       help="Specify the password to connect to SSC", dest="authentication_password")

        credentials_group.add_argument("-t", "--token", "--api-key", type=str,
                                       help="Specify the token/api key of the user account",
                                       dest="authentication_api_key")

        self.main_parser.add_argument("-s", "--server-url", type=str,
                                      help="Specify the SSC base URL or SSC API URL",
                                      required=True, dest="ssc_base_url")

        ssl_group = self.main_parser.add_argument_group("ssl", "SSL Options")
        ssl_group.add_argument("--trustcerts", "--no-ssl-verify", action="store_false",
                               help="Disable the SSL verification",
                               dest="ssl_cert_verification")
        ssl_group.add_argument("--cacert", "--ca-certificate", type=str, default="", required=False,
                               help="Specify the path to the application certificate or the CA certificate which has signed the application certificate",
                               dest="ssl_ca_cert")
        ssl_group.add_argument("--client-cert", type=str, default="", required=False,
                               help="Specify the path to the client certificate", dest="ssl_client_cert")
        ssl_group.add_argument("--client-key", type=str, default="", required=False,
                               help="Specify the path to the client certificate key", dest="ssl_client_key")
        ssl_group.add_argument("--hostname-verify", action="store_true",
                               help="Enable SSL hostname verification", dest="ssl_hostname_verification")

        logging_group = self.main_parser.add_argument_group(
            "logging", "Logging Options")
        logging_group.add_argument("--debug", action="store_true",
                                   help="Enable the Debug level mode",
                                   dest="logging_debug")
        logging_group.add_argument("--logfile", type=str, default="./{}.log".format(self.prog).replace(" ", "_"),
                                   help="Specify the path to the logfile",
                                   dest="logging_logfile")

        self.subparsers = self.main_parser.add_subparsers(
            dest="command",
            title="Commands",
            help="Commands help"
        )

        self.subparsers.required = False

        ########################################################
        # Add command parsers here
        # For each command parser create a dedicated private method like __project_version_cmd(self)
        # And add subcommand parsers or arguments
        self.project_version_parser = self.subparsers.add_parser(
            "appversion",
            help="""Create/Update/Copy/getIssues Project Version: command line interface for the Project Version API Endpoints
            """
        )
        self.__project_version_cmd()

        self.attribute_definition_parser = self.subparsers.add_parser(
            "attribute",
            help="""Create/Update Attribute definition: command line interface for the Attribute definition API Endpoints
            """
        )
        self.__attribute_definition_cmd()

    def parse_args(self, args: list[str]) -> Namespace:
        return self.main_parser.parse_args(args)

    def print_help(self) -> None:
        self.main_parser.print_help()

    def __project_version_cmd(self):
        """Project Version command line parser
        """
        self.project_version_subparsers = self.project_version_parser.add_subparsers(
            dest="subcommand", title="Project Version Subcommands", help="Project Version Subcommands Help")

        self.create_project_version_parser = self.project_version_subparsers.add_parser(
            "create",
            help="""Create Project Version: command line interface for the Project Version API Endpoints
            """
        )
        self.__create_project_version_subcommand()

        self.update_project_version_parser = self.project_version_subparsers.add_parser(
            "update",
            help="""Update Project Version: command line interface for the Project Version API Endpoints
            """
        )
        self.__update_project_version_subcommand()

        self.getIssues_project_version_parser = self.project_version_subparsers.add_parser(
            "getIssues",
            help="""Get Issues From Project Version: command line interface for the Project Version API Endpoints
            """
        )
        self.__getIssues_project_version_subcommand()

        self.copy_project_version_parser = self.project_version_subparsers.add_parser(
            "copy",
            help="""Copy Project Version: command line interface for the Project Version API Endpoints
            """
        )
        self.__copy_project_version_subcommand()

    def __create_project_version_subcommand(self):
        """Create SubCommand
        """
        application_group = self.create_project_version_parser.add_argument_group(
            "application", "Application Options")
        application_group.add_argument("--app", "--application", type=str, help="Specify the application name",
                                       required=True, dest="project_version_project_name")

        application_group.add_argument("--appversion", "--application-version", type=str,
                                       help="Specify the application version",
                                       required=True, dest="project_version_version_name")

        self.create_project_version_parser.add_argument("--deactivate", action="store_false",
                                                        help="Disable the application version",
                                                        dest="project_version_active")

        self.create_project_version_parser.add_argument("--auto-predict", action="store_true",
                                                        help="Enable the auto prediction - AuditAssistant",
                                                        dest="project_version_auto_predict")

        bug_tracker_plugin = self.create_project_version_parser.add_mutually_exclusive_group(
            required=False)
        bug_tracker_plugin.add_argument("--bug-tracker-plugin-id", type=str, help="The Bug Tracker Plugin Id",
                                        dest="project_version_bug_tracker_plugin_id")

        bug_tracker_plugin.add_argument("--bug-tracker-plugin-name", type=str, help="The Bug Tracker Plugin name",
                                        dest="project_version_bug_tracker_plugin_name", default="")

        self.create_project_version_parser.add_argument("--enable-bug-tracker", action="store_true",
                                                        help="Enable the given Bug Track Plugin",
                                                        dest="project_version_bug_tracker_enabled")

        self.create_project_version_parser.add_argument("--created-by", type=str,
                                                        help="Username of the application version creator",
                                                        dest="project_version_created_by", default="")

        self.create_project_version_parser.add_argument("--enable-custom-tag-values-auto-apply", action="store_true",
                                                        help="Enable Custom Tag Values Auto Apply - AuditAssistant",
                                                        dest="project_version_custom_tag_values_auto_apply")

        self.create_project_version_parser.add_argument("--description", type=str,
                                                        help="The application version description",
                                                        dest="project_version_description",
                                                        default="From REST API - FortifyClientAPI")

        issue_template = self.create_project_version_parser.add_mutually_exclusive_group(
            required=False)
        issue_template.add_argument("--issue-template-id", type=str,
                                    help="The issue template attached to the application version",
                                    dest="project_version_issue_template_id")

        issue_template.add_argument("--issue-template-name", type=str,
                                    help="The issue template attached to the application version",
                                    dest="project_version_issue_template_name",
                                    default="Prioritized High Risk Issue Template")

        custom_tag = self.create_project_version_parser.add_mutually_exclusive_group(
            required=False)
        custom_tag.add_argument("--custom-tag-guid", type=str,
                                help="The primary custom tag for the application version",
                                dest="project_version_custom_tag_guid")

        custom_tag.add_argument("--custom-tag-name", type=str,
                                help="The primary custom tag for the application version",
                                dest="project_version_custom_tag_name", default="Analysis")

        self.create_project_version_parser.add_argument("-a", "--attribute", action="append", type=str,
                                                        help="Set the attributes of the application version as myAttr=myValue",
                                                        dest="project_version_attributes", default=list())

        self.create_project_version_parser.add_argument("-r", "--processing-rule", action="append", type=str,
                                                        help="Set the Result Processing Rules of the application version as myProcessRule=true/false",
                                                        dest="project_version_processing_rules", default=list())

    def __update_project_version_subcommand(self):
        """Update SubCommand
        """
        application_options_group = self.update_project_version_parser.add_argument_group(
            "application", "Application Options")

        application_options_group.add_argument(
            "--id", "--application-id", type=int, help="Specify the application id",
            dest="project_version_project_version_id")

        application_options_group.add_argument("--app", "--application", type=str, help="Specify the application name",
                                               dest="project_version_project_name")

        application_options_group.add_argument("--appversion", "--application-version", type=str,
                                               help="Specify the application version",
                                               dest="project_version_version_name")

        self.update_project_version_parser.add_argument("--auto-create", action="store_true",
                                                        help="Automatically create the application version if does not exist",
                                                        dest="project_version_auto_create")

        self.update_project_version_parser.add_argument("--deactivate", action="store_false",
                                                        help="Disable the application version",
                                                        dest="project_version_active")

        self.update_project_version_parser.add_argument("--auto-predict", action="store_true",
                                                        help="Enable the auto prediction - AuditAssistant",
                                                        dest="project_version_auto_predict")

        bug_tracker_plugin = self.update_project_version_parser.add_mutually_exclusive_group(
            required=False)
        bug_tracker_plugin.add_argument("--bug-tracker-plugin-id", type=str, help="The Bug Tracker Plugin Id",
                                        dest="project_version_bug_tracker_plugin_id")

        bug_tracker_plugin.add_argument("--bug-tracker-plugin-name", type=str, help="The Bug Tracker Plugin name",
                                        dest="project_version_bug_tracker_plugin_name", default="")

        self.update_project_version_parser.add_argument("--enable-bug-tracker", action="store_true",
                                                        help="Enable the given Bug Track Plugin",
                                                        dest="project_version_bug_tracker_enabled")

        self.update_project_version_parser.add_argument("--created-by", type=str,
                                                        help="Username of the application version creator",
                                                        dest="project_version_created_by", default="")

        self.update_project_version_parser.add_argument("--enable-custom-tag-values-auto-apply", action="store_true",
                                                        help="Enable Custom Tag Values Auto Apply - AuditAssistant",
                                                        dest="project_version_custom_tag_values_auto_apply")

        self.update_project_version_parser.add_argument("--description", type=str,
                                                        help="The application version description",
                                                        dest="project_version_description",
                                                        default="From REST API - FortifyClientAPI")

        issue_template = self.update_project_version_parser.add_mutually_exclusive_group(
            required=False)
        issue_template.add_argument("--issue-template-id", type=str,
                                    help="The issue template attached to the application version",
                                    dest="project_version_issue_template_id")

        issue_template.add_argument("--issue-template-name", type=str,
                                    help="The issue template attached to the application version",
                                    dest="project_version_issue_template_name",
                                    default="Prioritized High Risk Issue Template")

        custom_tag = self.update_project_version_parser.add_mutually_exclusive_group(
            required=False)
        custom_tag.add_argument("--custom-tag-guid", type=str,
                                help="The primary custom tag for the application version",
                                dest="project_version_custom_tag_guid")

        custom_tag.add_argument("--custom-tag-name", type=str,
                                help="The primary custom tag for the application version",
                                dest="project_version_custom_tag_name", default="Analysis")

        self.update_project_version_parser.add_argument("-a", "--attribute", action="append", type=str,
                                                        help="Set the attributes of the application version as myAttr=myValue",
                                                        dest="project_version_attributes", default=list())

        self.update_project_version_parser.add_argument("-r", "--processing-rule", action="append", type=str,
                                                        help="Set the Result Processing Rules of the application version as myProcessRule=true/false",
                                                        dest="project_version_processing_rules", default=list())

    def __copy_project_version_subcommand(self):
        """Copy SubCommand
        """
        application_options_group = self.copy_project_version_parser.add_argument_group(
            "application", "Application Options")

        application_options_group.add_argument(
            "--src-id", "--source-application-id", type=int, help="Specify the source application id",
            dest="project_version_source_project_version_id")

        application_options_group.add_argument("--src-app", "--source-application", type=str,
                                               help="Specify the source application name",
                                               dest="project_version_source_project_name")

        application_options_group.add_argument("--src-appversion", "--source-application-version", type=str,
                                               help="Specify the source application version",
                                               dest="project_version_source_version_name")

        application_options_group.add_argument(
            "--dst-id", "--dest-application-id", type=int, help="Specify the destination application id",
            dest="project_version_destination_project_version_id")

        application_options_group.add_argument("--dst-app", "--dest-application", type=str,
                                               help="Specify the destination application name",
                                               dest="project_version_destination_project_name")

        application_options_group.add_argument("--dst-appversion", "--dest-application-version", type=str,
                                               help="Specify the destination application version",
                                               dest="project_version_destination_version_name")

        copy_options_group = self.copy_project_version_parser.add_argument_group(
            "copy", "Copy Options")

        copy_options_group.add_argument("--copy-attributes", action="store_true",
                                        help="Copy the Project Version Attributes to the new application version",
                                        dest="project_version_copy_attributes")

        copy_options_group.add_argument("--copy-user-access", action="store_true",
                                        help="Copy the User Access Settings to the new application version",
                                        dest="project_version_copy_user_access")

        copy_options_group.add_argument("--copy-analysis-processing-rules", action="store_true",
                                        help="Copy the Analysis Processing Rules to the new application version",
                                        dest="project_version_copy_analysis_processing_rules")

        copy_options_group.add_argument("--copy-bug-tracker-configuration", action="store_true",
                                        help="Copy the Bug Tracker Configuration to the new application version",
                                        dest="project_version_copy_bug_tracker_configuration")

        copy_options_group.add_argument("--copy-custom-tags", action="store_true",
                                        help="Copy the Custom Tags to the new application version",
                                        dest="project_version_copy_custom_tags")

        copy_options_group.add_argument("--copy-current-state", action="store_true", help="Copy the Current State",
                                        dest="project_version_copy_current_state")

    def __getIssues_project_version_subcommand(self):
        """get Issues SubCommand
        """
        application_options_group = self.getIssues_project_version_parser.add_argument_group(
            "application", "Application Options")

        application_options_group.add_argument(
            "--id", "--application-id", type=int, help="Specify the application id",
            dest="project_version_project_version_id")

        application_options_group.add_argument("--app", "--application", type=str, help="Specify the application name",
                                               dest="project_version_project_name")

        application_options_group.add_argument("--appversion", "--application-version", type=str,
                                               help="Specify the application version",
                                               dest="project_version_version_name")

        self.getIssues_project_version_parser.add_argument("--show-hidden", action="store_true",
                                                           help="Include hidden issues in search results",
                                                           dest="get_issues_show_hidden", required=False)

        self.getIssues_project_version_parser.add_argument("--show-removed", action="store_true",
                                                           help="Include removed issues in search results",
                                                           dest="get_issues_show_removed", required=False)

        self.getIssues_project_version_parser.add_argument("--show-suppressed", action="store_true",
                                                           help="Include suppressed issues in search results",
                                                           dest="get_issues_show_suppressed", required=False)

        self.getIssues_project_version_parser.add_argument("--query", type=str, help="An issue query expression",
                                                           dest="get_issues_query", required=False)

    ###############################
    # Attribute Definition Command

    def __attribute_definition_cmd(self):
        """Project Version command line arguments
        """
        self.attribute_definition_subparsers = self.attribute_definition_parser.add_subparsers(
            dest="subcommand", title="Project Version Subcommands", help="Project Version Subcommands Help")

        self.create_attribute_definition_parser = self.attribute_definition_subparsers.add_parser(
            "create",
            help="""Create Attribute Definition: command line interface for the Attribute Definition API Endpoints
            """
        )
        self.__create_attribute_definition_subcommand()

        self.update_attribute_definition_parser = self.attribute_definition_subparsers.add_parser(
            "update",
            help="""Update Attribute Definition: command line interface for the Attribute Definition API Endpoints
            """
        )
        self.__update_attribute_definition_subcommand()

    def __create_attribute_definition_subcommand(self):
        """Attribute Definition Create subcommand
        """
        self.create_attribute_definition_parser.add_argument("-f", "--from-file", type=FileType("r", encoding="UTF-8"),
                                                             help="""Specify the filepath of the data file
                                    The file is a JSON-based format containing a list of attribute definition
                                    [
                                        {
                                            "app_entity_type": "PROJECT_VERSION",
                                            "category": "TECHNICAL",
                                            "description": "string",
                                            "guid": "string",
                                            "has_default": false,
                                            "hidden": false,
                                            "name": "string",
                                            "options": [
                                                {
                                                "description": "string",
                                                "guid": "string",
                                                "hidden": false,
                                                "name": "string"
                                                },
                                                ...
                                            ],
                                            "required": false,
                                            "system_usage": "USER_DEFINED_DELETABLE",
                                            "type": "SINGLE"
                                        }, 
                                        ...
                                    ]
                                    """, dest="attribute_data_filepath")

        data_from_cmd_group = self.create_attribute_definition_parser.add_argument_group(
            "attribute", "Attribute Options")

        data_from_cmd_group.add_argument("-n", "--name", type=str, help="Specify the name of the attribute",
                                         dest="attribute_name")

        data_from_cmd_group.add_argument("--id", type=int, help="Specify the id of the attribute",
                                         dest="attribute_id")

        data_from_cmd_group.add_argument("--type", type=str,
                                         choices=["TEXT", "SINGLE", "MULTIPLE", "LONG_TEXT",
                                                  "SENSITIVE_TEXT", "BOOLEAN", "INTEGER", "DATE", "FILE"],
                                         help="Specify the type of values for the attribute", dest="attribute_type")

        data_from_cmd_group.add_argument("--app-entity-type", type=str, help="Specify the entity type of the attribute",
                                         choices=["PROJECT_VERSION", "NONE"], default="PROJECT_VERSION",
                                         dest="attribute_app_entity_type")

        data_from_cmd_group.add_argument("--category", type=str,
                                         choices=["TECHNICAL", "BUSINESS",
                                                  "DYNAMIC_SCAN_REQUEST", "ORGANIZATION"],
                                         help="Specify the category of the attribute", default="TECHNICAL",
                                         dest="attribute_category")

        data_from_cmd_group.add_argument(
            "--description", type=str, help="Specify the description of the attribute", default="",
            dest="attribute_description")

        data_from_cmd_group.add_argument(
            "--guid", type=str, help="Specify the guid of the attribute", default="", dest="attribute_guid")

        data_from_cmd_group.add_argument("--hidden", action="store_true",
                                         help="Specify if the attribute is hidden", dest="attribute_hidden")

        data_from_cmd_group.add_argument("--required", action="store_true",
                                         help="Specify if the attribute is required", dest="attribute_required")

        data_from_cmd_group.add_argument("--system-usage", type=str,
                                         choices=["HP_DEFINED_DELETABLE", "HP_DEFINED_NON_DELETABLE",
                                                  "USER_DEFINED_DELETABLE", "USER_DEFINED_NON_DELETABLE"],
                                         help="Specify the systemUsage of the attribute",
                                         default="USER_DEFINED_DELETABLE", dest="attribute_system_usage")

        data_from_cmd_group.add_argument("--option", action="append", nargs=4,
                                         help="""Specify the options for the attribute
                                        Repeatable switch
                                        usage: --option <name> [description | ''] [guid | '']  [hidden | 'false']
                                        """, dest="attribute_options")

    def __update_attribute_definition_subcommand(self):
        """Attribute Definition Create subcommand
        """
        self.update_attribute_definition_parser.add_argument("--auto-create", action="store_true",
                                                             help="Automatically create the attribute if not exists",
                                                             dest="attribute_auto_create")

        self.update_attribute_definition_parser.add_argument("-f", "--from-file", type=FileType("r", encoding="UTF-8"),
                                                             help="""Specify the filepath of the data file or the stdin (with -)
                                    The file is a JSON-based format containing a list of attribute definition
                                    [
                                        {
                                            "app_entity_type": "PROJECT_VERSION",
                                            "category": "TECHNICAL",
                                            "description": "string",
                                            "guid": "string",
                                            "has_default": false,
                                            "hidden": false,
                                            "name": "string",
                                            "options": [
                                                {
                                                "description": "string",
                                                "guid": "string",
                                                "hidden": false,
                                                "name": "string"
                                                }, 
                                                ...
                                            ],
                                            "required": false,
                                            "system_usage": "USER_DEFINED_DELETABLE",
                                            "type": "SINGLE"
                                        }, 
                                        ...
                                    ]
                                    """, dest="attribute_data_filepath")

        data_from_cmd_group = self.update_attribute_definition_parser.add_argument_group(
            "attribute", "Attribute Options")

        data_from_cmd_group.add_argument("-n", "--name", type=str, help="Specify the name of the attribute",
                                         dest="attribute_name")

        data_from_cmd_group.add_argument("--id", type=int, help="Specify the id of the attribute",
                                         dest="attribute_id")

        data_from_cmd_group.add_argument("--type", type=str,
                                         choices=["TEXT", "SINGLE", "MULTIPLE", "LONG_TEXT",
                                                  "SENSITIVE_TEXT", "BOOLEAN", "INTEGER", "DATE", "FILE"],
                                         help="Specify the type of values for the attribute", dest="attribute_type")

        data_from_cmd_group.add_argument("--app-entity-type", type=str, help="Specify the entity type of the attribute",
                                         choices=["PROJECT_VERSION", "NONE"], default="PROJECT_VERSION",
                                         dest="attribute_app_entity_type")

        data_from_cmd_group.add_argument("--category", type=str,
                                         choices=["TECHNICAL", "BUSINESS",
                                                  "DYNAMIC_SCAN_REQUEST", "ORGANIZATION"],
                                         help="Specify the category of the attribute", default="TECHNICAL",
                                         dest="attribute_category")

        data_from_cmd_group.add_argument(
            "--description", type=str, help="Specify the description of the attribute", default="",
            dest="attribute_description")

        data_from_cmd_group.add_argument(
            "--guid", type=str, help="Specify the guid of the attribute", default="", dest="attribute_guid")

        data_from_cmd_group.add_argument("--hidden", action="store_true",
                                         help="Specify if the attribute is hidden", dest="attribute_hidden")

        data_from_cmd_group.add_argument("--required", action="store_true",
                                         help="Specify if the attribute is required", dest="attribute_required")

        data_from_cmd_group.add_argument("--system-usage", type=str,
                                         choices=["HP_DEFINED_DELETABLE", "HP_DEFINED_NON_DELETABLE",
                                                  "USER_DEFINED_DELETABLE", "USER_DEFINED_NON_DELETABLE"],
                                         help="Specify the systemUsage of the attribute",
                                         default="USER_DEFINED_DELETABLE", dest="attribute_system_usage")

        options_group = data_from_cmd_group.add_mutually_exclusive_group(
            required=False)
        options_group.add_argument("--option", action="append", nargs=4,
                                   help="""Specify the options for the attribute
                                        Repeatable switch
                                        usage: --option <name> [description | ''] [guid | '']  [hidden | 'false']
                                        """, dest="attribute_options")

        options_group.add_argument("--add-option", action="append", nargs=4,
                                   help="""Specify the options for adding to the existing attribute
                                        Repeatable switch
                                        usage: --add-option <name> [description | ''] [guid | ''] [hidden | 'false']
                                        """, dest="attribute_options_to_add")
