#!/usr/bin/env python

# coding: utf-8

from argparse import Namespace
from datetime import datetime
from fortifyclientapi.cli.cli_controller.cli_controller import CliController
from fortifyclientapi.cli.command_exception import CommandException
from fortifyclientapi.controller.attribute_definition_controller import AttributeDefinitionController
from fortifyclientapi.controller.custom_tag_controller import CustomTagController
from fortifyclientapi.controller.issue_template_controller import IssueTemplateController
from fortifyclientapi.controller.project_version_attribute_controller import ProjectVersionAttributeController
from fortifyclientapi.controller.project_version_auth_entity_controller import ProjectVersionAuthEntityController
from fortifyclientapi.controller.project_version_controller import ProjectVersionController
from fortifyclientapi.controller.project_version_result_processing_rule_controller import ProjectVersionResultProcessingRuleController
from fortifyclientapi.helper.attribute_helper import AttributeHelper
from fortifyclientapi.helper.project_version_helper import ProjectVersionHelper
from fortifyclientapi.sscclientapi.models.attribute import Attribute
from fortifyclientapi.sscclientapi.models.project_version import ProjectVersion


class CopyProjectVersionCliController(CliController):

    def parse(self, data: Namespace):
        self.source_id: int = data.project_version_source_project_version_id or None
        self.source_project_name: str = data.project_version_source_project_name or None
        self.source_version_name: str = data.project_version_source_version_name or None

        self.destination_id: int = data.project_version_destination_project_version_id or None
        self.destination_project_name: str = data.project_version_destination_project_name or None
        self.destination_version_name: str = data.project_version_destination_version_name or None

        self.copy_attributes: bool = data.project_version_copy_attributes or False
        self.copy_user_access: bool = data.project_version_copy_user_access or False
        self.copy_analysis_processing_rules: bool = data.project_version_copy_analysis_processing_rules or False
        self.copy_bug_tracker_configuration: bool = data.project_version_copy_bug_tracker_configuration or False
        self.copy_custom_tags: bool = data.project_version_copy_custom_tags or False
        self.copy_current_state: bool = data.project_version_copy_current_state or False

    def execute(self):
        project_version_helper: ProjectVersionHelper = ProjectVersionHelper(
            ProjectVersionController(self._configuration),
            ProjectVersionAttributeController(self._configuration),
            IssueTemplateController(self._configuration),
            CustomTagController(self._configuration),
            ProjectVersionResultProcessingRuleController(self._configuration),
            ProjectVersionAuthEntityController(self._configuration)
        )

        attribute_helper: AttributeHelper = AttributeHelper(
            AttributeDefinitionController(self._configuration)
        )

        destination_project_version: ProjectVersion = project_version_helper.configure_project_version(
            project_name=self.destination_project_name,
            version_name=self.destination_version_name,
            project_version_id=self.destination_id
        )
        
        source_project_version: ProjectVersion = project_version_helper.configure_project_version(
            project_name=self.source_project_name,
            version_name=self.source_version_name,
            project_version_id=self.source_id
        )

        attributes: list[Attribute] = None 
        
        if self.copy_attributes:
            attributes = project_version_helper.get_controller(controller_type=ProjectVersionAttributeController).get_project_version_attributes(project_version_id=source_project_version.id)
        else:
            attributes = attribute_helper.get_default_required_attributes()

        if destination_project_version.id is None:
                destination_project_version: ProjectVersion = project_version_helper.configure_project_version(
                    project_name=self.destination_project_name,
                    version_name=self.destination_version_name,
                    project_version_id=self.destination_id,
                    auto_predict=source_project_version.auto_predict,
                    description=source_project_version.description,
                    issue_template_name=source_project_version.issue_template_name
                )
                
                destination_project_version.id = project_version_helper.create_project_version(
                    project_version=destination_project_version
                ).id
        else:
            if destination_project_version.committed and (self.copy_analysis_processing_rules or self.copy_bug_tracker_configuration or self.copy_custom_tags):
                raise CommandException(
                    command="CopyProjectVersion",
                    status="conflict",
                    reason="Destination Application Version {} - {} ({}) already exists and has been committed".format(
                        destination_project_version.project.name if destination_project_version.project.name else "unknown",
                        destination_project_version.name if destination_project_version.name else "unknown",
                        destination_project_version.id)
                )
            
        if self.copy_analysis_processing_rules or self.copy_bug_tracker_configuration or self.copy_custom_tags:
            project_version_helper.get_controller(ProjectVersionController).copy_version_by_id(
                source_project_version_id=source_project_version.id,
                destination_project_version_id=destination_project_version.id,
                copy_analysis_processing_rules=self.copy_analysis_processing_rules or False,
                copy_bug_tracker_configuration=self.copy_bug_tracker_configuration or False,
                copy_custom_tags=self.copy_custom_tags or False
            )

            destination_project_version = project_version_helper.configure_project_version(
                project_name=self.destination_project_name,
                version_name=self.destination_version_name,
                project_version_id=self.destination_id
            )
            
            if not destination_project_version.committed:
                destination_project_version = project_version_helper.update_project_version(
                    project_version=destination_project_version,
                    attributes=attributes
                )
        
        if self.copy_attributes:
            destination_project_version = project_version_helper.get_controller(controller_type=ProjectVersionAttributeController).create_or_update_project_version_attributes(
                destination_project_version.id,
                *attributes
            )
            
        if self.copy_current_state:
            destination_project_version = project_version_helper.configure_project_version(
                project_name=self.destination_project_name,
                version_name=self.destination_version_name,
                project_version_id=self.destination_id
            )
            
            if not destination_project_version.committed:
                destination_project_version = project_version_helper.update_project_version(
                    project_version=destination_project_version,
                    attributes=attributes
                )

            project_version_helper.get_controller(ProjectVersionController).copy_current_state_by_id(
                source_project_version_id=source_project_version.id,
                destination_project_version_id=destination_project_version.id
            )
        
        if self.copy_user_access:
            project_version_helper.get_controller(controller_type=ProjectVersionAuthEntityController).update_user_access_list(
                project_version_id=destination_project_version.id, 
                user_list=project_version_helper.get_controller(
                    controller_type=ProjectVersionAuthEntityController).get_user_access_list(project_version_id=source_project_version.id)
            )

        print(destination_project_version.id)
