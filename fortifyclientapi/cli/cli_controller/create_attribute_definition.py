#!/usr/bin/env python

# coding: utf-8

from argparse import Namespace
from json import load
from typing import Any

from fortifyclientapi.cli.cli_controller.cli_controller import CliController
from fortifyclientapi.cli.command_exception import CommandException
from fortifyclientapi.helper.attribute_helper import AttributeHelper
from fortifyclientapi.controller.attribute_definition_controller import AttributeDefinitionController
from fortifyclientapi.sscclientapi.models.attribute_definition import AttributeDefinition
from fortifyclientapi.sscclientapi.models.attribute_option import AttributeOption


class CreateAttributeDefinitionCliController(CliController):

    def parse(self, data: Namespace):
        attribute_helper: AttributeHelper = AttributeHelper(
            AttributeDefinitionController(self._configuration)
        )

        self.attribute_definitions: list[AttributeDefinition] = []

        if data.attribute_data_filepath is None:
            id: int = data.attribute_id or None
            app_entity_type: str = data.attribute_app_entity_type or "PROJECT_VERSION"
            category: str = data.attribute_category or "TECHNICAL"
            description: str = data.attribute_description or ""
            guid: str = data.attribute_guid or None
            has_default: bool = False
            hidden: bool = data.attribute_hidden or False
            name: str = data.attribute_name or None
            required: bool = data.attribute_required or False
            system_usage: str = data.attribute_system_usage or "USER_DEFINED_DELETABLE"
            type: str = data.attribute_type or "SIGNLE"

            options: list[AttributeOption] = None

            # Options parsing
            option_data: list[tuple[str, str, str, bool]
                              ] = data.attribute_options or []
            options = [] if len(option_data) > 0 and type in [
                "SINGLE", "MULTIPLE"] else None
            index: int = 0

            if options == []:
                for option_name, option_description, option_guid, option_hidden in option_data:
                    options += [
                        attribute_helper.configure_attribute_option(
                            name=option_name,
                            guid=option_guid if option_guid != "" else None,
                            description=option_description or "",
                            hidden=True if option_hidden.lower(
                            ) in ["yes", "true", "on", "1"] else False,
                            index=index,
                            attribute_definition_id=id or None,
                            attribute_definition_name=name or None
                        )
                    ]

            attribute_definition: AttributeDefinition = attribute_helper.configure_attribute_definition(
                attribute_definition_id=id or None,
                app_entity_type=app_entity_type or "PROJECT_VERSION",
                category=category or "TECHNICAL",
                description=description or "",
                guid=guid or None,
                has_default=has_default or False,
                hidden=hidden or False,
                name=name or None,
                required=required or False,
                system_usage=system_usage or "USER_DEFINED_DELETABLE",
                type=type or "SIGNLE" if options is not None else "TEXT",
                options=options if options else None
            )

            if attribute_definition.id:
                raise CommandException(
                    command="CreateAttributeDefinition",
                    status="conflict",
                    reason="Attribute Definition {} ({}) already exists".format(attribute_definition.name,
                                                                                attribute_definition.id)
                )

            self.attribute_definitions += [attribute_definition]
        else:
            # Handle creation if a file is given
            attributes_data: list[dict[str, Any]] = load(
                fp=data.attribute_data_filepath)

            data.attribute_data_filepath.close()

            for attribute in attributes_data:
                id: int = attribute.get("id") or None

                app_entity_type: str = attribute.get(
                    "app_entity_type", "PROJECT_VERSION")
                category: str = attribute.get("category", "TECHNICAL")
                description: str = attribute.get("description", "")
                guid: str = attribute.get("guid", None)
                has_default: bool = False
                hidden: bool = attribute.get("hidden", False)
                name: str = attribute.get("name", None)
                required: bool = attribute.get("required", False)
                system_usage: str = attribute.get(
                    "system_usage", "USER_DEFINED_DELETABLE")
                type: str = attribute.get("type", "SIGNLE")

                options: list[AttributeOption] = None

                # Options parsing
                option_data: list[dict[str, Any]
                                  ] = attribute.get("options", [])
                options = [] if len(option_data) > 0 and type in [
                    "SINGLE", "MULTIPLE"] else None
                index: int = 0

                if options == []:
                    for option in option_data:
                        options += [
                            attribute_helper.configure_attribute_option(
                                name=option.get(name),
                                guid=option.get(guid),
                                description=option.get("description") or "",
                                hidden=option.get("hidden") or False,
                                index=index,
                                attribute_definition_id=id or None,
                                attribute_definition_name=name or None
                            )
                        ]

                attribute_definition: AttributeDefinition = attribute_helper.configure_attribute_definition(
                    attribute_definition_id=id or None,
                    app_entity_type=app_entity_type or "PROJECT_VERSION",
                    category=category or "TECHNICAL",
                    description=description or "",
                    guid=guid or None,
                    has_default=has_default or False,
                    hidden=hidden or False,
                    name=name or None,
                    required=required or False,
                    system_usage=system_usage or "USER_DEFINED_DELETABLE",
                    type=type or "SIGNLE" if options is not None else "TEXT",
                    options=options if options else None
                )

                if attribute_definition.id:
                    raise CommandException(
                        command="CreateAttributeDefinition",
                        status="conflict",
                        reason="Attribute Definition {} ({}) already exists".format(attribute_definition.name,
                                                                                    attribute_definition.id)
                    )

                self.attribute_definitions += [attribute_definition]

    def execute(self):
        attribute_helper: AttributeHelper = AttributeHelper(
            AttributeDefinitionController(self._configuration)
        )

        for attribute_definition in self.attribute_definitions:
            attribute: AttributeDefinition = attribute_helper.create_attribute_definition(
                attribute_definition=attribute_definition
            )

            print(attribute.id)
