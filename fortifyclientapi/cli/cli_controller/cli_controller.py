#!/usr/bin/env python

# coding: utf-8

from abc import abstractclassmethod
from argparse import Namespace
from fortifyclientapi.controller.authentication_controller import AuthenticationController

from fortifyclientapi.helper.configuration_helper import ConfigurationHelper


class CliController(object):

    def __init__(self, cli_data: Namespace, execute: bool = False, configuration: ConfigurationHelper = None, authenticate: bool = True) -> None:
        super().__init__()

        self._cli_data: Namespace = cli_data

        if configuration is None:
            self._configuration: ConfigurationHelper = self.__configure()
        else:
            self._configuration = configuration

        if authenticate is not None and authenticate:
            self._configuration = self.__authenticate()
        else:
            self._configuration = configuration

        self.parse(self._cli_data)

        if execute:
            self.execute()

    @abstractclassmethod
    def parse(self, data: Namespace):
        pass

    @abstractclassmethod
    def execute(self):
        pass

    def __configure(self) -> ConfigurationHelper:
        ssc_api_url: str = ""

        if self._cli_data.ssc_base_url[-1] == "/":
            ssc_api_url = self._cli_data.ssc_base_url[:len(
                self._cli_data.ssc_base_url) - 1]
        else:
            ssc_api_url = self._cli_data.ssc_base_url

        if "api/v1" not in ssc_api_url:
            ssc_api_url = ssc_api_url + "/api/v1"

        return ConfigurationHelper().configure(
            username=self._cli_data.authentication_username,
            password=self._cli_data.authentication_password,
            api_key=self._cli_data.authentication_api_key,
            api_url=ssc_api_url,
            ssl_cert_verification=self._cli_data.ssl_cert_verification,
            ssl_ca_cert=self._cli_data.ssl_ca_cert,
            ssl_client_cert=self._cli_data.ssl_client_cert,
            ssl_client_key=self._cli_data.ssl_client_key,
            ssl_hostname_verification=self._cli_data.ssl_hostname_verification,
            logging_debug=self._cli_data.logging_debug,
            logging_logfile=self._cli_data.logging_logfile
        )

    def __authenticate(self) -> ConfigurationHelper:
        return AuthenticationController(self._configuration).authenticate()

    def get_configuration(self) -> ConfigurationHelper or None:
        return self._configuration

    def close_token_connection(self):
        if self._configuration.token_id is not None:
            AuthenticationController(self._configuration).remove_token_by_id(
                self._configuration.token_id)
