#!/usr/bin/env python

# coding: utf-8


class CommandException(Exception):

    def __init__(self, command: str, status: str = None, reason: str = None):
        self.command: str = command
        self.status: str = status
        self.reason: str = reason

    def __str__(self):
        """Custom error messages for exception"""
        error_message = "{0} ({1}):\n"\
                        "Reason: {2}\n".format(
                            self.command, self.status, self.reason)
        return error_message
