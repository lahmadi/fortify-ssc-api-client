#!/usr/bin/env python

# coding: utf-8

from sys import argv
from argparse import Namespace, ArgumentError, ArgumentTypeError
from fortifyclientapi.sscclientapi.rest import ApiException
from fortifyclientapi.cli.cli_controller.cli_controller import CliController
from fortifyclientapi.cli.cli_controller.copy_project_version import CopyProjectVersionCliController
from fortifyclientapi.cli.cli_controller.create_attribute_definition import CreateAttributeDefinitionCliController
from fortifyclientapi.cli.cli_controller.create_project_version import CreateProjectVersionCliController
from fortifyclientapi.cli.cli_controller.update_attribute_definition import UpdateAttributeDefinitionCliController
from fortifyclientapi.cli.cli_controller.update_project_version import UpdateProjectVersionCliController
from fortifyclientapi.cli.cli_controller.get_issues_project_version import GetIssuesProjectVersionCliController
from fortifyclientapi.cli.command_exception import CommandException

from fortifyclientapi.cli.command_parsers.ssc_api_commands import SscApiCommands
from fortifyclientapi.controller.controller_exception import ControllerException
from fortifyclientapi.helper.helper_exception import HelperException


def main(args: list[str] = argv):
    cli_data: Namespace = None
    command_parser: SscApiCommands = SscApiCommands()

    try:
        cli_data = command_parser.parse_args(args[1:])
    except (Exception, ArgumentError, ArgumentTypeError) as e:
        command_parser.print_help()
        raise CommandException(
            command="SscApiCommandsParser",
            status="error",
            reason="{}".format(e)
        )

    try:
        cli_controller: CliController = None

        if cli_data.command == "appversion":
            if cli_data.subcommand == "create":
                cli_controller = CreateProjectVersionCliController(
                    cli_data=cli_data, execute=True
                )
            elif cli_data.subcommand == "update":
                cli_controller = UpdateProjectVersionCliController(
                    cli_data=cli_data, execute=True
                )
            elif cli_data.subcommand == "copy":
                cli_controller = CopyProjectVersionCliController(
                    cli_data=cli_data, execute=True
                )
            elif cli_data.subcommand == "getIssues":
                cli_controller = GetIssuesProjectVersionCliController(
                    cli_data=cli_data, execute=True
                )
        elif cli_data.command == "attribute":
            if cli_data.subcommand == "create":
                cli_controller = CreateAttributeDefinitionCliController(
                    cli_data=cli_data, execute=True
                )
            elif cli_data.subcommand == "update":
                cli_controller = UpdateAttributeDefinitionCliController(
                    cli_data=cli_data, execute=True
                )
    except (ApiException, ControllerException, HelperException) as e:
        raise CommandException(
            command="SscApiCommandsParser",
            status="error",
            reason="{}".format(e)
        )
    except (Exception, InterruptedError, KeyboardInterrupt) as e:
        raise Exception(e)
    finally:
        if cli_controller is not None:
            cli_controller.close_token_connection()


if __name__ == "__main__":
    try:
        main()
    except CommandException as err_command:
        print(err_command)
        exit(1)
    except Exception as err_other:
        print("Error: Something goes wrong\n{}".format(err_other))
        exit(2)
    except (InterruptedError, KeyboardInterrupt) as err_interupted:
        print("Interrupted")
        exit(3)
    except (RuntimeError, RuntimeWarning) as err_runtime:
        print("Faltal error: Runtime error\n{}".format(err_runtime))
        exit(3)
