# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.saved_report_controller_api import SavedReportControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestSavedReportControllerApi(unittest.TestCase):
    """SavedReportControllerApi unit test stubs"""

    def setUp(self):
        self.api = SavedReportControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_create_saved_report(self):
        """Test case for create_saved_report

        create  # noqa: E501
        """
        pass

    def test_delete_saved_report(self):
        """Test case for delete_saved_report

        delete  # noqa: E501
        """
        pass

    def test_list_saved_report(self):
        """Test case for list_saved_report

        list  # noqa: E501
        """
        pass

    def test_read_saved_report(self):
        """Test case for read_saved_report

        read  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
