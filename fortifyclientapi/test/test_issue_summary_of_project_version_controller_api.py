# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.issue_summary_of_project_version_controller_api import IssueSummaryOfProjectVersionControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestIssueSummaryOfProjectVersionControllerApi(unittest.TestCase):
    """IssueSummaryOfProjectVersionControllerApi unit test stubs"""

    def setUp(self):
        self.api = IssueSummaryOfProjectVersionControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_list_issue_summary_of_project_version(self):
        """Test case for list_issue_summary_of_project_version

        list  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
