# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.cloud_job_controller_api import CloudJobControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestCloudJobControllerApi(unittest.TestCase):
    """CloudJobControllerApi unit test stubs"""

    def setUp(self):
        self.api = CloudJobControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_cancel_cloud_job(self):
        """Test case for cancel_cloud_job

        Cancel a job  # noqa: E501
        """
        pass

    def test_do_action_cloud_job(self):
        """Test case for do_action_cloud_job

        doAction  # noqa: E501
        """
        pass

    def test_list_cloud_job(self):
        """Test case for list_cloud_job

        list  # noqa: E501
        """
        pass

    def test_read_cloud_job(self):
        """Test case for read_cloud_job

        read  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
