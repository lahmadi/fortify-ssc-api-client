# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.models.cloud_system_metrics import CloudSystemMetrics  # noqa: E501
from swagger_client.rest import ApiException


class TestCloudSystemMetrics(unittest.TestCase):
    """CloudSystemMetrics unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testCloudSystemMetrics(self):
        """Test CloudSystemMetrics"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.cloud_system_metrics.CloudSystemMetrics()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
