# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.custom_tag_of_issue_template_controller_api import CustomTagOfIssueTemplateControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestCustomTagOfIssueTemplateControllerApi(unittest.TestCase):
    """CustomTagOfIssueTemplateControllerApi unit test stubs"""

    def setUp(self):
        self.api = CustomTagOfIssueTemplateControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_list_custom_tag_of_issue_template(self):
        """Test case for list_custom_tag_of_issue_template

        list  # noqa: E501
        """
        pass

    def test_update_custom_tag_of_issue_template(self):
        """Test case for update_custom_tag_of_issue_template

        update  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
