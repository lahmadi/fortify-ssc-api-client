# coding: utf-8

"""
    Fortify Software Security Center API

    No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)  # noqa: E501

    OpenAPI spec version: 1:21.1.2.0005
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.api.rulepack_update_controller_api import RulepackUpdateControllerApi  # noqa: E501
from swagger_client.rest import ApiException


class TestRulepackUpdateControllerApi(unittest.TestCase):
    """RulepackUpdateControllerApi unit test stubs"""

    def setUp(self):
        self.api = RulepackUpdateControllerApi()  # noqa: E501

    def tearDown(self):
        pass

    def test_do_rulepack_update(self):
        """Test case for do_rulepack_update

        DoImport  # noqa: E501
        """
        pass


if __name__ == '__main__':
    unittest.main()
