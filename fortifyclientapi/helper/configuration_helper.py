#!/usr/bin/env python

# coding: utf-8

from fortifyclientapi.sscclientapi.configuration import Configuration


class ConfigurationHelper(Configuration):
    """Helping API Configuration 
    """

    def configure(self, username: str = "", password: str = "", api_key: str = "", token_id: int = None, api_url: str = "", ssl_cert_verification: bool = True, ssl_ca_cert: str = None, ssl_client_cert: str = None, ssl_client_key: str = None, ssl_hostname_verification: bool = False, logging_debug: bool = False, logging_logfile: str = ""):
        self.username = username
        self.password = password
        if api_key != "" and api_key != None:
            self.api_key['Authorization'] = api_key
            self.api_key_prefix['Authorization'] = "FortifyToken"
        self.host = api_url
        self.verify_ssl = ssl_cert_verification
        self.ssl_ca_cert = ssl_ca_cert
        self.cert_file = ssl_client_cert
        self.key_file = ssl_client_key
        # Deprecated in UrlLib3
        self.assert_hostname = None
        self.debug = logging_debug
        self.logger_file = logging_logfile

        self.token_id = token_id

        return self
