#!/usr/bin/env python

# coding: utf-8


from hashlib import md5
from datetime import datetime
from genericpath import isfile
from os.path import normpath, abspath, isfile
from fortifyclientapi.controller.attribute_definition_controller import AttributeDefinitionController
from fortifyclientapi.controller.controller_exception import ControllerException
from fortifyclientapi.helper.helper import Helper
from fortifyclientapi.helper.helper_exception import HelperException
from fortifyclientapi.sscclientapi.models.attribute import Attribute
from fortifyclientapi.sscclientapi.models.attribute_definition import AttributeDefinition
from fortifyclientapi.sscclientapi.models.attribute_option import AttributeOption


class AttributeHelper(Helper):

    def configure_project_version_attribute_by_id(
        self,
        attribute_definition_id: int,
        value: str = None,
        value_names: list[str] = None
    ) -> Attribute:
        values: list[AttributeOption] = None

        try:
            attribute_definition: AttributeDefinition = self.get_controller(controller_type=AttributeDefinitionController).get_attribute_definition_by_id(
                id=attribute_definition_id
            )

            if attribute_definition.type == "SINGLE" or attribute_definition.type == "MULTIPLE":
                value = "null"
                values = self.get_controller(controller_type=AttributeDefinitionController).get_attribute_option_by_name(
                    attribute_definition_id=attribute_definition.id, value_names=value_names)
            else:
                values = []

                if attribute_definition.type == "TEXT" or \
                        attribute_definition.type == "LONG_TEXT" or \
                        attribute_definition.type == "SENSITIVE_TEXT":
                    value = value if value else ""

                elif attribute_definition.type == "INTEGER":
                    try:
                        value = value if int(value) else ""
                    except ValueError as int_err:
                        raise HelperException(
                            "AttributeHelper",
                            status="error",
                            reason="{}".format(int_err)
                        )

                elif attribute_definition.type == "DATE":
                    try:
                        value = value if datetime.strptime(
                            value, "%m/%d/%Y") else ""
                    except ValueError as int_err:
                        raise HelperException(
                            "AttributeHelper",
                            status="error",
                            reason="{}".format(int_err)
                        )

                elif attribute_definition.type == "BOOLEAN":
                    try:
                        value = "true" if value.lower() in [
                            "yes", "true", "1", "on"] else "false"
                    except ValueError as int_err:
                        raise HelperException(
                            "AttributeHelper",
                            status="error",
                            reason="{}".format(int_err)
                        )

                elif attribute_definition.type == "FILE":
                    try:
                        if isfile(
                                abspath(normpath(value))):
                            with open(abspath(normpath(value)), "rb") as file:
                                value = file.read()
                        else:
                            value = ""
                    except (OSError, ValueError, FileNotFoundError, FileExistsError) as int_err:
                        raise HelperException(
                            "AttributeHelper",
                            status="error",
                            reason="{}".format(int_err)
                        )
                else:
                    raise HelperException(
                        "AttributeHelper",
                        status="error",
                        reason="Unknown Attribute Definition Type {}".format(
                            attribute_definition.type)
                    )

            return Attribute(
                attribute_definition_id=attribute_definition.id,
                value=value if value else "null",
                values=values if values else []
            )
        except (HelperException, ControllerException, ValueError) as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

    def configure_project_version_attribute_by_name(
        self,
        attribute_definition_name: str,
        value: str = None,
        value_names: list[str] = None
    ) -> Attribute:
        try:
            return self.configure_project_version_attribute_by_id(
                attribute_definition_id=self.get_controller(controller_type=AttributeDefinitionController).get_attribute_definition_by_name_or_guid(
                    attribute_definition_name).id,
                value=value,
                value_names=value_names)
        except HelperException as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

    def get_default_required_attributes(
        self
    ) -> list[Attribute]:
        list_default_required_attributes: list[Attribute] = []

        try:
            for required_attribute in self.get_controller(controller_type=AttributeDefinitionController).get_required_attribute_definition():
                list_default_required_attributes += [Attribute(
                    attribute_definition_id=required_attribute.id,
                    value="" if required_attribute.options == None else "null",
                    values=[required_attribute.options[0]
                            ] if required_attribute.options != None else []
                )]
        except (HelperException, ControllerException, ValueError) as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        return list_default_required_attributes

    def configure_attribute_definition(
        self,
        attribute_definition_id: int = None,
        app_entity_type: str = "PROJECT_VERSION",
        category: str = "TECHNICAL",
        description: str = "",
        guid: str = None,
        has_default: bool = False,
        hidden: bool = False,
        name: str = None,
        options: list[AttributeOption] = None,
        required: bool = False,
        system_usage: str = "USER_DEFINED_DELETABLE",
        type: str = None
    ) -> AttributeDefinition:
        attribute_definition: AttributeDefinition = None

        try:
            if attribute_definition_id:
                attribute_definition = self.get_controller(
                    controller_type=AttributeDefinitionController).get_attribute_definition_by_id(attribute_definition_id)
            elif name:
                attribute_definition = self.get_controller(
                    controller_type=AttributeDefinitionController).get_attribute_definition_by_name_or_guid(name)
            elif guid:
                attribute_definition = self.get_controller(
                    controller_type=AttributeDefinitionController).get_attribute_definition_by_name_or_guid(name)
            else:
                raise HelperException(
                    "AttributeHelper",
                    status="error",
                    reason="Neither Attribute Definition Id nor Attribute Definition Guid nor Attribute Definition Name specified"
                )
        except ControllerException as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        try:
            if attribute_definition == None:
                attribute_definition = AttributeDefinition(
                    app_entity_type=app_entity_type or "PROJECT_VERSION",
                    category=category or "TECHNICAL",
                    description=description or "",
                    guid=guid or md5(name.encode(
                        encoding="UTF-8")).hexdigest(),
                    has_default=has_default or False,
                    hidden=hidden or False,
                    name=name or guid,
                    options=options if options and type in [
                        "SINGLE", "MULTIPLE"] else None or None,
                    required=required or False,
                    system_usage=system_usage or "USER_DEFINED_DELETABLE",
                    type=type or "SIGNLE" if options is not None else "TEXT"
                )
            else:
                attribute_definition.description = description or attribute_definition.description
                attribute_definition.hidden = hidden or attribute_definition.hidden
                attribute_definition.required = required or attribute_definition.required
                attribute_definition.options = options if options and attribute_definition.type in [
                    "SINGLE", "MULTIPLE"] else attribute_definition.options or attribute_definition.options

        except ValueError as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        return attribute_definition

    def configure_attribute_option(
        self,
        name: str,
        guid: str = None,
        description: str = "",
        hidden: bool = False,
        index: int = 1,
        attribute_definition_id: int = None,
        attribute_definition_name: str = None
    ) -> AttributeOption:
        attribute_option: AttributeOption = None
        attribute_definition: AttributeDefinition = None

        try:
            if attribute_definition_id:
                attribute_definition = self.get_controller(controller_type=AttributeDefinitionController).get_attribute_definition_by_id(
                    attribute_definition_id
                )
            elif attribute_definition_name:
                attribute_definition = self.get_controller(controller_type=AttributeDefinitionController).get_attribute_definition_by_name(
                    attribute_definition_name
                )

            if attribute_definition is not None:
                for option in attribute_definition.options:
                    if name == option.name:
                        attribute_option = option
                        break
        except ControllerException as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        try:
            if attribute_option is None:
                attribute_option = AttributeOption(
                    description=description or "",
                    guid=guid if guid is not None and guid != "" else md5(
                        name.encode(encoding="UTF-8")).hexdigest(),
                    hidden=hidden or False,
                    index=index or 1,
                    name=name
                )
            else:
                attribute_option.name = name or attribute_option.name
                attribute_option.guid = guid if guid is not None and guid != "" else attribute_option.guid
                attribute_option.hidden = hidden or attribute_option.hidden
                attribute_option.description = description or attribute_option.description
                attribute_option.index = index or attribute_option.index
        except ValueError as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        return attribute_option

    def create_attribute_definition(self, attribute_definition: AttributeDefinition) -> AttributeDefinition:
        try:
            attribute_definition: AttributeDefinition = self.get_controller(controller_type=AttributeDefinitionController).create_attribute_definition(
                attribute_definition=attribute_definition
            )
        except ControllerException as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        return attribute_definition

    def update_attribute_definition(self, attribute_definition: AttributeDefinition) -> AttributeDefinition:
        try:
            attribute_definition: AttributeDefinition = self.get_controller(controller_type=AttributeDefinitionController).update_attribute_definition(
                attribute_definition=attribute_definition
            )
        except ControllerException as e:
            raise HelperException(
                "AttributeHelper",
                status="error",
                reason="{}".format(e)
            )

        return attribute_definition
