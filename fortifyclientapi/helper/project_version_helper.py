#!/usr/bin/env python

# coding: utf-8


from datetime import datetime
from fortifyclientapi.controller.controller_exception import ControllerException
from fortifyclientapi.controller.custom_tag_controller import CustomTagController
from fortifyclientapi.controller.issue_template_controller import IssueTemplateController
from fortifyclientapi.controller.project_version_attribute_controller import ProjectVersionAttributeController
from fortifyclientapi.controller.project_version_controller import ProjectVersionController
from fortifyclientapi.controller.project_version_result_processing_rule_controller import \
    ProjectVersionResultProcessingRuleController
from fortifyclientapi.helper.helper import Helper
from fortifyclientapi.helper.helper_exception import HelperException
from fortifyclientapi.sscclientapi import ApiResultListProjectVersionIssue
from fortifyclientapi.sscclientapi.models.attribute import Attribute
from fortifyclientapi.sscclientapi.models.custom_tag import CustomTag
from fortifyclientapi.sscclientapi.models.issue_template import IssueTemplate
from fortifyclientapi.sscclientapi.models.project import Project
from fortifyclientapi.sscclientapi.models.project_version import ProjectVersion
from fortifyclientapi.sscclientapi.models.result_processing_rule import ResultProcessingRule


class ProjectVersionHelper(Helper):

    def configure_project_version(
            self,
            project_name: str,
            version_name: str,
            project_version_id: int = None,
            active: bool = True,
            auto_predict: bool = False,
            bug_tracker_enabled: bool = False,
            bug_tracker_plugin_name: str = "",
            created_by: str = "",
            creation_date: datetime = datetime.now(),
            custom_tag_values_auto_apply: bool = True,
            description: str = "REST API - FortifyClientAPI",
            issue_template_name: str = "Prioritized High Risk Issue Template",
            issue_template_id: str = "Prioritized-HighRisk-Project-Template",
            custom_tag_name: str = "Analysis",
            refresh_required: bool = True,
            server_version: float = 0,
            snapshot_out_of_date: bool = False,
            stale_issue_template: bool = False
    ) -> ProjectVersion:
        issue_template: IssueTemplate = None
        custom_tag: CustomTag = None
        project_version: ProjectVersion = None
        project: Project = None

        try:
            if issue_template_id != "Prioritized-HighRisk-Project-Template":
                issue_template = self.get_controller(controller_type=IssueTemplateController).get_issue_template_by_id(
                    issue_template_id)
            elif issue_template_name != "Prioritized High Risk Issue Template":
                issue_template = self.get_controller(
                    controller_type=IssueTemplateController).get_issue_template_by_name(
                    issue_template_name)
            else:
                issue_template = self.get_controller(controller_type=IssueTemplateController).get_issue_template_by_id(
                    "Prioritized-HighRisk-Project-Template")

            # Todo: bug tracker id resolution
        except (HelperException, ControllerException) as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

        try:
            custom_tag = self.get_controller(controller_type=CustomTagController).get_custom_tag_by_name(
                custom_tag_name)

            # Todo: bug tracker id resolution
        except (HelperException, ControllerException) as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

        try:
            if project_version_id == None:
                project_version = self.get_controller(
                    controller_type=ProjectVersionController).get_project_version_by_name(
                    project_name=project_name, project_version_name=version_name)

                project = self.get_controller(controller_type=ProjectVersionController).get_project_by_name(
                    project_name=project_name)
            else:
                project_version = self.get_controller(
                    controller_type=ProjectVersionController).get_project_version_by_id(
                    id=project_version_id)
        except (HelperException, ControllerException) as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

        try:
            if project_version == None:
                if project == None:
                    project = Project(
                        creation_date=creation_date,
                        created_by=created_by,
                        description=description,
                        issue_template_id=issue_template.id,
                        name=project_name
                    )

                project_version = ProjectVersion(
                    active=active,
                    auto_predict=auto_predict,
                    bug_tracker_enabled=bug_tracker_enabled,
                    bug_tracker_plugin_id=bug_tracker_plugin_name,
                    committed=False,
                    created_by=created_by,
                    creation_date=datetime.now(),
                    custom_tag_values_auto_apply=custom_tag_values_auto_apply,
                    description=description,
                    issue_template_name=issue_template.name,
                    issue_template_id=issue_template.id,
                    issue_template_modified_time=int((datetime.now(
                    ) - datetime(1970, 1, 1)).total_seconds()),
                    master_attr_guid=custom_tag.guid,
                    name=version_name,
                    project=project,
                    refresh_required=refresh_required,
                    server_version=server_version,
                    snapshot_out_of_date=snapshot_out_of_date,
                    stale_issue_template=stale_issue_template
                )
            else:
                project_version.active = project_version.active if project_version.active is not None else active
                project_version.auto_predict = project_version.auto_predict or auto_predict
                project_version.bug_tracker_enabled = project_version.bug_tracker_enabled or bug_tracker_enabled
                project_version.bug_tracker_plugin_id = project_version.bug_tracker_plugin_id or bug_tracker_plugin_name or ""
                project_version.committed = False
                project_version.created_by = project_version.created_by or created_by
                project_version.creation_date = project_version.creation_date or creation_date
                project_version.custom_tag_values_auto_apply = custom_tag_values_auto_apply
                project_version.description = project_version.description or description or ""
                project_version.issue_template_id = project_version.issue_template_id or issue_template.id or ""
                project_version.issue_template_modified_time = project_version.issue_template_modified_time or int(
                    (datetime.now(
                    ) - datetime(1970, 1, 1)).total_seconds())
                project_version.issue_template_name = project_version.issue_template_name or issue_template.name or ""
                project_version.master_attr_guid = project_version.master_attr_guid or custom_tag.guid or ""
                project_version.refresh_required = project_version.refresh_required or refresh_required
                project_version.server_version = project_version.server_version or server_version or 0
                project_version.snapshot_out_of_date = project_version.snapshot_out_of_date or snapshot_out_of_date
                project_version.stale_issue_template = project_version.stale_issue_template or stale_issue_template
        except ValueError as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

        return project_version

    def create_project_version(
            self,
            project_version: ProjectVersion
    ) -> ProjectVersion:
        try:
            return self.get_controller(controller_type=ProjectVersionController).create_project_version(
                project_version=project_version
            )
        except (HelperException, ControllerException) as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

    def update_project_version(
            self,
            project_version: ProjectVersion,
            attributes: list[Attribute] = [],
            processing_rules: list[ResultProcessingRule] = []
    ) -> ProjectVersion:
        try:
            # Add attributes
            if len(attributes) > 0:
                self.get_controller(
                    controller_type=ProjectVersionAttributeController).create_or_update_project_version_attributes(
                    project_version.id,
                    *attributes
                )

            # Add processing rules
            if len(processing_rules) > 0:
                self.get_controller(
                    controller_type=ProjectVersionResultProcessingRuleController).update_project_version_result_processing_rule(
                    project_version.id,
                    *processing_rules
                )

            return self.get_controller(controller_type=ProjectVersionController).update_project_version(
                project_version=project_version
            )
        except (HelperException, ControllerException) as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

    def configure_project_version_result_processing_rule(
            self,
            project_version_id: int,
            processing_rule_identifier: str,
            processing_rule_enabled: bool,
            processing_rule_display_name: str = None,
            processing_rule_displayable: bool = True
    ) -> ResultProcessingRule:
        try:
            return self.get_controller(
                controller_type=ProjectVersionResultProcessingRuleController).configure_project_version_result_processing_rule(
                project_version_id,
                processing_rule_identifier,
                processing_rule_enabled,
                processing_rule_display_name,
                processing_rule_displayable)
        except ControllerException as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )

    def get_issues_project_version(self,
                                   project_version_id: int,
                                   query: str, show_hidden: bool,
                                   show_suppressed: bool,
                                   show_removed: bool
                                   ) -> ApiResultListProjectVersionIssue:
        try:
            return self.get_controller(controller_type=ProjectVersionController).get_project_issues_list(
                project_version_id=project_version_id,
                query=query,
                show_suppressed=show_suppressed,
                show_hidden=show_hidden,
                show_removed=show_removed
            )
        except ControllerException as e:
            raise HelperException(
                "ProjectVersionHelper",
                status="error",
                reason="{}".format(e)
            )


# def configure_project_version_user_access(
#     self,
#     project_version_id: int,

# )
