#!/usr/bin/env python

# coding: utf-8


from typing import overload
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.controller.controller_exception import ControllerException
from fortifyclientapi.sscclientapi.endpoints.attribute_definition_controller_api import AttributeDefinitionControllerApi
from fortifyclientapi.sscclientapi.models.api_result_attribute_definition import ApiResultAttributeDefinition
from fortifyclientapi.sscclientapi.models.api_result_list_attribute_definition import ApiResultListAttributeDefinition
from fortifyclientapi.sscclientapi.models.attribute_definition import AttributeDefinition
from fortifyclientapi.sscclientapi.models.attribute_option import AttributeOption
from fortifyclientapi.sscclientapi.rest import ApiException


class AttributeDefinitionController(Controller):
    """Attribute Definition API Controller
    """

    def get_attribute_definition_by_id(self, id: int) -> AttributeDefinition:
        attribute_definition: AttributeDefinition = None

        try:
            response: ApiResultAttributeDefinition = AttributeDefinitionControllerApi(self.api).read_attribute_definition(
                id=id, async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                attribute_definition = response.data
            else:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return attribute_definition

    def get_attribute_definition_by_name(self, name: str) -> AttributeDefinition:
        attribute_definition: AttributeDefinition = None

        try:
            response: ApiResultListAttributeDefinition = AttributeDefinitionControllerApi(self.api).list_attribute_definition(
                limit=1, q="name:{}".format(name), async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                attribute_definition = response.data[0] if response.data and len(
                    response.data) > 0 else None
            else:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return attribute_definition

    def get_attribute_definition_by_guid(self, guid: str) -> AttributeDefinition:
        attribute_definition: AttributeDefinition = None

        try:
            response: ApiResultListAttributeDefinition = AttributeDefinitionControllerApi(self.api).list_attribute_definition(
                limit=1, q="guid:{}".format(guid), async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                attribute_definition = response.data[0] if response.data and len(
                    response.data) > 0 else None
            else:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return attribute_definition

    def get_attribute_definition_by_name_or_guid(self, name_or_guid: str) -> AttributeDefinition:
        attribute_definition: AttributeDefinition = None
        try:
            attribute_definition = self.get_attribute_definition_by_name(
                name=name_or_guid)

        except ControllerException as e:
            try:
                attribute_definition = self.get_attribute_definition_by_guid(
                    guid=name_or_guid)
            except ControllerException as enext:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="error",
                    reason="{} in\n{}".format(enext, e)
                )

        return attribute_definition

    def get_required_attribute_definition(self) -> list[AttributeDefinition]:
        list_required_attribute_definition: list[AttributeDefinition] = []

        try:
            response: ApiResultListAttributeDefinition = AttributeDefinitionControllerApi(self.api).list_attribute_definition(
                limit=-1, async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                for attribute_definition in response.data:
                    if attribute_definition.required:
                        list_required_attribute_definition += [
                            attribute_definition]
            else:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return list_required_attribute_definition

    def get_attribute_option_by_name(
        self,
        attribute_definition_id: int,
        value_names: list[str]
    ) -> list[AttributeOption]:
        list_attribute_options: list[AttributeOption] = []

        try:
            attribute_definition: AttributeDefinition = self.get_attribute_definition_by_id(
                id=attribute_definition_id)

            for option in attribute_definition.options:
                if option.name in value_names:
                    list_attribute_options += [option]
        except ControllerException as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return list_attribute_options

    def get_attribute_option_by_guid(
        self,
        attribute_definition_id: int,
        value_guids: list[str]
    ) -> list[AttributeOption]:
        list_attribute_options: list[AttributeOption] = []

        try:
            attribute_definition: AttributeDefinition = self.get_attribute_definition_by_id(
                id=attribute_definition_id)

            for option in attribute_definition.options:
                if option.guid in value_guids:
                    list_attribute_options += [option]
        except ControllerException as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return list_attribute_options

    def create_attribute_definition(self, attribute_definition: AttributeDefinition) -> AttributeDefinition:
        attribute: AttributeDefinition = None

        try:
            response: ApiResultAttributeDefinition = AttributeDefinitionControllerApi(self.api).create_attribute_definition(
                body=attribute_definition, async_req=False
            )

            if response.error_code == None and response.response_code == 201:
                attribute = response.data
            else:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return attribute

    def update_attribute_definition(self, attribute_definition: AttributeDefinition) -> AttributeDefinition:
        attribute: AttributeDefinition = None

        try:
            response: ApiResultAttributeDefinition = AttributeDefinitionControllerApi(self.api).update_attribute_definition(
                id=attribute_definition.id, body=attribute_definition, async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                attribute = response.data
            else:
                raise ControllerException(
                    "AttributeDefinitionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "AttributeDefinitionController",
                status="error",
                reason="{}".format(e)
            )

        return attribute
