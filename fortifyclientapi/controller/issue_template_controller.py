#!/usr/bin/env python

# coding: utf-8

from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.sscclientapi.endpoints.issue_template_controller_api import \
    IssueTemplateControllerApi
from fortifyclientapi.sscclientapi.models.api_result_issue_template import \
    ApiResultIssueTemplate
from fortifyclientapi.sscclientapi.models.api_result_list_issue_template import ApiResultListIssueTemplate
from fortifyclientapi.sscclientapi.models.issue_template import IssueTemplate
from fortifyclientapi.sscclientapi.rest import ApiException

from fortifyclientapi.controller.controller_exception import ControllerException


class IssueTemplateController(Controller):
    """Issue Template API Controller
    """

    def get_issue_template_by_name(self, name: str) -> IssueTemplate:
        """Get the Issue Template by a given name

        Args:
            name (str): Issue Template name

        Raises:
            ControllerException: Issue Template API Controller exception

        Returns:
            IssueTemplate: the issue template found. None otherwise
        """
        issue_template: IssueTemplate = None
        
        try:
            response: ApiResultListIssueTemplate = IssueTemplateControllerApi(self.api).list_issue_template(
                async_req=False, limit=1, q="name:{}".format(name)
            )

            if response.error_code == None and response.response_code == 200:
                issue_template = response.data[0] if response.data != [
                ] and response.data != None else None
            else:
                raise ControllerException(
                    "IssueTemplateController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "IssueTemplateController",
                status="error",
                reason="{}".format(e)
            )

        return issue_template

    def get_issue_template_by_id(self, id: str) -> IssueTemplate:
        """Get the Issue Template by a given id

        Args:
            id (str): Issue Template ID

        Raises:
            ControllerException: Issue Template API Controller exception

        Returns:
            IssueTemplate: the issue template found
        """
        issue_template: IssueTemplate = None

        try:
            response: ApiResultIssueTemplate = IssueTemplateControllerApi(self.api).read_issue_template(
                async_req=False, id=id
            )

            if response.error_code == None and response.response_code == 200:
                issue_template = response.data
            else:
                raise ControllerException(
                    "IssueTemplateController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "IssueTemplateController",
                status="error",
                reason="{}".format(e)
            )

        return issue_template
