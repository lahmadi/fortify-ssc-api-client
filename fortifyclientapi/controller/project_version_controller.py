#!/usr/bin/env python

# coding: utf-8

from datetime import datetime
from typing import overload
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.sscclientapi import ApiResultListProjectVersionIssue, IssueOfProjectVersionControllerApi
from fortifyclientapi.sscclientapi.endpoints.project_controller_api import ProjectControllerApi

from fortifyclientapi.sscclientapi.endpoints.project_version_controller_api import \
    ProjectVersionControllerApi
from fortifyclientapi.sscclientapi.models.api_result_list_project import ApiResultListProject
from fortifyclientapi.sscclientapi.models.api_result_list_project_version import \
    ApiResultListProjectVersion
from fortifyclientapi.sscclientapi.models.api_result_project import ApiResultProject
from fortifyclientapi.sscclientapi.models.api_result_project_version import \
    ApiResultProjectVersion
from fortifyclientapi.sscclientapi.models.api_result_void import ApiResultVoid
from fortifyclientapi.sscclientapi.models.project import Project
from fortifyclientapi.sscclientapi.models.project_version import ProjectVersion
from fortifyclientapi.sscclientapi.models.project_version_copy_current_state_request import \
    ProjectVersionCopyCurrentStateRequest
from fortifyclientapi.sscclientapi.models.project_version_copy_partial_request import \
    ProjectVersionCopyPartialRequest
from fortifyclientapi.sscclientapi.rest import ApiException

from fortifyclientapi.controller.controller_exception import ControllerException


class ProjectVersionController(Controller):
    """Project Version API Controller
    """

    def create_project_version(
            self, project_version: ProjectVersion) -> ProjectVersion:
        """Create the project and the version
        For a full creation, update the app version

        Args:
            project_version (ProjectVersion): [description]

        Raises:
            ControllerException: Error during creation

        Returns:
            ProjectVersion: ProjectVersion
        """

        # App Version Creation but not commited
        # Before committing attributes, processing rules, etc. must be set up
        try:
            response: ApiResultProjectVersion = ProjectVersionControllerApi(self.api).create_project_version(
                project_version, async_req=False
            )

            if response.error_code == None and response.response_code == 201:
                project_version = response.data
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return project_version

    def update_project_version(self, project_version: ProjectVersion) -> ProjectVersion:
        """Update/commit the project version given by the `project_version`

        Args:
            project_version (ProjectVersion): [description]

        Raises:
            ControllerException: Error during the commit

        Returns:
            ProjectVersion: ProjectVersion
        """
        project_version.committed = True

        try:
            response: ApiResultProjectVersion = ProjectVersionControllerApi(self.api).update_project_version(
                project_version, project_version.id, async_req=False
            )

            if response.error_code == None and response.response_code == 200:
                project_version = response.data
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )

        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return project_version

    def get_project_version_by_name(self, project_name: str, project_version_name: str) -> ProjectVersion:
        """Get the Project Version by a given name

        Args:
            project_name (str): Project name
            project_version_name (str): Version names

        Raises:
            ControllerException: Project Version API Controller exception

        Returns:
            ProjectVersion: the Project Version found
        """
        project_version: ProjectVersion = None

        try:
            response: ApiResultListProjectVersion = ProjectVersionControllerApi(self.api).list_project_version(
                async_req=False, q="project.name:{}".format(project_name)
            )

            if response.error_code == None and response.response_code == 200:
                for pv in response.data:
                    if pv.project.name == project_name and pv.name == project_version_name:
                        project_version = pv
                        break
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return project_version

    def get_project_version_by_id(self, id: int) -> ProjectVersion:
        """Get the Project Version by a given id

        Args:
            id (int): Project Version ID

        Raises:
            ControllerException: Project Version API Controller exception

        Returns:
            ProjectVersion: the Project Version found
        """
        project_version: ProjectVersion = None

        try:
            response: ApiResultProjectVersion = ProjectVersionControllerApi(self.api).read_project_version(
                async_req=False, id=id
            )

            if response.error_code == None and response.response_code == 200:
                project_version = response.data
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return project_version

    def copy_version_by_id(
            self,
            source_project_version_id: int,
            destination_project_version_id: int,
            copy_analysis_processing_rules: bool = True,
            copy_bug_tracker_configuration: bool = True,
            copy_custom_tags: bool = True
    ) -> None:
        """Copy Analysis Processing Rules, Bug Track Conf or/and Custom Tags from Project Version Source to Project Version Destination

        Args:
            source_project_version_id (int): [description]
            destination_project_version_id (int): [description]
            copy_analysis_processing_rules (bool, optional): [description]. Defaults to True.
            copy_bug_tracker_configuration (bool, optional): [description]. Defaults to True.
            copy_custom_tags (bool, optional): [description]. Defaults to True.

        Raises:
            ControllerException: [description]
        """
        try:
            project_version_copy_partial_request: ProjectVersionCopyPartialRequest = ProjectVersionCopyPartialRequest(
                copy_analysis_processing_rules=copy_analysis_processing_rules,
                copy_bug_tracker_configuration=copy_bug_tracker_configuration,
                copy_custom_tags=copy_custom_tags,
                previous_project_version_id=source_project_version_id,
                project_version_id=destination_project_version_id
            )

            response: ApiResultVoid = ProjectVersionControllerApi(self.api).copy_project_version(
                project_version_copy_partial_request, async_req=False
            )

            if response.error_code != None or response.response_code != 200:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )

        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

    def copy_version_by_name(
            self,
            source_project_name: str,
            source_project_version_name: str,
            destination_project_name: str,
            destination_project_version_name: str,
            copy_analysis_processing_rules: bool = True,
            copy_bug_tracker_configuration: bool = True,
            copy_custom_tags: bool = True
    ) -> None:
        """Copy Analysis Processing Rules, Bug Track Conf or/and Custom Tags from Project Version Source to Project Version Destination

        Args:
            source_project_name (str): [description]
            source_project_version_name (str): [description]
            destination_project_name (str): [description]
            destination_project_version_name (str): [description]
            copy_analysis_processing_rules (bool, optional): [description]. Defaults to True.
            copy_bug_tracker_configuration (bool, optional): [description]. Defaults to True.
            copy_custom_tags (bool, optional): [description]. Defaults to True.

        Raises:
            ControllerException: [description]
        """
        try:
            self.copy_version_by_id(
                source_project_version_id=self.get_project_version_by_name(
                    project_name=source_project_name, project_version_name=source_project_version_name).id,
                destination_project_version_id=self.get_project_version_by_name(
                    project_name=destination_project_name, project_version_name=destination_project_version_name).id,
                copy_analysis_processing_rules=copy_analysis_processing_rules,
                copy_bug_tracker_configuration=copy_bug_tracker_configuration,
                copy_custom_tags=copy_custom_tags
            )

        except ControllerException as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

    def copy_current_state_by_id(
            self,
            source_project_version_id: int,
            destination_project_version_id: int
    ) -> None:
        """Copy current state from Project Version Source to Project Version Destination

        Args:
            source_project_version_name (int): [description]
            destination_project_version_name (int): [description]

        Raises:
            ControllerException: [description]
        """
        try:
            project_version_copy_current_state_request: ProjectVersionCopyCurrentStateRequest = ProjectVersionCopyCurrentStateRequest(
                previous_project_version_id=source_project_version_id,
                project_version_id=destination_project_version_id
            )

            response: ApiResultVoid = ProjectVersionControllerApi(self.api).copy_current_state_for_project_version(
                project_version_copy_current_state_request, async_req=False
            )

            if response.error_code != None or response.response_code != 200:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code: {}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )

        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

    def copy_current_state_by_name(
            self,
            source_project_name: str,
            source_project_version_name: str,
            destination_project_name: str,
            destination_project_version_name: str
    ) -> None:
        """Copy current state from Project Version Source to Project Version Destination

        Args:
            source_project_name (str): [description]
            source_project_version_name (str): [description]
            destination_project_name (str): [description]
            destination_project_version_name (str): [description]

        Raises:
            ControllerException: [description]
        """
        try:
            self.copy_current_state_by_id(
                source_project_version_id=self.get_project_version_by_name(
                    source_project_name, source_project_version_name).id,
                destination_project_version_id=self.get_project_version_by_name(
                    destination_project_name, destination_project_version_name).id
            )

        except ControllerException as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

    def get_project_by_id(self, id: int) -> Project:
        project: Project = None

        try:
            response: ApiResultProject = ProjectControllerApi(self.api).read_project(
                async_req=False, id=id
            )

            if response.error_code == None and response.response_code == 200:
                project = response.data
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return project

    def get_project_by_name(self, project_name: str) -> Project:
        project: Project = None

        try:
            response: ApiResultListProject = ProjectControllerApi(self.api).list_project(
                async_req=False, limit=1, q="name:{}".format(project_name)
            )

            if response.error_code == None and response.response_code == 200:
                project = response.data[0] if response.data != [
                ] and response.data != None else None
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return project

    def get_project_issues_list(self,
                                project_version_id: int,
                                query: str,
                                show_hidden: bool = False,
                                show_removed: bool = False,
                                show_suppressed: bool = False,
                                ) -> ApiResultListProjectVersionIssue:
        issues_list: ApiResultListProjectVersionIssue = None

        try:
            if query:
                response: ApiResultListProjectVersionIssue = IssueOfProjectVersionControllerApi(
                    self.api).list_issue_of_project_version(
                    parent_id=project_version_id,
                    async_req=False,
                    limit=0,
                    q=query,
                    qm="issues",
                    showhidden=show_hidden,
                    showremoved=show_removed,
                    showsuppressed=show_suppressed
                )
            else:
                response: ApiResultListProjectVersionIssue = IssueOfProjectVersionControllerApi(
                    self.api).list_issue_of_project_version(
                    parent_id=project_version_id,
                    async_req=False,
                    limit=0,
                    showhidden=show_hidden,
                    showremoved=show_removed,
                    showsuppressed=show_suppressed
                )
            if response.error_code is None and response.response_code == 200:
                issues_list = response if response.data != [
                ] and response.data is not None else None
            else:
                raise ControllerException(
                    "ProjectVersionController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionController",
                status="error",
                reason="{}".format(e)
            )

        return issues_list
