#!/usr/bin/env python

# coding: utf-8

from typing import overload
from fortifyclientapi.controller.controller import Controller
from fortifyclientapi.sscclientapi.endpoints.result_processing_rule_of_project_version_controller_api import ResultProcessingRuleOfProjectVersionControllerApi
from fortifyclientapi.sscclientapi.models.api_result_list_result_processing_rule import ApiResultListResultProcessingRule
from fortifyclientapi.sscclientapi.models.result_processing_rule import ResultProcessingRule
from fortifyclientapi.sscclientapi.rest import ApiException
from fortifyclientapi.controller.controller_exception import ControllerException


class ProjectVersionResultProcessingRuleController(Controller):
    """Project Version Result Processing Rule API Controller
    """

    def get_project_version_result_processing_rule(self, project_version_id: int) -> list[ResultProcessingRule]:
        """Get the List of Project Version Result Processing Rule

        Args:
            project_version_id (int): [description]

        Raises:
            ControllerException: [description]
        """
        list_result_processing_rule: list[ResultProcessingRule] = None

        try:
            response: ApiResultListResultProcessingRule = ResultProcessingRuleOfProjectVersionControllerApi(
                self.api).list_result_processing_rule_of_project_version(parent_id=project_version_id, async_req=False)

            if response.error_code == None and response.response_code == 200:
                list_result_processing_rule = response.data
            else:
                raise ControllerException(
                    "ProjectVersionResultProcessingRuleController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionResultProcessingRuleController",
                status="error",
                reason="{}".format(e)
            )

        return list_result_processing_rule

    def update_project_version_result_processing_rule(self, project_version_id: int, *processing_rules: ResultProcessingRule) -> list[ResultProcessingRule]:
        """Update Result Processing Rule of a given Project Version

        Args:
            project_version_id (int): [description]
            *processing_rules (list[ResultProcessingRule]): one or more ResultProcessingRule object to update

        Raises:
            ControllerException: [description]

        Returns:
            list(ResultProcessingRule): [description]
        """

        list_result_processing_rule: list[ResultProcessingRule] = None

        try:
            response: ApiResultListResultProcessingRule = ResultProcessingRuleOfProjectVersionControllerApi(
                self.api).update_collection_result_processing_rule_of_project_version(async_req=False, parent_id=project_version_id, body=processing_rules)

            if response.error_code == None and response.response_code == 200:
                list_result_processing_rule = response.data
            else:
                raise ControllerException(
                    "ProjectVersionResultProcessingRuleController",
                    status="Return code: {} - Error code:{}".format(
                        str(response.response_code), str(response.error_code)),
                    reason=response.message
                )
        except (ApiException, ValueError) as e:
            raise ControllerException(
                "ProjectVersionResultProcessingRuleController",
                status="error",
                reason="{}".format(e)
            )

        return list_result_processing_rule

    def configure_project_version_result_processing_rule(
        self,
        project_version_id: int,
        processing_rule_identifier: str,
        processing_rule_enabled: bool,
        processing_rule_display_name: str = None,
        processing_rule_displayable: bool = True
    ) -> ResultProcessingRule:
        result_processing_rule: ResultProcessingRule = None

        try:
            list_result_processing_rule: list[ResultProcessingRule] = self.get_project_version_result_processing_rule(
                project_version_id=project_version_id
            )

            for processing_rule in list_result_processing_rule:
                if processing_rule_identifier == processing_rule.identifier:
                    result_processing_rule = processing_rule
                    if processing_rule_enabled is not None:
                        result_processing_rule.enabled = processing_rule_enabled
                    if processing_rule_display_name is not None:
                        result_processing_rule.display_name = processing_rule_display_name
                    if processing_rule_displayable is not None:
                        result_processing_rule.displayable = processing_rule_displayable
                    break
        except (ControllerException, ValueError) as e:
            raise ControllerException(
                "ResultProcessingRuleHelper",
                status="error",
                reason="{}".format(e)
            )

        return result_processing_rule
