# fortifyclientapi.sscclientapi.JobControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                 | HTTP request                    | Description  |
| ------------------------------------------------------ | ------------------------------- | ------------ |
| [**cancel_job**](JobControllerApi.md#cancel_job)       | **POST** /jobs/action/cancel    | Cancel a job |
| [**do_action_job**](JobControllerApi.md#do_action_job) | **POST** /jobs/{jobName}/action | doAction     |
| [**list_job**](JobControllerApi.md#list_job)           | **GET** /jobs                   | list         |
| [**read_job**](JobControllerApi.md#read_job)           | **GET** /jobs/{jobName}         | read         |
| [**update_job**](JobControllerApi.md#update_job)       | **PUT** /jobs/{jobName}         | update       |

# **cancel_job**
> ApiResultVoid cancel_job(body)

Cancel a job

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.JobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.JobCancelRequest() # JobCancelRequest | resource

try:
    # Cancel a job
    api_response = api_instance.cancel_job(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling JobControllerApi->cancel_job: %s\n" % e)
```

### Parameters

| Name     | Type                                        | Description | Notes |
| -------- | ------------------------------------------- | ----------- | ----- |
| **body** | [**JobCancelRequest**](JobCancelRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_job**
> ApiResultApiActionResponse do_action_job(body, job_name)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.JobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiResourceAction() # ApiResourceAction | resourceAction
job_name = 'job_name_example' # str | jobName

try:
    # doAction
    api_response = api_instance.do_action_job(body, job_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling JobControllerApi->do_action_job: %s\n" % e)
```

### Parameters

| Name         | Type                                          | Description    | Notes |
| ------------ | --------------------------------------------- | -------------- | ----- |
| **body**     | [**ApiResourceAction**](ApiResourceAction.md) | resourceAction |
| **job_name** | **str**                                       | jobName        |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_job**
> ApiResultListJob list_job(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.JobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_job(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling JobControllerApi->list_job: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListJob**](ApiResultListJob.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_job**
> ApiResultJob read_job(job_name, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.JobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
job_name = 'job_name_example' # str | jobName
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_job(job_name, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling JobControllerApi->read_job: %s\n" % e)
```

### Parameters

| Name         | Type    | Description   | Notes      |
| ------------ | ------- | ------------- | ---------- |
| **job_name** | **str** | jobName       |
| **fields**   | **str** | Output fields | [optional] |

### Return type

[**ApiResultJob**](ApiResultJob.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_job**
> ApiResultJob update_job(body, job_name)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.JobControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.Job() # Job | resource
job_name = 'job_name_example' # str | jobName

try:
    # update
    api_response = api_instance.update_job(body, job_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling JobControllerApi->update_job: %s\n" % e)
```

### Parameters

| Name         | Type              | Description | Notes |
| ------------ | ----------------- | ----------- | ----- |
| **body**     | [**Job**](Job.md) | resource    |
| **job_name** | **str**           | jobName     |

### Return type

[**ApiResultJob**](ApiResultJob.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

