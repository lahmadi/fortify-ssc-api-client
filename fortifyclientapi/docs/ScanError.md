# ScanError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **str** |  | 
**error_description** | **str** |  | 
**id** | **int** |  | 
**scan_id** | **int** | id of scan associated with the error | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

