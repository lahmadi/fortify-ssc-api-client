# CloudPoolStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active_worker_count** | **int** |  | [optional] 
**idle_worker_count** | **int** |  | [optional] 
**inactive_worker_count** | **int** |  | [optional] 
**pending_job_count** | **int** |  | [optional] 
**processing_worker_count** | **int** |  | [optional] 
**project_version_count** | **int** |  | [optional] 
**running_job_count** | **int** |  | [optional] 
**stale_worker_count** | **int** |  | [optional] 
**total_worker_count** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

