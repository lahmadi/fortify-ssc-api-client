# fortifyclientapi.sscclientapi.UserPreferencesControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                 | HTTP request                | Description                                                                                                     |
| -------------------------------------------------------------------------------------- | --------------------------- | --------------------------------------------------------------------------------------------------------------- |
| [**post_user_preferences**](UserPreferencesControllerApi.md#post_user_preferences)     | **POST** /userSession/prefs | Retrieve the current user&#x27;s session preferences. (The &#x27;username&#x27; parameter is not yet supported) |
| [**update_user_preferences**](UserPreferencesControllerApi.md#update_user_preferences) | **PUT** /userSession/prefs  | update                                                                                                          |

# **post_user_preferences**
> ApiResultUserPreferences post_user_preferences(body)

Retrieve the current user's session preferences. (The 'username' parameter is not yet supported)

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserPreferencesControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = 'body_example' # str | username

try:
    # Retrieve the current user's session preferences. (The 'username' parameter is not yet supported)
    api_response = api_instance.post_user_preferences(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserPreferencesControllerApi->post_user_preferences: %s\n" % e)
```

### Parameters

| Name     | Type              | Description | Notes |
| -------- | ----------------- | ----------- | ----- |
| **body** | [**str**](str.md) | username    |

### Return type

[**ApiResultUserPreferences**](ApiResultUserPreferences.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_user_preferences**
> ApiResultUserPreferences update_user_preferences(body)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserPreferencesControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.UserPreferences() # UserPreferences | resource

try:
    # update
    api_response = api_instance.update_user_preferences(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserPreferencesControllerApi->update_user_preferences: %s\n" % e)
```

### Parameters

| Name     | Type                                      | Description | Notes |
| -------- | ----------------------------------------- | ----------- | ----- |
| **body** | [**UserPreferences**](UserPreferences.md) | resource    |

### Return type

[**ApiResultUserPreferences**](ApiResultUserPreferences.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

