# Attribute

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_definition_id** | **int** | ID required when referencing an existing Attribute Definition. | 
**id** | **int** | ID required when referencing an existing Attribute. | [optional] 
**value** | **str** | Attribute value - should be set if Attribute Definition type is NUMBER/STRING/BOOLEAN/DATE/FILE | 
**values** | [**list[AttributeOption]**](AttributeOption.md) | Attribute Options associated with Attribute Definition with type&#x3D;SINGLE/MULTIPLE | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

