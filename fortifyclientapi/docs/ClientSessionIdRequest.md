# ClientSessionIdRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_name** | **str** | Client name for which to retrieve the session ID | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

