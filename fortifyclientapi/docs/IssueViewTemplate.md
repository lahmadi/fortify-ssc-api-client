# IssueViewTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data_version** | **int** | Issue data version for which template is created. | [optional] 
**description** | **str** | Template description. | [optional] 
**engine_type** | **str** | Engine type for which template is created. | [optional] 
**id** | **int** | Unique identifier of the issue view template. | [optional] 
**metadata_id** | **int** | Id of the plugin meta data for which template is created. | [optional] 
**object_version** | **int** | Object version. | 
**template_data** | **list[list[TemplateItem]]** | Actual template JSON data. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

