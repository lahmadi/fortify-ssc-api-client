# ReportParameter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**identifier** | **str** | Report parameter identifier | 
**name** | **str** |  | [optional] 
**param_order** | **int** | Order in which the parameter should appear | [optional] 
**report_definition_id** | **int** | Report definition identifier | 
**report_parameter_options** | [**list[ReportParameterOption]**](ReportParameterOption.md) | Report parameter options | [optional] 
**type** | **str** | Report parameter type | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

