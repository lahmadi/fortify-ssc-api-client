# fortifyclientapi.sscclientapi.BugFilingRequirementsOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                                                 | HTTP request                                                            | Description                                                                                   |
| -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------- | --------------------------------------------------------------------------------------------- |
| [**do_action_bug_filing_requirements_of_project_version**](BugFilingRequirementsOfProjectVersionControllerApi.md#do_action_bug_filing_requirements_of_project_version)                 | **POST** /projectVersions/{parentId}/bugfilingrequirements/action       | doAction                                                                                      |
| [**list_bug_filing_requirements_of_project_version**](BugFilingRequirementsOfProjectVersionControllerApi.md#list_bug_filing_requirements_of_project_version)                           | **GET** /projectVersions/{parentId}/bugfilingrequirements               | list                                                                                          |
| [**login_bug_filing_requirements_of_project_version**](BugFilingRequirementsOfProjectVersionControllerApi.md#login_bug_filing_requirements_of_project_version)                         | **POST** /projectVersions/{parentId}/bugfilingrequirements/action/login | Authenticate to the bug tracking system and return the initial set of bug filing requirements |
| [**update_collection_bug_filing_requirements_of_project_version**](BugFilingRequirementsOfProjectVersionControllerApi.md#update_collection_bug_filing_requirements_of_project_version) | **PUT** /projectVersions/{parentId}/bugfilingrequirements               | updateCollection                                                                              |

# **do_action_bug_filing_requirements_of_project_version**
> ApiResultApiActionResponse do_action_bug_filing_requirements_of_project_version(body, parent_id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugFilingRequirementsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionstring() # ApiCollectionActionstring | collectionAction
parent_id = 789 # int | parentId

try:
    # doAction
    api_response = api_instance.do_action_bug_filing_requirements_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugFilingRequirementsOfProjectVersionControllerApi->do_action_bug_filing_requirements_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                          | Description      | Notes |
| ------------- | ------------------------------------------------------------- | ---------------- | ----- |
| **body**      | [**ApiCollectionActionstring**](ApiCollectionActionstring.md) | collectionAction |
| **parent_id** | **int**                                                       | parentId         |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_bug_filing_requirements_of_project_version**
> ApiResultListBugFilingRequirements list_bug_filing_requirements_of_project_version(parent_id, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugFilingRequirementsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_bug_filing_requirements_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugFilingRequirementsOfProjectVersionControllerApi->list_bug_filing_requirements_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultListBugFilingRequirements**](ApiResultListBugFilingRequirements.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **login_bug_filing_requirements_of_project_version**
> ApiResultBugFilingRequirementsResponse login_bug_filing_requirements_of_project_version(body, parent_id)

Authenticate to the bug tracking system and return the initial set of bug filing requirements

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugFilingRequirementsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.BugFilingRequirementsRequest() # BugFilingRequirementsRequest | resource
parent_id = 789 # int | parentId

try:
    # Authenticate to the bug tracking system and return the initial set of bug filing requirements
    api_response = api_instance.login_bug_filing_requirements_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugFilingRequirementsOfProjectVersionControllerApi->login_bug_filing_requirements_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                                | Description | Notes |
| ------------- | ------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**BugFilingRequirementsRequest**](BugFilingRequirementsRequest.md) | resource    |
| **parent_id** | **int**                                                             | parentId    |

### Return type

[**ApiResultBugFilingRequirementsResponse**](ApiResultBugFilingRequirementsResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_collection_bug_filing_requirements_of_project_version**
> ApiResultListBugFilingRequirements update_collection_bug_filing_requirements_of_project_version(body, parent_id, changed_param_identifier=changed_param_identifier)

updateCollection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugFilingRequirementsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.BugFilingRequirements()] # list[BugFilingRequirements] | data
parent_id = 789 # int | parentId
changed_param_identifier = 'changed_param_identifier_example' # str | changedParamIdentifier (optional)

try:
    # updateCollection
    api_response = api_instance.update_collection_bug_filing_requirements_of_project_version(body, parent_id, changed_param_identifier=changed_param_identifier)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugFilingRequirementsOfProjectVersionControllerApi->update_collection_bug_filing_requirements_of_project_version: %s\n" % e)
```

### Parameters

| Name                         | Type                                                        | Description            | Notes      |
| ---------------------------- | ----------------------------------------------------------- | ---------------------- | ---------- |
| **body**                     | [**list[BugFilingRequirements]**](BugFilingRequirements.md) | data                   |
| **parent_id**                | **int**                                                     | parentId               |
| **changed_param_identifier** | **str**                                                     | changedParamIdentifier | [optional] |

### Return type

[**ApiResultListBugFilingRequirements**](ApiResultListBugFilingRequirements.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

