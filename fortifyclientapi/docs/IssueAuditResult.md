# IssueAuditResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_tag_values** | [**list[CustomTag]**](CustomTag.md) | Custom tag values that are set for the issue. | 
**id** | **int** | Audited issue ID. | 
**issue_instance_id** | **str** | Issue instance ID. | 
**project_version_id** | **int** | ID of the application version the audited issue belongs to. | 
**revision** | **int** | Audited issue revision. | 
**suppressed** | **bool** | Is issue suppressed. | 
**user_name** | **str** | User assigned to the audited issue. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

