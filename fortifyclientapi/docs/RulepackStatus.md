# RulepackStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | Numeric processing code. | [optional] 
**message** | **str** | Processing message. | [optional] 
**rulepack_resource_id** | **int** | Identifier of successfully created rulepack. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

