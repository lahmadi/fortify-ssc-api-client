# fortifyclientapi.sscclientapi.CustomTagControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                           | HTTP request                | Description |
| -------------------------------------------------------------------------------- | --------------------------- | ----------- |
| [**create_custom_tag**](CustomTagControllerApi.md#create_custom_tag)             | **POST** /customTags        | create      |
| [**delete_custom_tag**](CustomTagControllerApi.md#delete_custom_tag)             | **DELETE** /customTags/{id} | delete      |
| [**list_custom_tag**](CustomTagControllerApi.md#list_custom_tag)                 | **GET** /customTags         | list        |
| [**multi_delete_custom_tag**](CustomTagControllerApi.md#multi_delete_custom_tag) | **DELETE** /customTags      | multiDelete |
| [**read_custom_tag**](CustomTagControllerApi.md#read_custom_tag)                 | **GET** /customTags/{id}    | read        |
| [**update_custom_tag**](CustomTagControllerApi.md#update_custom_tag)             | **PUT** /customTags/{id}    | update      |

# **create_custom_tag**
> ApiResultCustomTag create_custom_tag(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CustomTag() # CustomTag | data

try:
    # create
    api_response = api_instance.create_custom_tag(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagControllerApi->create_custom_tag: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**CustomTag**](CustomTag.md) | data        |

### Return type

[**ApiResultCustomTag**](ApiResultCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_custom_tag**
> ApiResultVoid delete_custom_tag(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_custom_tag(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagControllerApi->delete_custom_tag: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_custom_tag**
> ApiResultListCustomTag list_custom_tag(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_custom_tag(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagControllerApi->list_custom_tag: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListCustomTag**](ApiResultListCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_custom_tag**
> ApiResultVoid multi_delete_custom_tag(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_custom_tag(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagControllerApi->multi_delete_custom_tag: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_custom_tag**
> ApiResultCustomTag read_custom_tag(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_custom_tag(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagControllerApi->read_custom_tag: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultCustomTag**](ApiResultCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_custom_tag**
> ApiResultCustomTag update_custom_tag(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CustomTagControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CustomTag() # CustomTag | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_custom_tag(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CustomTagControllerApi->update_custom_tag: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**CustomTag**](CustomTag.md) | resource    |
| **id**   | **int**                       | id          |

### Return type

[**ApiResultCustomTag**](ApiResultCustomTag.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

