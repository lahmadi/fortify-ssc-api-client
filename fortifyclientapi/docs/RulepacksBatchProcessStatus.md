# RulepacksBatchProcessStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | Numeric processing code. | [optional] 
**message** | **str** | Processing message. | [optional] 
**statuses** | [**list[RulepacksBatchProcessStatus]**](RulepacksBatchProcessStatus.md) | Child rulepacks processing statuses. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

