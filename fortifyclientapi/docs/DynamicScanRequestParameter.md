# DynamicScanRequestParameter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_value_document_info_id** | **int** | The file value document info id for an upload file | [optional] 
**file_value_name** | **str** | The name of an uploaded file | [optional] 
**id** | **int** | The id of the parameter | [optional] 
**object_version** | **int** | The objecct version of this parameter | [optional] 
**parameter_definition** | [**DynamicScanRequestParameterDefinition**](DynamicScanRequestParameterDefinition.md) |  | [optional] 
**value** | **str** | The value of the parameter | [optional] 
**value_options** | [**list[DynamicScanRequestParameterOption]**](DynamicScanRequestParameterOption.md) | The name of the uploaded file | [optional] 
**values** | [**list[DynamicScanRequestParameterOption]**](DynamicScanRequestParameterOption.md) | A list of possible values for this parameter | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

