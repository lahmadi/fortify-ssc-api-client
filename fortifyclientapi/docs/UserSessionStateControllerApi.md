# fortifyclientapi.sscclientapi.UserSessionStateControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                  | HTTP request               | Description |
| --------------------------------------------------------------------------------------- | -------------------------- | ----------- |
| [**list_user_session_state**](UserSessionStateControllerApi.md#list_user_session_state) | **GET** /userSession/state | list        |
| [**put_user_session_state**](UserSessionStateControllerApi.md#put_user_session_state)   | **PUT** /userSession/state | put         |

# **list_user_session_state**
> ApiResultListUserSessionState list_user_session_state(start=start, limit=limit, q=q)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserSessionStateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)

try:
    # list
    api_response = api_instance.list_user_session_state(start=start, limit=limit, q=q)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserSessionStateControllerApi->list_user_session_state: %s\n" % e)
```

### Parameters

| Name      | Type    | Description                                                                                             | Notes                       |
| --------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **start** | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit** | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**     | **str** | A search query                                                                                          | [optional]                  |

### Return type

[**ApiResultListUserSessionState**](ApiResultListUserSessionState.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **put_user_session_state**
> ApiResultListUserSessionState put_user_session_state(body)

put

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserSessionStateControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.UserSessionState()] # list[UserSessionState] | resources

try:
    # put
    api_response = api_instance.put_user_session_state(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserSessionStateControllerApi->put_user_session_state: %s\n" % e)
```

### Parameters

| Name     | Type                                              | Description | Notes |
| -------- | ------------------------------------------------- | ----------- | ----- |
| **body** | [**list[UserSessionState]**](UserSessionState.md) | resources   |

### Return type

[**ApiResultListUserSessionState**](ApiResultListUserSessionState.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

