# fortifyclientapi.sscclientapi.ReportDefinitionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                | HTTP request                       | Description |
| ----------------------------------------------------------------------------------------------------- | ---------------------------------- | ----------- |
| [**delete_report_definition**](ReportDefinitionControllerApi.md#delete_report_definition)             | **DELETE** /reportDefinitions/{id} | delete      |
| [**list_report_definition**](ReportDefinitionControllerApi.md#list_report_definition)                 | **GET** /reportDefinitions         | list        |
| [**multi_delete_report_definition**](ReportDefinitionControllerApi.md#multi_delete_report_definition) | **DELETE** /reportDefinitions      | multiDelete |
| [**read_report_definition**](ReportDefinitionControllerApi.md#read_report_definition)                 | **GET** /reportDefinitions/{id}    | read        |
| [**update_report_definition**](ReportDefinitionControllerApi.md#update_report_definition)             | **PUT** /reportDefinitions/{id}    | update      |
| [**upload_report_definition**](ReportDefinitionControllerApi.md#upload_report_definition)             | **POST** /reportDefinitions        | upload      |

# **delete_report_definition**
> ApiResultVoid delete_report_definition(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_report_definition(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportDefinitionControllerApi->delete_report_definition: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_report_definition**
> ApiResultListReportDefinition list_report_definition(fields=fields, start=start, limit=limit, q=q, groupby=groupby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
groupby = 'groupby_example' # str | Fields to group by (optional)

try:
    # list
    api_response = api_instance.list_report_definition(fields=fields, start=start, limit=limit, q=q, groupby=groupby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportDefinitionControllerApi->list_report_definition: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **groupby** | **str** | Fields to group by                                                                                      | [optional]                  |

### Return type

[**ApiResultListReportDefinition**](ApiResultListReportDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_report_definition**
> ApiResultVoid multi_delete_report_definition(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_report_definition(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportDefinitionControllerApi->multi_delete_report_definition: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_report_definition**
> ApiResultReportDefinition read_report_definition(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_report_definition(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportDefinitionControllerApi->read_report_definition: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultReportDefinition**](ApiResultReportDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_report_definition**
> ApiResultReportDefinition update_report_definition(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ReportDefinition() # ReportDefinition | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_report_definition(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportDefinitionControllerApi->update_report_definition: %s\n" % e)
```

### Parameters

| Name     | Type                                        | Description | Notes |
| -------- | ------------------------------------------- | ----------- | ----- |
| **body** | [**ReportDefinition**](ReportDefinition.md) | resource    |
| **id**   | **int**                                     | id          |

### Return type

[**ApiResultReportDefinition**](ApiResultReportDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **upload_report_definition**
> ApiResultReportDefinition upload_report_definition(file, name, type, rendering_engine=rendering_engine, description=description)

upload

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ReportDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
file = 'file_example' # str | 
name = 'name_example' # str | name
type = 'type_example' # str | type
rendering_engine = 'BIRT' # str | renderingEngine (optional) (default to BIRT)
description = 'description_example' # str | description (optional)

try:
    # upload
    api_response = api_instance.upload_report_definition(file, name, type, rendering_engine=rendering_engine, description=description)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ReportDefinitionControllerApi->upload_report_definition: %s\n" % e)
```

### Parameters

| Name                 | Type    | Description     | Notes                        |
| -------------------- | ------- | --------------- | ---------------------------- |
| **file**             | **str** |                 |
| **name**             | **str** | name            |
| **type**             | **str** | type            |
| **rendering_engine** | **str** | renderingEngine | [optional] [default to BIRT] |
| **description**      | **str** | description     | [optional]                   |

### Return type

[**ApiResultReportDefinition**](ApiResultReportDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

