# BulkResponseItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**request** | [**ApiBulkRequestItem**](ApiBulkRequestItem.md) |  | [optional] 
**responses** | [**list[ApiBulkResponseItemResponse]**](ApiBulkResponseItemResponse.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

