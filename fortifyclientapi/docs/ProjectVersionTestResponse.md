# ProjectVersionTestResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**found** | **bool** | Whether the application version was found | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

