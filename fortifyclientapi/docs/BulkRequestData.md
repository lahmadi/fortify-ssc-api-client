# BulkRequestData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**requests** | [**list[ApiBulkRequestItem]**](ApiBulkRequestItem.md) | A list of bulk request items that represents requests that could have been issued independently (or use HATEOAS \&quot;response\&quot; scheme requests that map to individual requests). | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

