# ProjectVersionIssue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analyzer** | **str** | Analyzer | 
**audited** | **bool** | Flag is set for issues that has been audited and primary tag value was set for this issue. | [optional] 
**bug_url** | **str** | Bug url | 
**confidence** | **float** | Issue confidence | 
**display_engine_type** | **str** | Display name for engine type | 
**engine_category** | **str** | Engine category | 
**engine_type** | **str** | Engine type | 
**external_bug_id** | **str** | Identifying information for the bug in the external bug tracking system. The actual format depends on the bug tracker plugin which filed the bug. | 
**folder_guid** | **str** | Issue folder globally unique identifier | 
**folder_id** | **int** | Deprecated - Issue folder identifier.  This may be incorrect or invalid.  Please use folderGuid instead. | 
**found_date** | **datetime** | Date when issue found | 
**friority** | **str** | Friority | 
**full_file_name** | **str** | Full file name where issue found | 
**has_attachments** | **bool** | Set to true if issue has attachments | 
**has_comments** | **bool** | Set to true if issue has audit comments | [optional] 
**has_correlated_issues** | **bool** | Set to true if issue has other correlated issues | 
**hidden** | **bool** | Set to true if issue is hidden | 
**id** | **int** | Application version issue identifier | [optional] 
**impact** | **float** | Issue impact | 
**issue_instance_id** | **str** | Issue instance identifier | 
**issue_name** | **str** | Issue name | 
**issue_status** | **str** | Flag represents issue review status and can have 3 types of values: Unreviewed, Under Review, Reviewed. | [optional] 
**kingdom** | **str** | Kingdom | 
**last_scan_id** | **int** | ID of the latest scan that found the issue | [optional] 
**likelihood** | **float** | Likelihood of issue | 
**line_number** | **int** | Line number where issue found | 
**primary_location** | **str** | Issue primary location | 
**primary_rule_guid** | **str** | Primary rule global unique identifier | 
**primary_tag** | **str** | Issue primary tag | 
**primary_tag_value_auto_applied** | **bool** | Flag equals true if value of custom tag was applied automatically | [optional] 
**project_name** | **str** | Application name | 
**project_version_id** | **int** | Application version identifier | 
**project_version_name** | **str** | Application version name | 
**removed** | **bool** | Set to true if issue is suppressed | 
**removed_date** | **datetime** | Date when issue removed | 
**reviewed** | **str** | Issue reviewer | 
**revision** | **int** | Application version revision | 
**scan_status** | **str** | Scan status | 
**severity** | **float** | Issue severity | 
**suppressed** | **bool** | Set to true if issue is suppressed | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

