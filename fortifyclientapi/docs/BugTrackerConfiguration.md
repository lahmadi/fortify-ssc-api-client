# BugTrackerConfiguration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Description | 
**display_label** | **str** | Configuration name | 
**identifier** | **str** | Identifier | 
**required** | **bool** | Set to true if configuration is required | 
**value** | **str** | Configuration value | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

