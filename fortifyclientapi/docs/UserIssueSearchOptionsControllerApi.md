# fortifyclientapi.sscclientapi.UserIssueSearchOptionsControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                          | HTTP request                    | Description |
| --------------------------------------------------------------------------------------------------------------- | ------------------------------- | ----------- |
| [**get_user_issue_search_options**](UserIssueSearchOptionsControllerApi.md#get_user_issue_search_options)       | **GET** /userIssueSearchOptions | get         |
| [**update_user_issue_search_options**](UserIssueSearchOptionsControllerApi.md#update_user_issue_search_options) | **PUT** /userIssueSearchOptions | update      |

# **get_user_issue_search_options**
> ApiResultUserIssueSearchOptions get_user_issue_search_options()

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserIssueSearchOptionsControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # get
    api_response = api_instance.get_user_issue_search_options()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserIssueSearchOptionsControllerApi->get_user_issue_search_options: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultUserIssueSearchOptions**](ApiResultUserIssueSearchOptions.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_user_issue_search_options**
> ApiResultUserIssueSearchOptions update_user_issue_search_options(body)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserIssueSearchOptionsControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.UserIssueSearchOptions() # UserIssueSearchOptions | resource

try:
    # update
    api_response = api_instance.update_user_issue_search_options(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserIssueSearchOptionsControllerApi->update_user_issue_search_options: %s\n" % e)
```

### Parameters

| Name     | Type                                                    | Description | Notes |
| -------- | ------------------------------------------------------- | ----------- | ----- |
| **body** | [**UserIssueSearchOptions**](UserIssueSearchOptions.md) | resource    |

### Return type

[**ApiResultUserIssueSearchOptions**](ApiResultUserIssueSearchOptions.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

