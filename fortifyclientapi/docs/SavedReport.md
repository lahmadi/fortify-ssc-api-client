# SavedReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**embed** | [**EmbeddedReportDefinition**](EmbeddedReportDefinition.md) |  | [optional] 
**auth_entity** | [**ReportAuthEntity**](ReportAuthEntity.md) |  | [optional] 
**format** | **str** | Saved report output format | 
**format_default_text** | **str** | Saved report output format default text | [optional] 
**generation_date** | **datetime** | Generation date | [optional] 
**id** | **int** |  | [optional] 
**input_report_parameters** | [**list[InputReportParameter]**](InputReportParameter.md) | List of report parameters | [optional] 
**is_published** | **bool** | Indicates whether saved report is published | [optional] 
**name** | **str** |  | [optional] 
**note** | **str** | Saved report notes | [optional] 
**projects** | [**list[ReportProject]**](ReportProject.md) | List of application versions | 
**published** | **bool** |  | [optional] 
**report_definition_id** | **int** | Report definition identifier | 
**report_projects_count** | **int** | Count of report applications | [optional] 
**status** | **str** | Saved report status | [optional] 
**status_default_text** | **str** | Saved report status default text | [optional] 
**type** | **str** | Saved report type | 
**type_default_text** | **str** | Saved report type default text | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

