# PerformanceIndicatorHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**id** | **str** | ID required when referencing an existing Performance Indicator History object | [optional] 
**name** | **str** |  | [optional] 
**range** | **str** | Range of values | [optional] 
**timestamp** | **datetime** |  | [optional] 
**value** | **float** | Required if referencing an existing Performance Indicator History  object | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

