# ObsoletePolicy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_policy** | **bool** | True if this obsolete policy is also configured as system default policy | [optional] 
**obsolete_av_count** | **int** | Count of application versions where this obsolete policy has been found | [optional] 
**policy_name** | **str** | Obsolete prediction policy name found in application version | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

