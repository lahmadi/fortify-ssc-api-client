# fortifyclientapi.sscclientapi.DataExportControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                  | HTTP request                                       | Description                              |
| ----------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------- | ---------------------------------------- |
| [**delete_data_export**](DataExportControllerApi.md#delete_data_export)                                                 | **DELETE** /dataExports/{id}                       | delete                                   |
| [**do_action_data_export**](DataExportControllerApi.md#do_action_data_export)                                           | **POST** /dataExports/action                       | doAction                                 |
| [**export_audit_to_csv_for_data_export**](DataExportControllerApi.md#export_audit_to_csv_for_data_export)               | **POST** /dataExports/action/exportAuditToCsv      | Export audit issues to csv               |
| [**export_issues_stats_to_csv_for_data_export**](DataExportControllerApi.md#export_issues_stats_to_csv_for_data_export) | **POST** /dataExports/action/exportIssueStatsToCsv | Export issue statistics to csv           |
| [**export_osc_to_csv_for_data_export**](DataExportControllerApi.md#export_osc_to_csv_for_data_export)                   | **POST** /dataExports/action/exportOscToCsv        | Export open source component data to csv |
| [**list_data_export**](DataExportControllerApi.md#list_data_export)                                                     | **GET** /dataExports                               | list                                     |

# **delete_data_export**
> ApiResultVoid delete_data_export(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DataExportControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_data_export(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataExportControllerApi->delete_data_export: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_data_export**
> ApiResultApiActionResponse do_action_data_export(body, appversionid=appversionid, datasetname=datasetname)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DataExportControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction
appversionid = 'appversionid_example' # str | appversionid (optional)
datasetname = 'datasetname_example' # str | datasetname (optional)

try:
    # doAction
    api_response = api_instance.do_action_data_export(body, appversionid=appversionid, datasetname=datasetname)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataExportControllerApi->do_action_data_export: %s\n" % e)
```

### Parameters

| Name             | Type                                                      | Description      | Notes      |
| ---------------- | --------------------------------------------------------- | ---------------- | ---------- |
| **body**         | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |
| **appversionid** | **str**                                                   | appversionid     | [optional] |
| **datasetname**  | **str**                                                   | datasetname      | [optional] |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **export_audit_to_csv_for_data_export**
> ApiResultVoid export_audit_to_csv_for_data_export(body)

Export audit issues to csv

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DataExportControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ExportAuditToCSVRequest() # ExportAuditToCSVRequest | resource

try:
    # Export audit issues to csv
    api_response = api_instance.export_audit_to_csv_for_data_export(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataExportControllerApi->export_audit_to_csv_for_data_export: %s\n" % e)
```

### Parameters

| Name     | Type                                                      | Description | Notes |
| -------- | --------------------------------------------------------- | ----------- | ----- |
| **body** | [**ExportAuditToCSVRequest**](ExportAuditToCSVRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **export_issues_stats_to_csv_for_data_export**
> ApiResultVoid export_issues_stats_to_csv_for_data_export(body)

Export issue statistics to csv

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DataExportControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ExportIssueStatsToCSVRequest() # ExportIssueStatsToCSVRequest | resource

try:
    # Export issue statistics to csv
    api_response = api_instance.export_issues_stats_to_csv_for_data_export(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataExportControllerApi->export_issues_stats_to_csv_for_data_export: %s\n" % e)
```

### Parameters

| Name     | Type                                                                | Description | Notes |
| -------- | ------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ExportIssueStatsToCSVRequest**](ExportIssueStatsToCSVRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **export_osc_to_csv_for_data_export**
> ApiResultVoid export_osc_to_csv_for_data_export(body)

Export open source component data to csv

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DataExportControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ExportOscToCsvRequest() # ExportOscToCsvRequest | resource

try:
    # Export open source component data to csv
    api_response = api_instance.export_osc_to_csv_for_data_export(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataExportControllerApi->export_osc_to_csv_for_data_export: %s\n" % e)
```

### Parameters

| Name     | Type                                                  | Description | Notes |
| -------- | ----------------------------------------------------- | ----------- | ----- |
| **body** | [**ExportOscToCsvRequest**](ExportOscToCsvRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_data_export**
> ApiResultListDataExport list_data_export(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DataExportControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_data_export(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DataExportControllerApi->list_data_export: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListDataExport**](ApiResultListDataExport.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

