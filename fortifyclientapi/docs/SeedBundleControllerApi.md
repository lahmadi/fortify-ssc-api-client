# fortifyclientapi.sscclientapi.SeedBundleControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                  | HTTP request          | Description |
| ----------------------------------------------------------------------- | --------------------- | ----------- |
| [**upload_seed_bundle**](SeedBundleControllerApi.md#upload_seed_bundle) | **POST** /seedBundles | upload      |

# **upload_seed_bundle**
> ApiResultVoid upload_seed_bundle(file)

upload

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.SeedBundleControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
file = 'file_example' # str | 

try:
    # upload
    api_response = api_instance.upload_seed_bundle(file)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling SeedBundleControllerApi->upload_seed_bundle: %s\n" % e)
```

### Parameters

| Name     | Type    | Description | Notes |
| -------- | ------- | ----------- | ----- |
| **file** | **str** |             |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

