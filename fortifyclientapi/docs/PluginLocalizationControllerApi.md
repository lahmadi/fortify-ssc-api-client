# fortifyclientapi.sscclientapi.PluginLocalizationControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                      | HTTP request                | Description |
| ------------------------------------------------------------------------------------------- | --------------------------- | ----------- |
| [**read_plugin_localization**](PluginLocalizationControllerApi.md#read_plugin_localization) | **GET** /pluginlocalization | read        |

# **read_plugin_localization**
> ApiResultPluginLocalization read_plugin_localization(engine_type, data_version, language)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.PluginLocalizationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
engine_type = 'engine_type_example' # str | engineType
data_version = 56 # int | dataVersion
language = 'language_example' # str | language

try:
    # read
    api_response = api_instance.read_plugin_localization(engine_type, data_version, language)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PluginLocalizationControllerApi->read_plugin_localization: %s\n" % e)
```

### Parameters

| Name             | Type    | Description | Notes |
| ---------------- | ------- | ----------- | ----- |
| **engine_type**  | **str** | engineType  |
| **data_version** | **int** | dataVersion |
| **language**     | **str** | language    |

### Return type

[**ApiResultPluginLocalization**](ApiResultPluginLocalization.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

