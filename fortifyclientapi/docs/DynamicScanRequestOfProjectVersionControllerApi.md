# fortifyclientapi.sscclientapi.DynamicScanRequestOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                  | HTTP request                                                           | Description                   |
| ------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------- | ----------------------------- |
| [**cancel_dynamic_scan_request_of_project_version**](DynamicScanRequestOfProjectVersionControllerApi.md#cancel_dynamic_scan_request_of_project_version) | **POST** /projectVersions/{parentId}/dynamicScanRequests/action/cancel | Cancel a dynamic scan request |
| [**create_dynamic_scan_request_of_project_version**](DynamicScanRequestOfProjectVersionControllerApi.md#create_dynamic_scan_request_of_project_version) | **POST** /projectVersions/{parentId}/dynamicScanRequests               | create                        |
| [**list_dynamic_scan_request_of_project_version**](DynamicScanRequestOfProjectVersionControllerApi.md#list_dynamic_scan_request_of_project_version)     | **GET** /projectVersions/{parentId}/dynamicScanRequests                | list                          |
| [**read_dynamic_scan_request_of_project_version**](DynamicScanRequestOfProjectVersionControllerApi.md#read_dynamic_scan_request_of_project_version)     | **GET** /projectVersions/{parentId}/dynamicScanRequests/{id}           | read                          |
| [**update_dynamic_scan_request_of_project_version**](DynamicScanRequestOfProjectVersionControllerApi.md#update_dynamic_scan_request_of_project_version) | **PUT** /projectVersions/{parentId}/dynamicScanRequests/{id}           | update                        |

# **cancel_dynamic_scan_request_of_project_version**
> ApiResultVoid cancel_dynamic_scan_request_of_project_version(body)

Cancel a dynamic scan request

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DynamicScanRequestOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.DynamicScanCancelRequest() # DynamicScanCancelRequest | resource

try:
    # Cancel a dynamic scan request
    api_response = api_instance.cancel_dynamic_scan_request_of_project_version(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DynamicScanRequestOfProjectVersionControllerApi->cancel_dynamic_scan_request_of_project_version: %s\n" % e)
```

### Parameters

| Name     | Type                                                        | Description | Notes |
| -------- | ----------------------------------------------------------- | ----------- | ----- |
| **body** | [**DynamicScanCancelRequest**](DynamicScanCancelRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **create_dynamic_scan_request_of_project_version**
> ApiResultDynamicScanRequest create_dynamic_scan_request_of_project_version(body, parent_id)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DynamicScanRequestOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.DynamicScanRequest() # DynamicScanRequest | resource
parent_id = 789 # int | parentId

try:
    # create
    api_response = api_instance.create_dynamic_scan_request_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DynamicScanRequestOfProjectVersionControllerApi->create_dynamic_scan_request_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                            | Description | Notes |
| ------------- | ----------------------------------------------- | ----------- | ----- |
| **body**      | [**DynamicScanRequest**](DynamicScanRequest.md) | resource    |
| **parent_id** | **int**                                         | parentId    |

### Return type

[**ApiResultDynamicScanRequest**](ApiResultDynamicScanRequest.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_dynamic_scan_request_of_project_version**
> ApiResultListDynamicScanRequest list_dynamic_scan_request_of_project_version(parent_id, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DynamicScanRequestOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_dynamic_scan_request_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DynamicScanRequestOfProjectVersionControllerApi->list_dynamic_scan_request_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultListDynamicScanRequest**](ApiResultListDynamicScanRequest.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_dynamic_scan_request_of_project_version**
> ApiResultDynamicScanRequest read_dynamic_scan_request_of_project_version(parent_id, id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DynamicScanRequestOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_dynamic_scan_request_of_project_version(parent_id, id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DynamicScanRequestOfProjectVersionControllerApi->read_dynamic_scan_request_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **id**        | **int** | id            |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultDynamicScanRequest**](ApiResultDynamicScanRequest.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_dynamic_scan_request_of_project_version**
> ApiResultDynamicScanRequest update_dynamic_scan_request_of_project_version(body, parent_id, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.DynamicScanRequestOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.DynamicScanRequest() # DynamicScanRequest | resource
parent_id = 789 # int | parentId
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_dynamic_scan_request_of_project_version(body, parent_id, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DynamicScanRequestOfProjectVersionControllerApi->update_dynamic_scan_request_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                            | Description | Notes |
| ------------- | ----------------------------------------------- | ----------- | ----- |
| **body**      | [**DynamicScanRequest**](DynamicScanRequest.md) | resource    |
| **parent_id** | **int**                                         | parentId    |
| **id**        | **int**                                         | id          |

### Return type

[**ApiResultDynamicScanRequest**](ApiResultDynamicScanRequest.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

