# IssueAgingPortlet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application_versions** | **int** |  | [optional] 
**average_days_to_remediate** | **int** |  | [optional] 
**average_days_to_review** | **int** |  | [optional] 
**files_scanned** | **int** |  | [optional] 
**issues_pending_review** | **int** |  | [optional] 
**issues_remediated** | **int** |  | [optional] 
**lines_of_code** | **int** |  | [optional] 
**open_issues** | **int** |  | [optional] 
**open_issues_reviewed** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

