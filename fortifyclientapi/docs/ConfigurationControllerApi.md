# fortifyclientapi.sscclientapi.ConfigurationControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                   | HTTP request                                                 | Description                                                          |
| ---------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ | -------------------------------------------------------------------- |
| [**get_configuration**](ConfigurationControllerApi.md#get_configuration)                                                                 | **GET** /configuration                                       | get                                                                  |
| [**get_current_authentication_info_configuration**](ConfigurationControllerApi.md#get_current_authentication_info_configuration)         | **GET** /configuration/currentAuthenticationInfo             | getCurrentAuthenticationInfo                                         |
| [**get_full_text_index_status_configuration**](ConfigurationControllerApi.md#get_full_text_index_status_configuration)                   | **GET** /configuration/searchIndex                           | getFullTextIndexStatus                                               |
| [**read_configuration**](ConfigurationControllerApi.md#read_configuration)                                                               | **GET** /configuration/{id}                                  | read                                                                 |
| [**refresh_audit_assistant_policies_configuration**](ConfigurationControllerApi.md#refresh_audit_assistant_policies_configuration)       | **POST** /configuration/action/refreshAuditAssistantPolicies | Refreshes prediction policies between Audit Assistant server and SSC |
| [**update_configuration**](ConfigurationControllerApi.md#update_configuration)                                                           | **PUT** /configuration                                       | update                                                               |
| [**validate_audit_assistant_connection_configuration**](ConfigurationControllerApi.md#validate_audit_assistant_connection_configuration) | **POST** /configuration/validateAuditAssistantConnection     | validateAuditAssistantConnection                                     |
| [**validate_report_connection_configuration**](ConfigurationControllerApi.md#validate_report_connection_configuration)                   | **POST** /configuration/validateReportConnection             | validateReportConnection                                             |

# **get_configuration**
> ApiResultConfigProperties get_configuration(group=group)

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
group = 'group_example' # str | group (optional)

try:
    # get
    api_response = api_instance.get_configuration(group=group)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->get_configuration: %s\n" % e)
```

### Parameters

| Name      | Type    | Description | Notes      |
| --------- | ------- | ----------- | ---------- |
| **group** | **str** | group       | [optional] |

### Return type

[**ApiResultConfigProperties**](ApiResultConfigProperties.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **get_current_authentication_info_configuration**
> ApiResultCurrentAuthenticationInfo get_current_authentication_info_configuration()

getCurrentAuthenticationInfo

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # getCurrentAuthenticationInfo
    api_response = api_instance.get_current_authentication_info_configuration()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->get_current_authentication_info_configuration: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultCurrentAuthenticationInfo**](ApiResultCurrentAuthenticationInfo.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **get_full_text_index_status_configuration**
> ApiResultSearchIndexStatus get_full_text_index_status_configuration()

getFullTextIndexStatus

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))

try:
    # getFullTextIndexStatus
    api_response = api_instance.get_full_text_index_status_configuration()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->get_full_text_index_status_configuration: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ApiResultSearchIndexStatus**](ApiResultSearchIndexStatus.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_configuration**
> ApiResultConfigProperty read_configuration(id)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 'id_example' # str | id

try:
    # read
    api_response = api_instance.read_configuration(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->read_configuration: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **str** | id          |

### Return type

[**ApiResultConfigProperty**](ApiResultConfigProperty.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **refresh_audit_assistant_policies_configuration**
> ApiResultRefreshAuditAssistantPoliciesRequest refresh_audit_assistant_policies_configuration(body)

Refreshes prediction policies between Audit Assistant server and SSC

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.RefreshAuditAssistantPoliciesRequest() # RefreshAuditAssistantPoliciesRequest | resource

try:
    # Refreshes prediction policies between Audit Assistant server and SSC
    api_response = api_instance.refresh_audit_assistant_policies_configuration(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->refresh_audit_assistant_policies_configuration: %s\n" % e)
```

### Parameters

| Name     | Type                                                                                | Description | Notes |
| -------- | ----------------------------------------------------------------------------------- | ----------- | ----- |
| **body** | [**RefreshAuditAssistantPoliciesRequest**](RefreshAuditAssistantPoliciesRequest.md) | resource    |

### Return type

[**ApiResultRefreshAuditAssistantPoliciesRequest**](ApiResultRefreshAuditAssistantPoliciesRequest.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_configuration**
> ApiResultConfigProperties update_configuration(body)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ConfigProperties() # ConfigProperties | resource

try:
    # update
    api_response = api_instance.update_configuration(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->update_configuration: %s\n" % e)
```

### Parameters

| Name     | Type                                        | Description | Notes |
| -------- | ------------------------------------------- | ----------- | ----- |
| **body** | [**ConfigProperties**](ConfigProperties.md) | resource    |

### Return type

[**ApiResultConfigProperties**](ApiResultConfigProperties.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **validate_audit_assistant_connection_configuration**
> ApiResultConfigProperties validate_audit_assistant_connection_configuration(body)

validateAuditAssistantConnection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ConfigProperties() # ConfigProperties | resource

try:
    # validateAuditAssistantConnection
    api_response = api_instance.validate_audit_assistant_connection_configuration(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->validate_audit_assistant_connection_configuration: %s\n" % e)
```

### Parameters

| Name     | Type                                        | Description | Notes |
| -------- | ------------------------------------------- | ----------- | ----- |
| **body** | [**ConfigProperties**](ConfigProperties.md) | resource    |

### Return type

[**ApiResultConfigProperties**](ApiResultConfigProperties.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **validate_report_connection_configuration**
> ApiResultConfigProperties validate_report_connection_configuration(body)

validateReportConnection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ConfigurationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ConfigProperties() # ConfigProperties | resource

try:
    # validateReportConnection
    api_response = api_instance.validate_report_connection_configuration(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ConfigurationControllerApi->validate_report_connection_configuration: %s\n" % e)
```

### Parameters

| Name     | Type                                        | Description | Notes |
| -------- | ------------------------------------------- | ----------- | ----- |
| **body** | [**ConfigProperties**](ConfigProperties.md) | resource    |

### Return type

[**ApiResultConfigProperties**](ApiResultConfigProperties.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

