# fortifyclientapi.sscclientapi.ProjectControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                   | HTTP request                   | Description                                                                   |
| ---------------------------------------------------------------------------------------- | ------------------------------ | ----------------------------------------------------------------------------- |
| [**do_collection_action_project**](ProjectControllerApi.md#do_collection_action_project) | **POST** /projects/action      | doCollectionAction                                                            |
| [**list_project**](ProjectControllerApi.md#list_project)                                 | **GET** /projects              | list                                                                          |
| [**read_project**](ProjectControllerApi.md#read_project)                                 | **GET** /projects/{id}         | read                                                                          |
| [**test_project**](ProjectControllerApi.md#test_project)                                 | **POST** /projects/action/test | Check whether the specified application name is already defined in the system |
| [**update_project**](ProjectControllerApi.md#update_project)                             | **PUT** /projects/{id}         | update                                                                        |

# **do_collection_action_project**
> ApiResultApiActionResponse do_collection_action_project(body)

doCollectionAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | apiResourceAction

try:
    # doCollectionAction
    api_response = api_instance.do_collection_action_project(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectControllerApi->do_collection_action_project: %s\n" % e)
```

### Parameters

| Name     | Type                                                      | Description       | Notes |
| -------- | --------------------------------------------------------- | ----------------- | ----- |
| **body** | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | apiResourceAction |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_project**
> ApiResultListProject list_project(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search-spec of full text search query (see fulltextsearch parameter) (optional)
fulltextsearch = false # bool | If 'true', interpret 'q' parameter as full text search query, defaults to 'false' (optional) (default to false)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_project(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectControllerApi->list_project: %s\n" % e)
```

### Parameters

| Name               | Type     | Description                                                                                                     | Notes                         |
| ------------------ | -------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **fields**         | **str**  | Output fields                                                                                                   | [optional]                    |
| **start**          | **int**  | A start offset in object listing                                                                                | [optional] [default to 0]     |
| **limit**          | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied         | [optional] [default to 200]   |
| **q**              | **str**  | A search-spec of full text search query (see fulltextsearch parameter)                                          | [optional]                    |
| **fulltextsearch** | **bool** | If &#x27;true&#x27;, interpret &#x27;q&#x27; parameter as full text search query, defaults to &#x27;false&#x27; | [optional] [default to false] |
| **orderby**        | **str**  | Fields to order by                                                                                              | [optional]                    |

### Return type

[**ApiResultListProject**](ApiResultListProject.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_project**
> ApiResultProject read_project(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_project(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectControllerApi->read_project: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultProject**](ApiResultProject.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **test_project**
> ApiResultApplicationNameTestResponse test_project(body)

Check whether the specified application name is already defined in the system

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApplicationNameTestRequest() # ApplicationNameTestRequest | resource

try:
    # Check whether the specified application name is already defined in the system
    api_response = api_instance.test_project(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectControllerApi->test_project: %s\n" % e)
```

### Parameters

| Name     | Type                                                            | Description | Notes |
| -------- | --------------------------------------------------------------- | ----------- | ----- |
| **body** | [**ApplicationNameTestRequest**](ApplicationNameTestRequest.md) | resource    |

### Return type

[**ApiResultApplicationNameTestResponse**](ApiResultApplicationNameTestResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_project**
> ApiResultProject update_project(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.Project() # Project | data
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_project(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectControllerApi->update_project: %s\n" % e)
```

### Parameters

| Name     | Type                      | Description | Notes |
| -------- | ------------------------- | ----------- | ----- |
| **body** | [**Project**](Project.md) | data        |
| **id**   | **int**                   | id          |

### Return type

[**ApiResultProject**](ApiResultProject.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

