# fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                 | HTTP request                            | Description |
| ---------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ----------- |
| [**create_bugfield_template_group**](BugfieldTemplateGroupControllerApi.md#create_bugfield_template_group)             | **POST** /bugfieldTemplateGroups        | create      |
| [**delete_bugfield_template_group**](BugfieldTemplateGroupControllerApi.md#delete_bugfield_template_group)             | **DELETE** /bugfieldTemplateGroups/{id} | delete      |
| [**list_bugfield_template_group**](BugfieldTemplateGroupControllerApi.md#list_bugfield_template_group)                 | **GET** /bugfieldTemplateGroups         | list        |
| [**multi_delete_bugfield_template_group**](BugfieldTemplateGroupControllerApi.md#multi_delete_bugfield_template_group) | **DELETE** /bugfieldTemplateGroups      | multiDelete |
| [**read_bugfield_template_group**](BugfieldTemplateGroupControllerApi.md#read_bugfield_template_group)                 | **GET** /bugfieldTemplateGroups/{id}    | read        |
| [**update_bugfield_template_group**](BugfieldTemplateGroupControllerApi.md#update_bugfield_template_group)             | **PUT** /bugfieldTemplateGroups/{id}    | update      |

# **create_bugfield_template_group**
> ApiResultBugfieldTemplateGroupDto create_bugfield_template_group(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.BugfieldTemplateGroupDto() # BugfieldTemplateGroupDto | data

try:
    # create
    api_response = api_instance.create_bugfield_template_group(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugfieldTemplateGroupControllerApi->create_bugfield_template_group: %s\n" % e)
```

### Parameters

| Name     | Type                                                        | Description | Notes |
| -------- | ----------------------------------------------------------- | ----------- | ----- |
| **body** | [**BugfieldTemplateGroupDto**](BugfieldTemplateGroupDto.md) | data        |

### Return type

[**ApiResultBugfieldTemplateGroupDto**](ApiResultBugfieldTemplateGroupDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_bugfield_template_group**
> ApiResultVoid delete_bugfield_template_group(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_bugfield_template_group(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugfieldTemplateGroupControllerApi->delete_bugfield_template_group: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_bugfield_template_group**
> ApiResultListBugfieldTemplateGroupDto list_bugfield_template_group(fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_bugfield_template_group(fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugfieldTemplateGroupControllerApi->list_bugfield_template_group: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultListBugfieldTemplateGroupDto**](ApiResultListBugfieldTemplateGroupDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_bugfield_template_group**
> ApiResultVoid multi_delete_bugfield_template_group(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_bugfield_template_group(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugfieldTemplateGroupControllerApi->multi_delete_bugfield_template_group: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_bugfield_template_group**
> ApiResultBugfieldTemplateGroupDto read_bugfield_template_group(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_bugfield_template_group(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugfieldTemplateGroupControllerApi->read_bugfield_template_group: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultBugfieldTemplateGroupDto**](ApiResultBugfieldTemplateGroupDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_bugfield_template_group**
> ApiResultBugfieldTemplateGroupDto update_bugfield_template_group(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugfieldTemplateGroupControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.BugfieldTemplateGroupDto() # BugfieldTemplateGroupDto | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_bugfield_template_group(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugfieldTemplateGroupControllerApi->update_bugfield_template_group: %s\n" % e)
```

### Parameters

| Name     | Type                                                        | Description | Notes |
| -------- | ----------------------------------------------------------- | ----------- | ----- |
| **body** | [**BugfieldTemplateGroupDto**](BugfieldTemplateGroupDto.md) | resource    |
| **id**   | **int**                                                     | id          |

### Return type

[**ApiResultBugfieldTemplateGroupDto**](ApiResultBugfieldTemplateGroupDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

