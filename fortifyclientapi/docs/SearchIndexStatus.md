# SearchIndexStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**configured** | **bool** | Is index configured | [optional] 
**healthy_index** | **bool** | Is the index Healthy | [optional] 
**indexing_job_running** | **bool** | Is the indexing job running | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

