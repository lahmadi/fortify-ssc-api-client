# ExportAuditToCSVRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**aggregate_by** | **str** | Aggregateby | [optional] 
**collapse_issues** | **bool** | Will collapse issues in exported files | [optional] 
**dataset_name** | **str** | Dataset name (Audit) | 
**file_name** | **str** | File name to save | 
**filter** | **str** | Filter | [optional] 
**filter_by** | **str** | Filterby | [optional] 
**filter_set** | **str** | Filterset | 
**include_comments_in_history** | **bool** | Will include comments in history | [optional] 
**include_hidden** | **bool** | Will include hidden issues | [optional] 
**include_removed** | **bool** | Will include removed issues | [optional] 
**include_suppressed** | **bool** | Will include suppressed issues | [optional] 
**issue_search** | **str** | Search issue query | [optional] 
**limit** | **int** | Limit | [optional] 
**note** | **str** | Note | [optional] 
**order_by** | **str** | Orderby | 
**project_version_id** | **int** | Application version id to export audit data | 
**restrict_to_users_issues** | **bool** | will restrict to user issues | [optional] 
**start** | **int** | Start | [optional] 
**use_short_file_names** | **bool** | Will use short file names | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

