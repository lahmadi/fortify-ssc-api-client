# DynamicScanCancelRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dynamic_scan_request_ids** | **list[int]** | List containing a single dynamic scan request id to cancel | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

