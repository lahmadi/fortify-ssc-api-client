# ReportLibrary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**file_doc_id** | **int** | Report library file doc identifier | [optional] 
**guid** | **str** | Report library unique identifier | 
**id** | **int** |  | [optional] 
**name** | **str** |  | [optional] 
**object_version** | **int** | Object version | [optional] 
**publish_version** | **int** | Publish version | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

