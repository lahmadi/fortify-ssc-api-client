# fortifyclientapi.sscclientapi.BugTrackerOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                                          | HTTP request                                                                      | Description                                                                                                                     |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------- |
| [**clear_bug_links_by_external_ids_bug_tracker_of_project_version**](BugTrackerOfProjectVersionControllerApi.md#clear_bug_links_by_external_ids_bug_tracker_of_project_version) | **POST** /projectVersions/{parentId}/bugtracker/action/clearBugLinksByExternalIds | Clear the specified bug references from the application version. (Does not affect the external bug tracking system in any way.) |
| [**do_action_bug_tracker_of_project_version**](BugTrackerOfProjectVersionControllerApi.md#do_action_bug_tracker_of_project_version)                                             | **POST** /projectVersions/{parentId}/bugtracker/action                            | doAction                                                                                                                        |
| [**list_bug_tracker_of_project_version**](BugTrackerOfProjectVersionControllerApi.md#list_bug_tracker_of_project_version)                                                       | **GET** /projectVersions/{parentId}/bugtracker                                    | list                                                                                                                            |
| [**test_bug_tracker_of_project_version**](BugTrackerOfProjectVersionControllerApi.md#test_bug_tracker_of_project_version)                                                       | **POST** /projectVersions/{parentId}/bugtracker/action/test                       | Validate that the user can authenticate to the bug tracking system using the provided configuration and credentials             |
| [**update_collection_bug_tracker_of_project_version**](BugTrackerOfProjectVersionControllerApi.md#update_collection_bug_tracker_of_project_version)                             | **PUT** /projectVersions/{parentId}/bugtracker                                    | updateCollection                                                                                                                |

# **clear_bug_links_by_external_ids_bug_tracker_of_project_version**
> ApiResultVoid clear_bug_links_by_external_ids_bug_tracker_of_project_version(body, parent_id)

Clear the specified bug references from the application version. (Does not affect the external bug tracking system in any way.)

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugTrackerOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionClearBugLinksRequest() # ProjectVersionClearBugLinksRequest | resource
parent_id = 789 # int | parentId

try:
    # Clear the specified bug references from the application version. (Does not affect the external bug tracking system in any way.)
    api_response = api_instance.clear_bug_links_by_external_ids_bug_tracker_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugTrackerOfProjectVersionControllerApi->clear_bug_links_by_external_ids_bug_tracker_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                                            | Description | Notes |
| ------------- | ------------------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**ProjectVersionClearBugLinksRequest**](ProjectVersionClearBugLinksRequest.md) | resource    |
| **parent_id** | **int**                                                                         | parentId    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_bug_tracker_of_project_version**
> ApiResultApiActionResponse do_action_bug_tracker_of_project_version(body, parent_id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugTrackerOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionstring() # ApiCollectionActionstring | collectionAction
parent_id = 789 # int | parentId

try:
    # doAction
    api_response = api_instance.do_action_bug_tracker_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugTrackerOfProjectVersionControllerApi->do_action_bug_tracker_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                          | Description      | Notes |
| ------------- | ------------------------------------------------------------- | ---------------- | ----- |
| **body**      | [**ApiCollectionActionstring**](ApiCollectionActionstring.md) | collectionAction |
| **parent_id** | **int**                                                       | parentId         |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_bug_tracker_of_project_version**
> ApiResultListProjectVersionBugTracker list_bug_tracker_of_project_version(parent_id, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugTrackerOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_bug_tracker_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugTrackerOfProjectVersionControllerApi->list_bug_tracker_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultListProjectVersionBugTracker**](ApiResultListProjectVersionBugTracker.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **test_bug_tracker_of_project_version**
> ApiResultVoid test_bug_tracker_of_project_version(body, parent_id)

Validate that the user can authenticate to the bug tracking system using the provided configuration and credentials

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugTrackerOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.BugTrackerRequest() # BugTrackerRequest | resource
parent_id = 789 # int | parentId

try:
    # Validate that the user can authenticate to the bug tracking system using the provided configuration and credentials
    api_response = api_instance.test_bug_tracker_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugTrackerOfProjectVersionControllerApi->test_bug_tracker_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                          | Description | Notes |
| ------------- | --------------------------------------------- | ----------- | ----- |
| **body**      | [**BugTrackerRequest**](BugTrackerRequest.md) | resource    |
| **parent_id** | **int**                                       | parentId    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_collection_bug_tracker_of_project_version**
> ApiResultListProjectVersionBugTracker update_collection_bug_tracker_of_project_version(body, parent_id)

updateCollection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.BugTrackerOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.ProjectVersionBugTracker()] # list[ProjectVersionBugTracker] | data
parent_id = 789 # int | parentId

try:
    # updateCollection
    api_response = api_instance.update_collection_bug_tracker_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling BugTrackerOfProjectVersionControllerApi->update_collection_bug_tracker_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                              | Description | Notes |
| ------------- | ----------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**list[ProjectVersionBugTracker]**](ProjectVersionBugTracker.md) | data        |
| **parent_id** | **int**                                                           | parentId    |

### Return type

[**ApiResultListProjectVersionBugTracker**](ApiResultListProjectVersionBugTracker.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

