# CloudSystemMetrics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**controller_disk_free** | **int** | Free disk space in controller&#x27;s job directory [bytes] | 
**controller_disk_used** | **int** | Disk space used by files in controller&#x27;s job directory [bytes] | 
**controller_start_time** | **datetime** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

