# AuthenticationCodeExchangeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_code** | **str** | Authentication Code | 
**code_verifier** | **str** | Code Verifier, high-entropy cryptographic random string matching regular expression ^[a-zA-Z0-9.~_-]{43,128}$ | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

