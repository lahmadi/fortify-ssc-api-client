# fortifyclientapi.sscclientapi.IssueAuditCommentControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                     | HTTP request      | Description |
| ------------------------------------------------------------------------------------------ | ----------------- | ----------- |
| [**list_issue_audit_comment**](IssueAuditCommentControllerApi.md#list_issue_audit_comment) | **GET** /comments | list        |

# **list_issue_audit_comment**
> ApiResultListIssueAuditComment list_issue_audit_comment(q, fulltextsearch, fields=fields, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAuditCommentControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
q = 'q_example' # str | A full text search query
fulltextsearch = 'true' # str | Only 'true' is supported (default to true)
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_issue_audit_comment(q, fulltextsearch, fields=fields, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAuditCommentControllerApi->list_issue_audit_comment: %s\n" % e)
```

### Parameters

| Name               | Type    | Description                                                                                             | Notes                       |
| ------------------ | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **q**              | **str** | A full text search query                                                                                |
| **fulltextsearch** | **str** | Only &#x27;true&#x27; is supported                                                                      | [default to true]           |
| **fields**         | **str** | Output fields                                                                                           | [optional]                  |
| **start**          | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**          | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListIssueAuditComment**](ApiResultListIssueAuditComment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

