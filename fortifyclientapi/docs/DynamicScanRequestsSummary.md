# DynamicScanRequestsSummary

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**can_edit_or_cancel** | **bool** | Flag that indicates whether the user may edit or cancel the dynamic scan request | [optional] 
**can_submit** | **bool** | Flag that indicates whether the user can submit dynamic scan requests | [optional] 
**dynamic_scan_disabled** | **bool** | Flag that indicates whether dynamic scan request is disabled | [optional] 
**dynamic_scan_disabled_message** | **str** | A message indicating the reason for why dynamic scan is disabled | [optional] 
**last_scan_id** | **int** | The id of the last dynamic scan request | [optional] 
**last_scan_summary** | **str** | Holds information about the last dynamic scan request | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

