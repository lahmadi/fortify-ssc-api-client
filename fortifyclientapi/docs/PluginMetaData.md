# PluginMetaData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_version** | **str** | Version string of the SSC plugin api used to develop the plugin | 
**data_version** | **int** | An integer used to tag the set of issue attributes provided by this plugin. | 
**description** | **str** | Plugin description | [optional] 
**engine_type** | **str** | Name of the scan engine supported by the plugin. Value is defined for parser plugins only | [optional] 
**id** | **int** | Plugin unique identifier | [optional] 
**last_used_of_kind** | **bool** | Tracks whether this plugin instance was the most recently used of its kind | 
**plugin_configuration** | [**list[PluginConfiguration]**](PluginConfiguration.md) | Additional configuration properties used by the plugin | 
**plugin_id** | **str** | Identifier of the plugin, usually a fully-qualified classname. Non-unique when multiple versions of same plugin exist | 
**plugin_name** | **str** | A string name for the plugin | 
**plugin_state** | **str** | State of the plugin instance | 
**plugin_type** | **str** | Denotes functionality of the plugin instance, such as scan parsing, bugtracker integration. | 
**plugin_version** | **str** | A version string of the implementation code of the plugin | 
**supported_engine_versions** | **str** | Versions of the scan engine results supported by the plugin. Value is defined for parser plugins only | [optional] 
**system_installed** | **bool** | whether the plugin instance was installed by adding the jar to a special system folder | 
**vendor_name** | **str** | Name of the company / organization that developed the plugin | [optional] 
**vendor_url** | **str** | Plugin vendor&#x27;s web site URL | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

