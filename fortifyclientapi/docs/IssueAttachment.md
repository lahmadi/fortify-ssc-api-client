# IssueAttachment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**file_doc_id** | **int** | document ID referencing the attachment | 
**id** | **int** |  | 
**image** | **str** |  | [optional] 
**original_file_name** | **str** |  | [optional] 
**update_time** | **datetime** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

