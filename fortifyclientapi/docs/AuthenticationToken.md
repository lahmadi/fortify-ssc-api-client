# AuthenticationToken

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**creation_date** | **datetime** | Date and time the token was created (in ISO 8601 format) | [optional] 
**description** | **str** | Purpose for which the token was requested. | [optional] 
**id** | **int** | Unique identifier of token | [optional] 
**remaining_usages** | **int** | The remaining number of api calls that can be made using this token. A value of -1 denotes unlimited number of calls. | [optional] 
**terminal_date** | **datetime** | Date and time the token expires (in ISO 8601 format). If not specified, it will default to the maximum lifetime for this token type. | [optional] 
**token** | **str** | String that represents the authentication token. (For security reasons, this value is null except for a successful token creation response.) | [optional] 
**type** | **str** | Token type | 
**username** | **str** | Username of token owner | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

