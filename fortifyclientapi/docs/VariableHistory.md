# VariableHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Variable id | [optional] 
**name** | **str** | Variable Name | 
**timestamp** | **datetime** | The Date the variable value was taken | 
**value** | **int** | Variable value | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

