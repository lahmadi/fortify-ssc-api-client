# IssueUpdateTagRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_tag_audit** | [**CustomTag**](CustomTag.md) |  | 
**issues** | [**list[EntityStateIdentifier]**](EntityStateIdentifier.md) | Issues to audit | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

