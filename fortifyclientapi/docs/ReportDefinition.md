# ReportDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cross_app** | **bool** | True if report applies to multiple application versions | [optional] 
**description** | **str** | Report description | [optional] 
**file_name** | **str** | The name of the report definition file | [optional] 
**guid** | **str** | Report definition GUID | [optional] 
**id** | **int** | Report definition identifier | [optional] 
**in_use** | **bool** | Indicates whether the report definition is in use | [optional] 
**name** | **str** | Report name | 
**object_version** | **int** | Object version | [optional] 
**parameters** | [**list[ReportParameter]**](ReportParameter.md) | List of report parameters | [optional] 
**publish_version** | **int** | Publish version | [optional] 
**rendering_engine** | **str** | The engine used to render the report, e.g. BIRT | [optional] 
**template_doc_id** | **int** | Template doc identifier | [optional] 
**type** | **str** | Type of report | 
**type_default_text** | **str** | Report type default text | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

