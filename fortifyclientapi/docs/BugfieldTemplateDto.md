# BugfieldTemplateDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**field_name** | **str** | unique name of bugfield template. | 
**field_value** | **str** | description for bugfield template . | [optional] 
**id** | **int** | unique identifier of bugfield template. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

