# fortifyclientapi.sscclientapi.AuthTokenControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                       | HTTP request                             | Description                                                                                                                   |
| -------------------------------------------------------------------------------------------- | ---------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| [**create_auth_token**](AuthTokenControllerApi.md#create_auth_token)                         | **POST** /tokens                         | create                                                                                                                        |
| [**delete_auth_token**](AuthTokenControllerApi.md#delete_auth_token)                         | **DELETE** /tokens/{id}                  | delete                                                                                                                        |
| [**exchange_auth_code_auth_token**](AuthTokenControllerApi.md#exchange_auth_code_auth_token) | **POST** /tokens/action/exchangeAuthCode | Exchange authentication code and code verifier for a token (no authentication is required)                                    |
| [**generate_auth_code_auth_token**](AuthTokenControllerApi.md#generate_auth_code_auth_token) | **POST** /tokens/action/generateAuthCode | Request authentication code for token specification and current user                                                          |
| [**list_auth_token**](AuthTokenControllerApi.md#list_auth_token)                             | **GET** /tokens                          | list                                                                                                                          |
| [**multi_delete_auth_token**](AuthTokenControllerApi.md#multi_delete_auth_token)             | **DELETE** /tokens                       | Revoke authentication tokens using ONE of two choices: (1) all tokens owned by the requesting user, OR (2) list of token ids. |
| [**revoke_auth_token**](AuthTokenControllerApi.md#revoke_auth_token)                         | **POST** /tokens/action/revoke           | Revoke authentication tokens by value                                                                                         |
| [**update_auth_token**](AuthTokenControllerApi.md#update_auth_token)                         | **PUT** /tokens/{id}                     | update                                                                                                                        |

# **create_auth_token**
> ApiResultAuthenticationToken create_auth_token(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AuthenticationToken() # AuthenticationToken | authToken

try:
    # create
    api_response = api_instance.create_auth_token(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->create_auth_token: %s\n" % e)
```

### Parameters

| Name     | Type                                              | Description | Notes |
| -------- | ------------------------------------------------- | ----------- | ----- |
| **body** | [**AuthenticationToken**](AuthenticationToken.md) | authToken   |

### Return type

[**ApiResultAuthenticationToken**](ApiResultAuthenticationToken.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_auth_token**
> ApiResultVoid delete_auth_token(id)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id

try:
    # delete
    api_response = api_instance.delete_auth_token(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->delete_auth_token: %s\n" % e)
```

### Parameters

| Name   | Type    | Description | Notes |
| ------ | ------- | ----------- | ----- |
| **id** | **int** | id          |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **exchange_auth_code_auth_token**
> ApiResultAuthenticationToken exchange_auth_code_auth_token(body)

Exchange authentication code and code verifier for a token (no authentication is required)

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AuthenticationCodeExchangeRequest() # AuthenticationCodeExchangeRequest | authCodeExchangeRequest

try:
    # Exchange authentication code and code verifier for a token (no authentication is required)
    api_response = api_instance.exchange_auth_code_auth_token(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->exchange_auth_code_auth_token: %s\n" % e)
```

### Parameters

| Name     | Type                                                                          | Description             | Notes |
| -------- | ----------------------------------------------------------------------------- | ----------------------- | ----- |
| **body** | [**AuthenticationCodeExchangeRequest**](AuthenticationCodeExchangeRequest.md) | authCodeExchangeRequest |

### Return type

[**ApiResultAuthenticationToken**](ApiResultAuthenticationToken.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **generate_auth_code_auth_token**
> ApiResultAuthenticationCodeResponse generate_auth_code_auth_token(body)

Request authentication code for token specification and current user

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AuthenticationCodeGenerateRequest() # AuthenticationCodeGenerateRequest | authCodeGenerateRequest

try:
    # Request authentication code for token specification and current user
    api_response = api_instance.generate_auth_code_auth_token(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->generate_auth_code_auth_token: %s\n" % e)
```

### Parameters

| Name     | Type                                                                          | Description             | Notes |
| -------- | ----------------------------------------------------------------------------- | ----------------------- | ----- |
| **body** | [**AuthenticationCodeGenerateRequest**](AuthenticationCodeGenerateRequest.md) | authCodeGenerateRequest |

### Return type

[**ApiResultAuthenticationCodeResponse**](ApiResultAuthenticationCodeResponse.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_auth_token**
> ApiResultListAuthenticationToken list_auth_token(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_auth_token(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->list_auth_token: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListAuthenticationToken**](ApiResultListAuthenticationToken.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_auth_token**
> ApiResultVoid multi_delete_auth_token(all=all, ids=ids)

Revoke authentication tokens using ONE of two choices: (1) all tokens owned by the requesting user, OR (2) list of token ids.

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
all = false # bool | Specify value 'true' to revoke all authentication tokens of current logged-in user. This parameter can only be used if the 'ids' parameter is not being used. (optional) (default to false)
ids = 'ids_example' # str | A comma-separated list of resource identifiers (optional)

try:
    # Revoke authentication tokens using ONE of two choices: (1) all tokens owned by the requesting user, OR (2) list of token ids.
    api_response = api_instance.multi_delete_auth_token(all=all, ids=ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->multi_delete_auth_token: %s\n" % e)
```

### Parameters

| Name    | Type     | Description                                                                                                                                                                       | Notes                         |
| ------- | -------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **all** | **bool** | Specify value &#x27;true&#x27; to revoke all authentication tokens of current logged-in user. This parameter can only be used if the &#x27;ids&#x27; parameter is not being used. | [optional] [default to false] |
| **ids** | **str**  | A comma-separated list of resource identifiers                                                                                                                                    | [optional]                    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **revoke_auth_token**
> ApiResultVoid revoke_auth_token(body)

Revoke authentication tokens by value

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AuthTokenRevokeRequest() # AuthTokenRevokeRequest | resource

try:
    # Revoke authentication tokens by value
    api_response = api_instance.revoke_auth_token(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->revoke_auth_token: %s\n" % e)
```

### Parameters

| Name     | Type                                                    | Description | Notes |
| -------- | ------------------------------------------------------- | ----------- | ----- |
| **body** | [**AuthTokenRevokeRequest**](AuthTokenRevokeRequest.md) | resource    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_auth_token**
> ApiResultAuthenticationToken update_auth_token(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint
# Configure HTTP basic authorization: Basic
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.username = 'YOUR_USERNAME'
configuration.password = 'YOUR_PASSWORD'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthTokenControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AuthenticationToken() # AuthenticationToken | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_auth_token(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthTokenControllerApi->update_auth_token: %s\n" % e)
```

### Parameters

| Name     | Type                                              | Description | Notes |
| -------- | ------------------------------------------------- | ----------- | ----- |
| **body** | [**AuthenticationToken**](AuthenticationToken.md) | resource    |
| **id**   | **int**                                           | id          |

### Return type

[**ApiResultAuthenticationToken**](ApiResultAuthenticationToken.md)

### Authorization

[Basic](../sscclientapi/README.md#Basic)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

