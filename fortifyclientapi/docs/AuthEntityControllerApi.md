# fortifyclientapi.sscclientapi.AuthEntityControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                              | HTTP request               | Description |
| ----------------------------------------------------------------------------------- | -------------------------- | ----------- |
| [**list_auth_entity**](AuthEntityControllerApi.md#list_auth_entity)                 | **GET** /authEntities      | list        |
| [**multi_delete_auth_entity**](AuthEntityControllerApi.md#multi_delete_auth_entity) | **DELETE** /authEntities   | multiDelete |
| [**read_auth_entity**](AuthEntityControllerApi.md#read_auth_entity)                 | **GET** /authEntities/{id} | read        |

# **list_auth_entity**
> ApiResultListAuthenticationEntity list_auth_entity(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby, embed=embed, entityname=entityname, ldaptype=ldaptype)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search-spec of full text search query (see fulltextsearch parameter) (optional)
fulltextsearch = false # bool | If 'true', interpret 'q' parameter as full text search query, defaults to 'false' (optional) (default to false)
orderby = 'orderby_example' # str | Fields to order by (optional)
embed = 'embed_example' # str | Fields to embed (optional)
entityname = 'entityname_example' # str | entityname (optional)
ldaptype = 'ldaptype_example' # str | ldaptype (optional)

try:
    # list
    api_response = api_instance.list_auth_entity(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby, embed=embed, entityname=entityname, ldaptype=ldaptype)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthEntityControllerApi->list_auth_entity: %s\n" % e)
```

### Parameters

| Name               | Type     | Description                                                                                                     | Notes                         |
| ------------------ | -------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **fields**         | **str**  | Output fields                                                                                                   | [optional]                    |
| **start**          | **int**  | A start offset in object listing                                                                                | [optional] [default to 0]     |
| **limit**          | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied         | [optional] [default to 200]   |
| **q**              | **str**  | A search-spec of full text search query (see fulltextsearch parameter)                                          | [optional]                    |
| **fulltextsearch** | **bool** | If &#x27;true&#x27;, interpret &#x27;q&#x27; parameter as full text search query, defaults to &#x27;false&#x27; | [optional] [default to false] |
| **orderby**        | **str**  | Fields to order by                                                                                              | [optional]                    |
| **embed**          | **str**  | Fields to embed                                                                                                 | [optional]                    |
| **entityname**     | **str**  | entityname                                                                                                      | [optional]                    |
| **ldaptype**       | **str**  | ldaptype                                                                                                        | [optional]                    |

### Return type

[**ApiResultListAuthenticationEntity**](ApiResultListAuthenticationEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_auth_entity**
> ApiResultVoid multi_delete_auth_entity(ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers

try:
    # multiDelete
    api_response = api_instance.multi_delete_auth_entity(ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthEntityControllerApi->multi_delete_auth_entity: %s\n" % e)
```

### Parameters

| Name    | Type    | Description                                    | Notes |
| ------- | ------- | ---------------------------------------------- | ----- |
| **ids** | **str** | A comma-separated list of resource identifiers |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_auth_entity**
> ApiResultAuthenticationEntity read_auth_entity(id, fields=fields, embed=embed)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)
embed = 'embed_example' # str | Fields to embed (optional)

try:
    # read
    api_response = api_instance.read_auth_entity(id, fields=fields, embed=embed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AuthEntityControllerApi->read_auth_entity: %s\n" % e)
```

### Parameters

| Name       | Type    | Description     | Notes      |
| ---------- | ------- | --------------- | ---------- |
| **id**     | **int** | id              |
| **fields** | **str** | Output fields   | [optional] |
| **embed**  | **str** | Fields to embed | [optional] |

### Return type

[**ApiResultAuthenticationEntity**](ApiResultAuthenticationEntity.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

