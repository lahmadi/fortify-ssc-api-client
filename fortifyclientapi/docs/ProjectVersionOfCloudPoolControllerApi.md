# fortifyclientapi.sscclientapi.ProjectVersionOfCloudPoolControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                           | HTTP request                                            | Description                                    |
| -------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------- | ---------------------------------------------- |
| [**assign_project_version_of_cloud_pool**](ProjectVersionOfCloudPoolControllerApi.md#assign_project_version_of_cloud_pool)       | **POST** /cloudpools/{parentId}/versions/action/assign  | Assign application versions to the cloud pool  |
| [**do_action_project_version_of_cloud_pool**](ProjectVersionOfCloudPoolControllerApi.md#do_action_project_version_of_cloud_pool) | **POST** /cloudpools/{parentId}/versions/action         | doAction                                       |
| [**list_project_version_of_cloud_pool**](ProjectVersionOfCloudPoolControllerApi.md#list_project_version_of_cloud_pool)           | **GET** /cloudpools/{parentId}/versions                 | list                                           |
| [**replace_project_version_of_cloud_pool**](ProjectVersionOfCloudPoolControllerApi.md#replace_project_version_of_cloud_pool)     | **POST** /cloudpools/{parentId}/versions/action/replace | Replace application versions in the cloud pool |

# **assign_project_version_of_cloud_pool**
> ApiResultCloudPoolProjectVersionActionResponse assign_project_version_of_cloud_pool(body, parent_id)

Assign application versions to the cloud pool

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPoolProjectVersionAssignRequest() # CloudPoolProjectVersionAssignRequest | resource
parent_id = 'parent_id_example' # str | parentId

try:
    # Assign application versions to the cloud pool
    api_response = api_instance.assign_project_version_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfCloudPoolControllerApi->assign_project_version_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                                                | Description | Notes |
| ------------- | ----------------------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**CloudPoolProjectVersionAssignRequest**](CloudPoolProjectVersionAssignRequest.md) | resource    |
| **parent_id** | **str**                                                                             | parentId    |

### Return type

[**ApiResultCloudPoolProjectVersionActionResponse**](ApiResultCloudPoolProjectVersionActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_project_version_of_cloud_pool**
> ApiResultApiActionResponse do_action_project_version_of_cloud_pool(body, parent_id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction
parent_id = 'parent_id_example' # str | parentId

try:
    # doAction
    api_response = api_instance.do_action_project_version_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfCloudPoolControllerApi->do_action_project_version_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                      | Description      | Notes |
| ------------- | --------------------------------------------------------- | ---------------- | ----- |
| **body**      | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |
| **parent_id** | **str**                                                   | parentId         |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_project_version_of_cloud_pool**
> ApiResultListProjectVersion list_project_version_of_cloud_pool(parent_id, fields=fields, start=start, limit=limit, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 'parent_id_example' # str | parentId
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_project_version_of_cloud_pool(parent_id, fields=fields, start=start, limit=limit, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfCloudPoolControllerApi->list_project_version_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **str** | parentId                                                                                                |
| **fields**    | **str** | Output fields                                                                                           | [optional]                  |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **orderby**   | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListProjectVersion**](ApiResultListProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **replace_project_version_of_cloud_pool**
> ApiResultCloudPoolProjectVersionActionResponse replace_project_version_of_cloud_pool(body, parent_id)

Replace application versions in the cloud pool

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfCloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPoolProjectVersionReplaceRequest() # CloudPoolProjectVersionReplaceRequest | resource
parent_id = 'parent_id_example' # str | parentId

try:
    # Replace application versions in the cloud pool
    api_response = api_instance.replace_project_version_of_cloud_pool(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfCloudPoolControllerApi->replace_project_version_of_cloud_pool: %s\n" % e)
```

### Parameters

| Name          | Type                                                                                  | Description | Notes |
| ------------- | ------------------------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**CloudPoolProjectVersionReplaceRequest**](CloudPoolProjectVersionReplaceRequest.md) | resource    |
| **parent_id** | **str**                                                                               | parentId    |

### Return type

[**ApiResultCloudPoolProjectVersionActionResponse**](ApiResultCloudPoolProjectVersionActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

