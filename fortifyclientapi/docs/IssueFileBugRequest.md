# IssueFileBugRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bug_params** | [**list[BugParam]**](BugParam.md) | Bug param to file the bug | 
**filter_by** | **list[str]** | Filter by property | 
**filter_set** | **str** | Filterset | 
**issue_instance_ids** | **list[str]** | Instance id of issues that need to be filed as bugs | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

