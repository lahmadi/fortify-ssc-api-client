# fortifyclientapi.sscclientapi.CloudPoolMappingControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                | HTTP request                            | Description      |
| --------------------------------------------------------------------------------------------------------------------- | --------------------------------------- | ---------------- |
| [**map_by_version_id_cloud_pool_mapping**](CloudPoolMappingControllerApi.md#map_by_version_id_cloud_pool_mapping)     | **GET** /cloudmappings/mapByVersionId   | mapByVersionId   |
| [**map_by_version_ids_cloud_pool_mapping**](CloudPoolMappingControllerApi.md#map_by_version_ids_cloud_pool_mapping)   | **POST** /cloudmappings/mapByVersionIds | mapByVersionIds  |
| [**map_by_version_name_cloud_pool_mapping**](CloudPoolMappingControllerApi.md#map_by_version_name_cloud_pool_mapping) | **GET** /cloudmappings/mapByVersionName | mapByVersionName |

# **map_by_version_id_cloud_pool_mapping**
> ApiResultCloudPoolMapping map_by_version_id_cloud_pool_mapping(project_version_id)

mapByVersionId

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolMappingControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
project_version_id = 789 # int | projectVersionId

try:
    # mapByVersionId
    api_response = api_instance.map_by_version_id_cloud_pool_mapping(project_version_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolMappingControllerApi->map_by_version_id_cloud_pool_mapping: %s\n" % e)
```

### Parameters

| Name                   | Type    | Description      | Notes |
| ---------------------- | ------- | ---------------- | ----- |
| **project_version_id** | **int** | projectVersionId |

### Return type

[**ApiResultCloudPoolMapping**](ApiResultCloudPoolMapping.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **map_by_version_ids_cloud_pool_mapping**
> ApiResultListCloudPoolMapping map_by_version_ids_cloud_pool_mapping(body)

mapByVersionIds

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolMappingControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [56] # list[int] | projectVersionIds

try:
    # mapByVersionIds
    api_response = api_instance.map_by_version_ids_cloud_pool_mapping(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolMappingControllerApi->map_by_version_ids_cloud_pool_mapping: %s\n" % e)
```

### Parameters

| Name     | Type                    | Description       | Notes |
| -------- | ----------------------- | ----------------- | ----- |
| **body** | [**list[int]**](int.md) | projectVersionIds |

### Return type

[**ApiResultListCloudPoolMapping**](ApiResultListCloudPoolMapping.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **map_by_version_name_cloud_pool_mapping**
> ApiResultCloudPoolMapping map_by_version_name_cloud_pool_mapping(project_name, project_version_name)

mapByVersionName

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolMappingControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
project_name = 'project_name_example' # str | projectName
project_version_name = 'project_version_name_example' # str | projectVersionName

try:
    # mapByVersionName
    api_response = api_instance.map_by_version_name_cloud_pool_mapping(project_name, project_version_name)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolMappingControllerApi->map_by_version_name_cloud_pool_mapping: %s\n" % e)
```

### Parameters

| Name                     | Type    | Description        | Notes |
| ------------------------ | ------- | ------------------ | ----- |
| **project_name**         | **str** | projectName        |
| **project_version_name** | **str** | projectVersionName |

### Return type

[**ApiResultCloudPoolMapping**](ApiResultCloudPoolMapping.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

