# BugTrackerRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**test_plugin** | [**ProjectVersionBugTracker**](ProjectVersionBugTracker.md) |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

