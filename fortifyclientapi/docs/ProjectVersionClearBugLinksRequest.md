# ProjectVersionClearBugLinksRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**external_bug_ids** | **list[str]** | Identifying information for the bug links to be cleared. (The format of an externalBugId depends on the SSC bug tracker plugin which filed the bug) | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

