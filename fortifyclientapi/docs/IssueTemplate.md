# IssueTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_tag_ids** | **list[int]** | Deprecated - value is always null. | [optional] 
**default_template** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**id** | **str** | Issue template id | [optional] 
**in_use** | **bool** | Is the template in use | [optional] 
**master_attr_guid** | **str** |  | [optional] 
**name** | **str** |  | 
**object_version** | **int** |  | [optional] 
**obsolete** | **bool** |  | [optional] 
**original_file_name** | **str** |  | 
**publish_version** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

