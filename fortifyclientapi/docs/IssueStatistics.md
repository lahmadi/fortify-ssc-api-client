# IssueStatistics

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**filter_set_id** | **int** | Filter set identifier | 
**hidden_count** | **int** | Total number of hidden issues in the application version | 
**hidden_displayable_count** | **int** | Number of displayable hidden issues in the application version | 
**project_version_id** | **int** | Application version identifier | 
**removed_count** | **int** | Total number of removed issues  | 
**removed_displayable_count** | **int** | Number of displayable removed issues  in the application version | [optional] 
**suppressed_count** | **int** | Total number of suppressed issues in the application version | 
**suppressed_displayable_count** | **int** | Number of displayable suppressed issues in the application version | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

