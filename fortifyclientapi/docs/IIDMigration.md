# IIDMigration

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artifact_id** | **int** |  | [optional] 
**id** | **int** | IID migration id | [optional] 
**mappings** | [**list[IidMapping]**](IidMapping.md) |  | [optional] 
**processing_date** | **datetime** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**status** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

