# ProjectVersionState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**analysis_results_exist** | **bool** |  | 
**analysis_upload_enabled** | **bool** |  | 
**attention_required** | **bool** |  | 
**audit_enabled** | **bool** |  | 
**batch_bug_submission_exists** | **bool** |  | 
**committed** | **bool** | False if application version is in an incomplete state | 
**critical_priority_issue_count_delta** | **int** |  | 
**delta_period** | **int** |  | 
**extra_message** | **str** |  | 
**has_custom_issues** | **bool** |  | 
**id** | **int** |  | 
**issue_count_delta** | **int** |  | 
**last_fpr_upload_date** | **datetime** |  | 
**metric_evaluation_date** | **datetime** |  | 
**percent_audited_delta** | **float** |  | 
**percent_critical_priority_issues_audited_delta** | **float** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

