# ProjectVersionCopyCurrentStateRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**previous_project_version_id** | **int** | Previous application version id | 
**project_version_id** | **int** | Application version id | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

