# CloudSystemPollStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**last_poll_successful** | **bool** |  | 
**last_poll_time** | **datetime** |  | 
**last_successful_poll_time** | **datetime** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

