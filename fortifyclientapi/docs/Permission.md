# Permission

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assign_by_default** | **bool** |  | [optional] 
**depends_on_permission** | [**list[Permission]**](Permission.md) | Set of permissions this permission requires | 
**description** | **str** | Permission description | 
**id** | **str** | Permission id | [optional] 
**name** | **str** | Permission name | 
**permitted_actions** | **list[str]** | Actions this permission is allowed to perform | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

