# CloudJob

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cloud_pool** | [**CloudPool**](CloudPool.md) |  | [optional] 
**cloud_worker** | [**CloudWorker**](CloudWorker.md) |  | [optional] 
**job_cancellable** | **bool** |  | [optional] 
**job_duration** | **int** |  | [optional] 
**job_expiry_time** | **datetime** |  | [optional] 
**job_finished_time** | **datetime** |  | [optional] 
**job_has_fpr** | **bool** |  | [optional] 
**job_has_log** | **bool** |  | [optional] 
**job_queued_time** | **datetime** |  | [optional] 
**job_started_time** | **datetime** |  | [optional] 
**job_state** | **str** |  | [optional] 
**job_token** | **str** |  | [optional] 
**project_id** | **int** |  | [optional] 
**project_name** | **str** |  | [optional] 
**pv_id** | **int** |  | [optional] 
**pv_name** | **str** |  | [optional] 
**queued_duration** | **int** |  | [optional] 
**sca_args** | **str** |  | [optional] 
**sca_build_id** | **str** |  | [optional] 
**sca_version** | **str** |  | [optional] 
**scan_duration** | **int** |  | [optional] 
**submitter_email** | **str** |  | [optional] 
**submitter_ip_address** | **str** |  | [optional] 
**submitter_user_name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

