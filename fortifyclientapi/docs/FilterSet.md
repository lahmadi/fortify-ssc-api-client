# FilterSet

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_filter_set** | **bool** | whether this filter set is the default within its issue template | 
**description** | **str** |  | 
**folders** | [**list[FolderDto]**](FolderDto.md) | List of folders defined in filter set | 
**guid** | **str** | GUID of filter set | 
**title** | **str** | name of filter set | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

