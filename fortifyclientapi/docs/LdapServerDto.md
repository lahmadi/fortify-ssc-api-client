# LdapServerDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_distinguished_name** | **str** |  | 
**attribute_email** | **str** |  | 
**attribute_first_name** | **str** |  | 
**attribute_groupname** | **str** |  | 
**attribute_last_name** | **str** |  | 
**attribute_member** | **str** |  | 
**attribute_member_of** | **str** |  | 
**attribute_object_class** | **str** |  | 
**attribute_object_sid** | **str** |  | [optional] 
**attribute_orgunit_name** | **str** |  | 
**attribute_password** | **str** |  | 
**attribute_thumbnail_mime_default** | **str** |  | [optional] 
**attribute_thumbnail_photo** | **str** |  | [optional] 
**attribute_user_name** | **str** |  | 
**authenticator_type** | **str** |  | 
**base_dn** | **str** | Distinguished name (DN) of root (base) LDAP entity SSC has access to. If value is set to not empty entity&#x27;s DN, SSC has access only to this entity and all its children. If value of this attribute is an empty string, SSC has access to whole LDAP entities tree | 
**base_object_sid** | **str** |  | [optional] 
**cache_enabled** | **bool** |  | 
**cache_executor_pool_size** | **int** |  | 
**cache_executor_pool_size_max** | **int** |  | 
**cache_max_threads_per_cache** | **int** |  | 
**check_ssl_hostname** | **bool** |  | 
**check_ssl_trust** | **bool** |  | 
**class_group** | **str** |  | 
**class_orgunit** | **str** |  | 
**class_user** | **str** |  | 
**default_server** | **bool** | Boolean flag that marks LDAP server as default. Default means the server which configuration was imported from ldap.properties legacy configuration file | 
**enabled** | **bool** | Flag that marks server as enabled. All enabled servers are used by SSC. Server can be temporary disabled if it is temporary down. | 
**id** | **int** | LDAP Server id | 
**ignore_partial_result_exception** | **bool** |  | 
**nested_groups_enabled** | **bool** |  | 
**object_version** | **int** | Version of the LDAP server entity to support editing LDAP server entity by multiply administrators | 
**page_size** | **int** |  | 
**paging_enabled** | **bool** |  | 
**password_encoder_type** | **str** |  | 
**referrals_processing_strategy** | **str** |  | 
**save_without_validation** | **bool** |  | [optional] 
**search_dns** | **str** |  | [optional] 
**server_name** | **str** | Name of the LDAP server to distinguish it from other servers | 
**url** | **str** |  | 
**user_dn** | **str** |  | 
**user_password** | **str** |  | 
**user_photo_enabled** | **bool** |  | 
**validation_idle_time** | **int** |  | 
**validation_time_limit** | **int** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

