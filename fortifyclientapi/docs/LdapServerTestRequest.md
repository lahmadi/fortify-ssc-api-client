# LdapServerTestRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ldap_config** | [**LdapServerDto**](LdapServerDto.md) |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

