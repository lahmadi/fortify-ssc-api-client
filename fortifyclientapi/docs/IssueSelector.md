# IssueSelector

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** | Description | 
**display_name** | **str** | Display name for issue selector | 
**entity_type** | **str** | Issue selector entity type | 
**guid** | **str** | Issue selector global unique identifier | 
**value** | **str** | Issue selector value | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

