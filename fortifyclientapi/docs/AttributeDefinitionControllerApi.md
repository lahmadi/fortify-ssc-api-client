# fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                         | HTTP request                          | Description |
| -------------------------------------------------------------------------------------------------------------- | ------------------------------------- | ----------- |
| [**create_attribute_definition**](AttributeDefinitionControllerApi.md#create_attribute_definition)             | **POST** /attributeDefinitions        | create      |
| [**delete_attribute_definition**](AttributeDefinitionControllerApi.md#delete_attribute_definition)             | **DELETE** /attributeDefinitions/{id} | delete      |
| [**list_attribute_definition**](AttributeDefinitionControllerApi.md#list_attribute_definition)                 | **GET** /attributeDefinitions         | list        |
| [**multi_delete_attribute_definition**](AttributeDefinitionControllerApi.md#multi_delete_attribute_definition) | **DELETE** /attributeDefinitions      | multiDelete |
| [**read_attribute_definition**](AttributeDefinitionControllerApi.md#read_attribute_definition)                 | **GET** /attributeDefinitions/{id}    | read        |
| [**update_attribute_definition**](AttributeDefinitionControllerApi.md#update_attribute_definition)             | **PUT** /attributeDefinitions/{id}    | update      |

# **create_attribute_definition**
> ApiResultAttributeDefinition create_attribute_definition(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AttributeDefinition() # AttributeDefinition | resource

try:
    # create
    api_response = api_instance.create_attribute_definition(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeDefinitionControllerApi->create_attribute_definition: %s\n" % e)
```

### Parameters

| Name     | Type                                              | Description | Notes |
| -------- | ------------------------------------------------- | ----------- | ----- |
| **body** | [**AttributeDefinition**](AttributeDefinition.md) | resource    |

### Return type

[**ApiResultAttributeDefinition**](ApiResultAttributeDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_attribute_definition**
> ApiResultVoid delete_attribute_definition(id, force=force)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
force = false # bool | If parameter is set to 'true', the specified attribute(s), if deletable, will be removed even if in use. (optional) (default to false)

try:
    # delete
    api_response = api_instance.delete_attribute_definition(id, force=force)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeDefinitionControllerApi->delete_attribute_definition: %s\n" % e)
```

### Parameters

| Name      | Type     | Description                                                                                                        | Notes                         |
| --------- | -------- | ------------------------------------------------------------------------------------------------------------------ | ----------------------------- |
| **id**    | **int**  | id                                                                                                                 |
| **force** | **bool** | If parameter is set to &#x27;true&#x27;, the specified attribute(s), if deletable, will be removed even if in use. | [optional] [default to false] |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_attribute_definition**
> ApiResultListAttributeDefinition list_attribute_definition(fields=fields, start=start, limit=limit, q=q, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_attribute_definition(fields=fields, start=start, limit=limit, q=q, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeDefinitionControllerApi->list_attribute_definition: %s\n" % e)
```

### Parameters

| Name        | Type    | Description                                                                                             | Notes                       |
| ----------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **fields**  | **str** | Output fields                                                                                           | [optional]                  |
| **start**   | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**   | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**       | **str** | A search query                                                                                          | [optional]                  |
| **orderby** | **str** | Fields to order by                                                                                      | [optional]                  |

### Return type

[**ApiResultListAttributeDefinition**](ApiResultListAttributeDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_attribute_definition**
> ApiResultVoid multi_delete_attribute_definition(ids, force=force)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
ids = 'ids_example' # str | A comma-separated list of resource identifiers
force = false # bool | If parameter is set to 'true', the specified attribute(s), if deletable, will be removed even if in use. (optional) (default to false)

try:
    # multiDelete
    api_response = api_instance.multi_delete_attribute_definition(ids, force=force)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeDefinitionControllerApi->multi_delete_attribute_definition: %s\n" % e)
```

### Parameters

| Name      | Type     | Description                                                                                                        | Notes                         |
| --------- | -------- | ------------------------------------------------------------------------------------------------------------------ | ----------------------------- |
| **ids**   | **str**  | A comma-separated list of resource identifiers                                                                     |
| **force** | **bool** | If parameter is set to &#x27;true&#x27;, the specified attribute(s), if deletable, will be removed even if in use. | [optional] [default to false] |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_attribute_definition**
> ApiResultAttributeDefinition read_attribute_definition(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_attribute_definition(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeDefinitionControllerApi->read_attribute_definition: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **int** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultAttributeDefinition**](ApiResultAttributeDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_attribute_definition**
> ApiResultAttributeDefinition update_attribute_definition(body, id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeDefinitionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.AttributeDefinition() # AttributeDefinition | resource
id = 789 # int | id

try:
    # update
    api_response = api_instance.update_attribute_definition(body, id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeDefinitionControllerApi->update_attribute_definition: %s\n" % e)
```

### Parameters

| Name     | Type                                              | Description | Notes |
| -------- | ------------------------------------------------- | ----------- | ----- |
| **body** | [**AttributeDefinition**](AttributeDefinition.md) | resource    |
| **id**   | **int**                                           | id          |

### Return type

[**ApiResultAttributeDefinition**](ApiResultAttributeDefinition.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

