# EmbeddedScans

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**scans** | [**list[Scan]**](Scan.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

