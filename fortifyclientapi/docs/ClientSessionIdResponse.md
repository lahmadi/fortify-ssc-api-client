# ClientSessionIdResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_session_id** | **str** | Opaque unique identifier for the current user session (between 43 and 255 characters) | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

