# AttributeOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**guid** | **str** | Unique string identifier for an Attribute Option | 
**hidden** | **bool** | Set to true if this option is to be hidden | [optional] 
**id** | **int** | ID required with referencing an existing Attribute Option | 
**in_use** | **bool** | Set to true if this option is being used | [optional] 
**index** | **int** | Index of this option in list of options | 
**name** | **str** | Name | 
**object_version** | **int** |  | [optional] 
**project_meta_data_def_id** | **int** | ID of Attribute Definition this option is associated with | 
**publish_version** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

