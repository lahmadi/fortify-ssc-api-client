# WebHookHistoryItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | **datetime** | Date and time when the webhook request was created | [optional] 
**id** | **int** | Id of sent webhook request | [optional] 
**status** | **str** | Status of webhook request | [optional] 
**web_hook_id** | **int** | ID of webhook definition that triggered the webhook request | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

