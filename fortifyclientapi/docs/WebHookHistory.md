# WebHookHistory

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type** | **str** |  | [optional] 
**created_at** | **datetime** | Date and time when the webhook request was created | [optional] 
**failure_reason** | **str** | Reason of failure, if webhook request failed | [optional] 
**id** | **int** | Id of sent webhook request | [optional] 
**request_headers** | **str** | Request headers of webhook request. HTML-encoded. | [optional] 
**request_payload** | **str** | Request payload of webhook request. HTML-encoded. | [optional] 
**response_body** | **str** | Response body of webhook request. Truncated to 1500 characters and HTML-encoded. | [optional] 
**response_code** | **int** | Response code of webhook request | [optional] 
**response_headers** | **str** | Response headers of webhook request. Truncated to 1000 characters and HTML-encoded. | [optional] 
**restricted** | **bool** | True if current user is missing some permissions to fully view or resend this webhook request | [optional] 
**status** | **str** | Status of webhook request | [optional] 
**web_hook_id** | **int** | ID of webhook definition that triggered the webhook request | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

