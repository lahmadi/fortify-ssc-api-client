# ActivityFeed

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_entity** | [**AuthenticationEntity**](AuthenticationEntity.md) |  | [optional] 
**detailed_note** | **str** |  | [optional] 
**entity_id** | **int** |  | [optional] 
**event_date** | **datetime** |  | [optional] 
**event_type** | **str** |  | [optional] 
**event_type_desc** | **str** | Description | [optional] 
**id** | **int** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**user_name** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

