# IssueAgingDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**average_days_to_remediate** | **int** | Average number of days to fix issues in application versions. | [optional] 
**average_days_to_review** | **int** | Average number of days to review issues in application versions. | [optional] 
**bug_density** | **float** | Bug density per 10 000 of scanned lines of code. | [optional] 
**files_scanned** | **int** | Number of files that were scanned in all application versions. | [optional] 
**id** | **str** | This is dynamic attribute which value depend of the aggregation attribute. It can be application ID, application version ID or application version attribute GUID. | [optional] 
**issues_pending_review** | **int** | Number of issues that are pending review in all application versions. | [optional] 
**lines_of_code** | **int** | Number of lines of code that were scanned by scans which results are stored in the application versions. | [optional] 
**name** | **str** | This is dynamic attribute which value depend of the aggregation attribute. It can be application name, application version name or application version attribute name. | [optional] 
**number_of_application_versions** | **int** | Number of application versions in the application. | [optional] 
**oldest_scan_date** | **datetime** | Issue audites that have been done after this date have been taken into account for average days to review calculation. | [optional] 
**open_issues** | **int** | Number of not remediated issues in all application versions. | [optional] 
**snapshot_out_of_date** | **bool** | Flag that indicates that application metrics state was changed and it metrics are going to be recalculated. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

