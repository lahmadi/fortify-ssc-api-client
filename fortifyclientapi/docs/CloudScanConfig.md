# CloudScanConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cloud_scan_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

