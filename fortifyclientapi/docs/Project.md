# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_by** | **str** | User that created this application | 
**creation_date** | **datetime** | Date created | 
**description** | **str** | Description | [optional] 
**id** | **int** | Id | [optional] 
**issue_template_id** | **str** | Id of the Issue Template used | 
**name** | **str** | Name | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

