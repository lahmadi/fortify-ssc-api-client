# AlertDefinitionDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alert_all_children** | **bool** |  | [optional] 
**alert_stakeholders** | **bool** |  | [optional] 
**alert_triggers** | [**list[AlertTriggerDto]**](AlertTriggerDto.md) |  | 
**created_by** | **str** |  | [optional] 
**creation_date** | **datetime** |  | [optional] 
**custom_message** | **str** | Required field for Scheduled alerts, Optional for other types | 
**description** | **str** |  | [optional] 
**enabled** | **bool** |  | [optional] 
**end_date** | **datetime** |  | [optional] 
**id** | **int** |  | [optional] 
**monitor_all_apps** | **bool** |  | [optional] 
**monitored_entity_name** | **str** |  | [optional] 
**monitored_entity_type** | **str** |  | 
**monitored_instance_id** | **int** |  | [optional] 
**monitors_project_versions** | **bool** |  | [optional] 
**monitors_runtime_app** | **bool** |  | [optional] 
**name** | **str** |  | 
**project_version_ids** | **list[int]** |  | [optional] 
**recipient_type** | **str** |  | 
**remind_periodically** | **bool** |  | [optional] 
**reminder_period** | **int** |  | [optional] 
**start_at_due_date** | **bool** |  | [optional] 
**start_date** | **datetime** |  | [optional] 
**trigger_description** | **str** |  | [optional] 
**trigger_description_name** | **str** |  | [optional] 
**trigger_description_operation** | **str** |  | [optional] 
**trigger_description_value** | **str** |  | [optional] 
**user_can_modify** | **bool** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

