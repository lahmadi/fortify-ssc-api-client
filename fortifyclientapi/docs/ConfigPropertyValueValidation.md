# ConfigPropertyValueValidation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**validation_type** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

