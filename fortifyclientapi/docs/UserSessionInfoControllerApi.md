# fortifyclientapi.sscclientapi.UserSessionInfoControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                               | HTTP request               | Description                                                                                              |
| ------------------------------------------------------------------------------------ | -------------------------- | -------------------------------------------------------------------------------------------------------- |
| [**post_user_session_info**](UserSessionInfoControllerApi.md#post_user_session_info) | **POST** /userSession/info | Retrieve the current user&#x27;s session info. (The &#x27;username&#x27; parameter is not yet supported) |

# **post_user_session_info**
> ApiResultUserSessionInformation post_user_session_info(body)

Retrieve the current user's session info. (The 'username' parameter is not yet supported)

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserSessionInfoControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = 'body_example' # str | username

try:
    # Retrieve the current user's session info. (The 'username' parameter is not yet supported)
    api_response = api_instance.post_user_session_info(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserSessionInfoControllerApi->post_user_session_info: %s\n" % e)
```

### Parameters

| Name     | Type              | Description | Notes |
| -------- | ----------------- | ----------- | ----- |
| **body** | [**str**](str.md) | username    |

### Return type

[**ApiResultUserSessionInformation**](ApiResultUserSessionInformation.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

