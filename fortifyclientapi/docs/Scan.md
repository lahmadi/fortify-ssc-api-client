# Scan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**artifact_id** | **int** | identifier of parent artifact object which contains this scan object | 
**build_id** | **str** | optional string identifier provided by the user when scanning | 
**build_label** | **str** | optional string identifier provided by the user when scanning | 
**build_project** | **str** | optional string identifier provided by the user when scanning | 
**build_version** | **str** | optional string identifier provided by the user when scanning | 
**certification** | **str** | indicates whether the checksum on the analysis result is valid | 
**elapsed_time** | **str** | analysis duration | 
**engine_version** | **str** | version of analysis engine | 
**exec_loc** | **int** | total executable lines of code included in sources (excluding comments etc.) | 
**fortify_annotations_loc** | **int** | lines of code with annotations | 
**guid** | **str** | globally unique id of scan object | 
**hostname** | **str** |  | 
**id** | **int** |  | [optional] 
**no_of_files** | **int** | number of source files included in scan | 
**total_loc** | **int** | total lines of code included in sources (includes comments etc.) | 
**type** | **str** | indicates the type of analyzer that produced it, such as SCA or WEBINSPECT | 
**upload_date** | **datetime** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

