# fortifyclientapi.sscclientapi.IssueAuditCommentOfIssueControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                  | HTTP request                         | Description |
| ----------------------------------------------------------------------------------------------------------------------- | ------------------------------------ | ----------- |
| [**create_issue_audit_comment_of_issue**](IssueAuditCommentOfIssueControllerApi.md#create_issue_audit_comment_of_issue) | **POST** /issues/{parentId}/comments | create      |
| [**list_issue_audit_comment_of_issue**](IssueAuditCommentOfIssueControllerApi.md#list_issue_audit_comment_of_issue)     | **GET** /issues/{parentId}/comments  | list        |

# **create_issue_audit_comment_of_issue**
> ApiResultIssueAuditComment create_issue_audit_comment_of_issue(body, parent_id)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAuditCommentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.IssueAuditComment() # IssueAuditComment | resource
parent_id = 789 # int | parentId

try:
    # create
    api_response = api_instance.create_issue_audit_comment_of_issue(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAuditCommentOfIssueControllerApi->create_issue_audit_comment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type                                          | Description | Notes |
| ------------- | --------------------------------------------- | ----------- | ----- |
| **body**      | [**IssueAuditComment**](IssueAuditComment.md) | resource    |
| **parent_id** | **int**                                       | parentId    |

### Return type

[**ApiResultIssueAuditComment**](ApiResultIssueAuditComment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_issue_audit_comment_of_issue**
> ApiResultListIssueAuditComment list_issue_audit_comment_of_issue(parent_id, start=start, limit=limit)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAuditCommentOfIssueControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)

try:
    # list
    api_response = api_instance.list_issue_audit_comment_of_issue(parent_id, start=start, limit=limit)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAuditCommentOfIssueControllerApi->list_issue_audit_comment_of_issue: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **int** | parentId                                                                                                |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |

### Return type

[**ApiResultListIssueAuditComment**](ApiResultListIssueAuditComment.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

