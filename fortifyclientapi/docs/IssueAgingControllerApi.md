# fortifyclientapi.sscclientapi.IssueAgingControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                              | HTTP request        | Description |
| ------------------------------------------------------------------- | ------------------- | ----------- |
| [**list_issue_aging**](IssueAgingControllerApi.md#list_issue_aging) | **GET** /issueaging | list        |

# **list_issue_aging**
> ApiResultListIssueAgingDto list_issue_aging(orderby=orderby, fields=fields, start=start, limit=limit, filterby=filterby, aggregateby=aggregateby, groupguid=groupguid, groupvalue=groupvalue, name=name, number_of_application_versions=number_of_application_versions, lines_of_code=lines_of_code, issues_pending_review=issues_pending_review, open_issues=open_issues, files_scanned=files_scanned, bug_density=bug_density, average_days_to_review=average_days_to_review, average_days_to_remediate=average_days_to_remediate)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueAgingControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
orderby = 'orderby_example' # str | Fields to order by (optional)
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
filterby = 'filterby_example' # str | filterby (optional)
aggregateby = 'aggregateby_example' # str | aggregateby (optional)
groupguid = 'groupguid_example' # str | groupguid (optional)
groupvalue = 'groupvalue_example' # str | groupvalue (optional)
name = 'name_example' # str | name (optional)
number_of_application_versions = 'number_of_application_versions_example' # str | numberOfApplicationVersions (optional)
lines_of_code = 'lines_of_code_example' # str | linesOfCode (optional)
issues_pending_review = 'issues_pending_review_example' # str | issuesPendingReview (optional)
open_issues = 'open_issues_example' # str | openIssues (optional)
files_scanned = 'files_scanned_example' # str | filesScanned (optional)
bug_density = 'bug_density_example' # str | bugDensity (optional)
average_days_to_review = 'average_days_to_review_example' # str | averageDaysToReview (optional)
average_days_to_remediate = 'average_days_to_remediate_example' # str | averageDaysToRemediate (optional)

try:
    # list
    api_response = api_instance.list_issue_aging(orderby=orderby, fields=fields, start=start, limit=limit, filterby=filterby, aggregateby=aggregateby, groupguid=groupguid, groupvalue=groupvalue, name=name, number_of_application_versions=number_of_application_versions, lines_of_code=lines_of_code, issues_pending_review=issues_pending_review, open_issues=open_issues, files_scanned=files_scanned, bug_density=bug_density, average_days_to_review=average_days_to_review, average_days_to_remediate=average_days_to_remediate)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueAgingControllerApi->list_issue_aging: %s\n" % e)
```

### Parameters

| Name                               | Type    | Description                                                                                             | Notes                       |
| ---------------------------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **orderby**                        | **str** | Fields to order by                                                                                      | [optional]                  |
| **fields**                         | **str** | Output fields                                                                                           | [optional]                  |
| **start**                          | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**                          | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **filterby**                       | **str** | filterby                                                                                                | [optional]                  |
| **aggregateby**                    | **str** | aggregateby                                                                                             | [optional]                  |
| **groupguid**                      | **str** | groupguid                                                                                               | [optional]                  |
| **groupvalue**                     | **str** | groupvalue                                                                                              | [optional]                  |
| **name**                           | **str** | name                                                                                                    | [optional]                  |
| **number_of_application_versions** | **str** | numberOfApplicationVersions                                                                             | [optional]                  |
| **lines_of_code**                  | **str** | linesOfCode                                                                                             | [optional]                  |
| **issues_pending_review**          | **str** | issuesPendingReview                                                                                     | [optional]                  |
| **open_issues**                    | **str** | openIssues                                                                                              | [optional]                  |
| **files_scanned**                  | **str** | filesScanned                                                                                            | [optional]                  |
| **bug_density**                    | **str** | bugDensity                                                                                              | [optional]                  |
| **average_days_to_review**         | **str** | averageDaysToReview                                                                                     | [optional]                  |
| **average_days_to_remediate**      | **str** | averageDaysToRemediate                                                                                  | [optional]                  |

### Return type

[**ApiResultListIssueAgingDto**](ApiResultListIssueAgingDto.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

