# BugFilingRequirements

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bug_params** | [**list[BugParam]**](BugParam.md) |  | [optional] 
**bug_tracker_long_display_name** | **str** |  | [optional] 
**bug_tracker_short_display_name** | **str** |  | [optional] 
**requires_authentication** | **bool** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

