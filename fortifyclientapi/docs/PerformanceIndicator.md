# PerformanceIndicator

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**equation** | **str** |  | 
**guid** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**in_use** | **bool** |  | [optional] 
**name** | **str** |  | 
**object_version** | **int** |  | [optional] 
**publish_version** | **int** |  | [optional] 
**range** | **str** |  | 
**type** | **str** |  | [optional] 
**variables** | [**list[Variable]**](Variable.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

