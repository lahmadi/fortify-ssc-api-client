# fortifyclientapi.sscclientapi.FilterSetOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                 | HTTP request                                   | Description |
| ---------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------- | ----------- |
| [**list_filter_set_of_project_version**](FilterSetOfProjectVersionControllerApi.md#list_filter_set_of_project_version) | **GET** /projectVersions/{parentId}/filterSets | list        |

# **list_filter_set_of_project_version**
> ApiResultListFilterSet list_filter_set_of_project_version(parent_id, start=start, limit=limit, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FilterSetOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_filter_set_of_project_version(parent_id, start=start, limit=limit, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FilterSetOfProjectVersionControllerApi->list_filter_set_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **int** | parentId                                                                                                |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **fields**    | **str** | Output fields                                                                                           | [optional]                  |

### Return type

[**ApiResultListFilterSet**](ApiResultListFilterSet.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

