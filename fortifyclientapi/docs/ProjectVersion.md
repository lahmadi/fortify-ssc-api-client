# ProjectVersion

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | **bool** | True if this version is active | 
**assigned_issues_count** | **int** |  | [optional] 
**attachments_out_of_date** | **bool** |  | [optional] 
**auto_predict** | **bool** | true if auto-prediction is enabled for this application version; false otherwise. This property modification is protected by AUDITASSISTANT_MANAGE permission. | [optional] 
**bug_tracker_enabled** | **bool** | true if the bug tracker plugin assigned to the application version is currently enabled in the system | 
**bug_tracker_plugin_id** | **str** | identifier of the bug tracker plugin (if any) assigned to this application version | 
**committed** | **bool** | True if this version is committed and ready to be used | 
**created_by** | **str** | User that created this version | 
**creation_date** | **datetime** | Date this version was created | 
**current_state** | [**ProjectVersionState**](ProjectVersionState.md) |  | [optional] 
**custom_tag_values_auto_apply** | **bool** | true if custom tag values auto-apply is enabled for this application version; false otherwise. This property modification is protected by AUDITASSISTANT_MANAGE permission. | [optional] 
**description** | **str** | Description | 
**id** | **int** | Id | [optional] 
**issue_template_id** | **str** | Id of the Issue Template used by this version | 
**issue_template_modified_time** | **int** | Last time the Issue Template was modified | 
**issue_template_name** | **str** | Name of the Issue Template used by this version | 
**latest_scan_id** | **int** | id of the latest scan uploaded to application version | 
**load_properties** | **str** |  | [optional] 
**master_attr_guid** | **str** | Guid of the primary custom tag for this version | 
**migration_version** | **float** |  | [optional] 
**mode** | **str** |  | [optional] 
**name** | **str** | Name | 
**obfuscated_id** | **str** |  | [optional] 
**prediction_policy** | **str** | Name of the policy to be used for predictions for this application version. Modification of this property is protected by AUDITASSISTANT_MANAGE permission. | [optional] 
**project** | [**Project**](Project.md) |  | [optional] 
**refresh_required** | **bool** |  | [optional] 
**security_group** | **str** |  | [optional] 
**server_version** | **float** | Server version this version&#x27;s data supports | 
**site_id** | **str** |  | [optional] 
**snapshot_out_of_date** | **bool** | True if the most recent snapshot is not accurate | 
**source_base_path** | **str** |  | [optional] 
**stale_issue_template** | **bool** | True if this version&#x27;s Issue Template has changed or been modified | 
**status** | **str** |  | [optional] 
**traces_out_of_date** | **bool** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

