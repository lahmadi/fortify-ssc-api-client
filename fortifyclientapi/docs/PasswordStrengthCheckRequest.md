# PasswordStrengthCheckRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**extra_dictionary** | **list[str]** | Get additional words to add to dictionary. If null, current user data will be added (for example: username, first name, last name, email). | [optional] 
**password** | **str** | Get password or its part from input | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

