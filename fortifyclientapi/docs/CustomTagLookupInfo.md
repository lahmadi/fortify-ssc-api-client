# CustomTagLookupInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lookup_index** | **int** | Current value index in values list | 
**lookup_value** | **str** | Plain text custom tag value | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

