# BugFilingRequirementsResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bug_filing_requirements** | [**BugFilingRequirements**](BugFilingRequirements.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

