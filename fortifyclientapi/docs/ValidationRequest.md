# ValidationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**string_to_validate** | **str** | String to validate | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

