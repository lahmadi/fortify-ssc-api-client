# RefreshAuditAssistantPoliciesRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**current_policies** | **list[str]** | A list with current policy names from Audit Assistant server. | [optional] 
**obsolete_policies** | [**list[ObsoletePolicy]**](ObsoletePolicy.md) | A list of policy objects containing existing obsolete AV policy names as a result of comparison with current server policies. | [optional] 
**policy_replacements** | [**list[PolicyReplacement]**](PolicyReplacement.md) | Mapping between old (obsolete) and new (existing) policy names; current AV policies and/or system default policy are to be replaced | [optional] 
**properties** | [**list[ConfigProperty]**](ConfigProperty.md) | Audit Assistant server configuration properties to be used policies retrieval from Fortify Scan Analytics server  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

