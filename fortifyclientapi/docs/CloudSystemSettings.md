# CloudSystemSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cleanup_period_seconds** | **int** |  | 
**controller_max_upload_size** | **int** |  | 
**controller_system_url** | **str** |  | 
**controller_system_version** | **str** |  | 
**is_ssc_lockdown_mode** | **bool** |  | 
**job_expiry_delay_seconds** | **int** |  | 
**pool_mapping_mode** | **str** |  | 
**smtp_from_address** | **str** |  | 
**smtp_host** | **str** |  | 
**smtp_port** | **int** |  | 
**ssc_url** | **str** |  | 
**worker_expiry_delay_seconds** | **int** |  | 
**worker_inactive_delay_seconds** | **int** |  | 
**worker_stale_delay_seconds** | **int** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

