# IssueAuditHistoryDto

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_name** | **str** | Changed attribute name. | 
**audit_date_time** | **datetime** | Date and time when audit was performed. | 
**conflict** | **bool** | Flag that indicates if there were any conflicts when audit information was merged. | 
**issue_id** | **int** | ID of the issue the history record belongs to | 
**new_value** | **str** | Attribute value after audit. | 
**old_value** | **str** | Attribute value before audit. | 
**sequence_number** | **int** | Sequence number of the history record in the list of all audit history records for the issue. | 
**user_name** | **str** | Name of the user who performed the audit. | 
**value_type** | **str** | Attribute type. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

