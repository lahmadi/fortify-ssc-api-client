# EmbeddedReportDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields_to_null_with_exclusions** | **list[str]** |  | [optional] 
**report_definition** | [**ReportDefinition**](ReportDefinition.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

