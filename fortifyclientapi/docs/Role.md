# Role

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**all_application_role** | **bool** | True if a user with this Role has access to all Application Versions | 
**assigned_to_non_users** | **bool** | True if this Role is currently assigned to entities that are not users(groups, organizations, etc.) | 
**built_in** | **bool** | True if this Role is a default Fortify Role | 
**default** | **bool** |  | [optional] 
**deletable** | **bool** |  | [optional] 
**description** | **str** |  | [optional] 
**id** | **str** |  | [optional] 
**name** | **str** |  | [optional] 
**object_version** | **int** |  | [optional] 
**permission_ids** | **list[str]** | Ids of the permissions this Role has | 
**publish_version** | **int** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

