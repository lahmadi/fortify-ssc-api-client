# ConfigPropertyValueItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** | Value display name. This string is displayed on UI in the option values list. | [optional] 
**value** | **str** | Value that is sent to server if this option is selected on UI. | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

