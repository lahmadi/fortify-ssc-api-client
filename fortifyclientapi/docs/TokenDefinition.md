# TokenDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**capability_description** | **str** | Describes what tokens generated from this token specification can be used for. Note that these capabilities are subordinate to the actual roles/permissions granted to the owner of the token | [optional] 
**max_days_to_live** | **int** | Maximum allowable lifetime (in nominal days) of token. At the time of token creation/edit, the exact value for its max expiration is calculated by adding maxDaysToLive * MilliSecondsPerDay to the current datetime | [optional] 
**max_usages** | **int** | Maximum number of api calls that can be made using this token type. A value of -1 denotes unlimited number of calls. | [optional] 
**type** | **str** | Token type | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

