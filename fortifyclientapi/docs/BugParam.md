# BugParam

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bug_param_type** | **str** |  | [optional] 
**choice_list** | **list[str]** |  | [optional] 
**description** | **str** |  | [optional] 
**display_label** | **str** |  | [optional] 
**has_dependent_params** | **bool** |  | [optional] 
**identifier** | **str** |  | [optional] 
**max_length** | **int** |  | [optional] 
**required** | **bool** |  | [optional] 
**value** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

