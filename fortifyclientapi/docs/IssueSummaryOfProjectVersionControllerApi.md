# fortifyclientapi.sscclientapi.IssueSummaryOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                          | HTTP request                                       | Description |
| ------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------- | ----------- |
| [**list_issue_summary_of_project_version**](IssueSummaryOfProjectVersionControllerApi.md#list_issue_summary_of_project_version) | **GET** /projectVersions/{parentId}/issueSummaries | list        |

# **list_issue_summary_of_project_version**
> ApiResultListIssueSummary list_issue_summary_of_project_version(parent_id, seriestype, groupaxistype, audited=audited, filter=filter, showhidden=showhidden, showremoved=showremoved, showsuppressed=showsuppressed)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.IssueSummaryOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
seriestype = 'seriestype_example' # str | seriestype
groupaxistype = 'groupaxistype_example' # str | List of allowed patterns: APP_NAME, SCAN_DATE, SCAN_PRODUCT, ISSUE_FOLDER, ISSUE_CATEGORY, ISSUE_KINGDOM, ISSUE_FILENAME, ISSUE_FRIORITY, ISSUE_AUDITED, ISSUE_PACKAGE_NAME, ISSUE_CLASS_NAME, ISSUE_FUNCTION_NAME, ISSUE_MAPPED_CATEGORY, FOLDER_FOLDER, ISSUE_{name of issue attribute}, EXTERNALLIST_{external category name}, CUSTOMTAG_{custom tag name}.
audited = 'audited_example' # str | audited (optional)
filter = 'filter_example' # str | filter (optional)
showhidden = true # bool | showhidden (optional)
showremoved = true # bool | showremoved (optional)
showsuppressed = true # bool | showsuppressed (optional)

try:
    # list
    api_response = api_instance.list_issue_summary_of_project_version(parent_id, seriestype, groupaxistype, audited=audited, filter=filter, showhidden=showhidden, showremoved=showremoved, showsuppressed=showsuppressed)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling IssueSummaryOfProjectVersionControllerApi->list_issue_summary_of_project_version: %s\n" % e)
```

### Parameters

| Name               | Type     | Description                                                                                                                                                                                                                                                                                                                                                    | Notes      |
| ------------------ | -------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- |
| **parent_id**      | **int**  | parentId                                                                                                                                                                                                                                                                                                                                                       |
| **seriestype**     | **str**  | seriestype                                                                                                                                                                                                                                                                                                                                                     |
| **groupaxistype**  | **str**  | List of allowed patterns: APP_NAME, SCAN_DATE, SCAN_PRODUCT, ISSUE_FOLDER, ISSUE_CATEGORY, ISSUE_KINGDOM, ISSUE_FILENAME, ISSUE_FRIORITY, ISSUE_AUDITED, ISSUE_PACKAGE_NAME, ISSUE_CLASS_NAME, ISSUE_FUNCTION_NAME, ISSUE_MAPPED_CATEGORY, FOLDER_FOLDER, ISSUE_{name of issue attribute}, EXTERNALLIST_{external category name}, CUSTOMTAG_{custom tag name}. |
| **audited**        | **str**  | audited                                                                                                                                                                                                                                                                                                                                                        | [optional] |
| **filter**         | **str**  | filter                                                                                                                                                                                                                                                                                                                                                         | [optional] |
| **showhidden**     | **bool** | showhidden                                                                                                                                                                                                                                                                                                                                                     | [optional] |
| **showremoved**    | **bool** | showremoved                                                                                                                                                                                                                                                                                                                                                    | [optional] |
| **showsuppressed** | **bool** | showsuppressed                                                                                                                                                                                                                                                                                                                                                 | [optional] |

### Return type

[**ApiResultListIssueSummary**](ApiResultListIssueSummary.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

