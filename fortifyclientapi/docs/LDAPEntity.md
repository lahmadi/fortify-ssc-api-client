# LDAPEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**distinguished_name** | **str** | Distinguished name of LDAP entity | 
**email** | **str** | Email of LDAP entity | 
**first_name** | **str** | First name of LDAP entity | 
**id** | **int** | LDAP entity identifier | [optional] 
**last_name** | **str** | Last name of LDAP entity | 
**ldap_type** | **str** | Type of LDAP entity. | 
**name** | **str** | LDAP entity name | 
**roles** | [**list[Role]**](Role.md) | List of roles pertaining to this LDAP entity | 
**user_photo** | [**UserPhoto**](UserPhoto.md) |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

