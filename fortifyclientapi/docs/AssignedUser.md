# AssignedUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**has_access** | **bool** | whether the assigned user currently has access to the application version in current context | 
**user_name** | **str** | username assigned to issue | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

