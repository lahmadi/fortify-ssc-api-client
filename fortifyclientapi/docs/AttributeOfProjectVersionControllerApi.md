# fortifyclientapi.sscclientapi.AttributeOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                         | HTTP request                                        | Description      |
| ---------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------- | ---------------- |
| [**create_attribute_of_project_version**](AttributeOfProjectVersionControllerApi.md#create_attribute_of_project_version)                       | **POST** /projectVersions/{parentId}/attributes     | create           |
| [**list_attribute_of_project_version**](AttributeOfProjectVersionControllerApi.md#list_attribute_of_project_version)                           | **GET** /projectVersions/{parentId}/attributes      | list             |
| [**read_attribute_of_project_version**](AttributeOfProjectVersionControllerApi.md#read_attribute_of_project_version)                           | **GET** /projectVersions/{parentId}/attributes/{id} | read             |
| [**update_collection_attribute_of_project_version**](AttributeOfProjectVersionControllerApi.md#update_collection_attribute_of_project_version) | **PUT** /projectVersions/{parentId}/attributes      | updateCollection |

# **create_attribute_of_project_version**
> ApiResultAttribute create_attribute_of_project_version(body, parent_id)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.Attribute() # Attribute | resource
parent_id = 789 # int | parentId

try:
    # create
    api_response = api_instance.create_attribute_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeOfProjectVersionControllerApi->create_attribute_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                          | Description | Notes |
| ------------- | ----------------------------- | ----------- | ----- |
| **body**      | [**Attribute**](Attribute.md) | resource    |
| **parent_id** | **int**                       | parentId    |

### Return type

[**ApiResultAttribute**](ApiResultAttribute.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_attribute_of_project_version**
> ApiResultListAttribute list_attribute_of_project_version(parent_id, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_attribute_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeOfProjectVersionControllerApi->list_attribute_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultListAttribute**](ApiResultListAttribute.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_attribute_of_project_version**
> ApiResultAttribute read_attribute_of_project_version(parent_id, id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
id = 789 # int | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_attribute_of_project_version(parent_id, id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeOfProjectVersionControllerApi->read_attribute_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **id**        | **int** | id            |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultAttribute**](ApiResultAttribute.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_collection_attribute_of_project_version**
> ApiResultListAttribute update_collection_attribute_of_project_version(body, parent_id)

updateCollection

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.AttributeOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [fortifyclientapi.sscclientapi.Attribute()] # list[Attribute] | data
parent_id = 789 # int | parentId

try:
    # updateCollection
    api_response = api_instance.update_collection_attribute_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AttributeOfProjectVersionControllerApi->update_collection_attribute_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                | Description | Notes |
| ------------- | ----------------------------------- | ----------- | ----- |
| **body**      | [**list[Attribute]**](Attribute.md) | data        |
| **parent_id** | **int**                             | parentId    |

### Return type

[**ApiResultListAttribute**](ApiResultListAttribute.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

