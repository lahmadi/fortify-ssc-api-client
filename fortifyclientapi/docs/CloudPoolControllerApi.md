# fortifyclientapi.sscclientapi.CloudPoolControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                               | HTTP request                  | Description |
| -------------------------------------------------------------------- | ----------------------------- | ----------- |
| [**create_cloud_pool**](CloudPoolControllerApi.md#create_cloud_pool) | **POST** /cloudpools          | create      |
| [**delete_cloud_pool**](CloudPoolControllerApi.md#delete_cloud_pool) | **DELETE** /cloudpools/{uuid} | delete      |
| [**list_cloud_pool**](CloudPoolControllerApi.md#list_cloud_pool)     | **GET** /cloudpools           | list        |
| [**read_cloud_pool**](CloudPoolControllerApi.md#read_cloud_pool)     | **GET** /cloudpools/{uuid}    | read        |
| [**update_cloud_pool**](CloudPoolControllerApi.md#update_cloud_pool) | **PUT** /cloudpools/{uuid}    | update      |

# **create_cloud_pool**
> ApiResultCloudPool create_cloud_pool(body)

create

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPool() # CloudPool | resource

try:
    # create
    api_response = api_instance.create_cloud_pool(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolControllerApi->create_cloud_pool: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**CloudPool**](CloudPool.md) | resource    |

### Return type

[**ApiResultCloudPool**](ApiResultCloudPool.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **delete_cloud_pool**
> ApiResultVoid delete_cloud_pool(uuid)

delete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
uuid = 'uuid_example' # str | uuid

try:
    # delete
    api_response = api_instance.delete_cloud_pool(uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolControllerApi->delete_cloud_pool: %s\n" % e)
```

### Parameters

| Name     | Type    | Description | Notes |
| -------- | ------- | ----------- | ----- |
| **uuid** | **str** | uuid        |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_cloud_pool**
> ApiResultListCloudPool list_cloud_pool(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search-spec of full text search query (see fulltextsearch parameter) (optional)
fulltextsearch = false # bool | If 'true', interpret 'q' parameter as full text search query, defaults to 'false' (optional) (default to false)
orderby = 'orderby_example' # str | Fields to order by (optional)

try:
    # list
    api_response = api_instance.list_cloud_pool(fields=fields, start=start, limit=limit, q=q, fulltextsearch=fulltextsearch, orderby=orderby)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolControllerApi->list_cloud_pool: %s\n" % e)
```

### Parameters

| Name               | Type     | Description                                                                                                     | Notes                         |
| ------------------ | -------- | --------------------------------------------------------------------------------------------------------------- | ----------------------------- |
| **fields**         | **str**  | Output fields                                                                                                   | [optional]                    |
| **start**          | **int**  | A start offset in object listing                                                                                | [optional] [default to 0]     |
| **limit**          | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied         | [optional] [default to 200]   |
| **q**              | **str**  | A search-spec of full text search query (see fulltextsearch parameter)                                          | [optional]                    |
| **fulltextsearch** | **bool** | If &#x27;true&#x27;, interpret &#x27;q&#x27; parameter as full text search query, defaults to &#x27;false&#x27; | [optional] [default to false] |
| **orderby**        | **str**  | Fields to order by                                                                                              | [optional]                    |

### Return type

[**ApiResultListCloudPool**](ApiResultListCloudPool.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_cloud_pool**
> ApiResultCloudPool read_cloud_pool(uuid, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
uuid = 'uuid_example' # str | uuid
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_cloud_pool(uuid, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolControllerApi->read_cloud_pool: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **uuid**   | **str** | uuid          |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultCloudPool**](ApiResultCloudPool.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_cloud_pool**
> ApiResultCloudPool update_cloud_pool(body, uuid)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.CloudPoolControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.CloudPool() # CloudPool | resource
uuid = 'uuid_example' # str | uuid

try:
    # update
    api_response = api_instance.update_cloud_pool(body, uuid)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling CloudPoolControllerApi->update_cloud_pool: %s\n" % e)
```

### Parameters

| Name     | Type                          | Description | Notes |
| -------- | ----------------------------- | ----------- | ----- |
| **body** | [**CloudPool**](CloudPool.md) | resource    |
| **uuid** | **str**                       | uuid        |

### Return type

[**ApiResultCloudPool**](ApiResultCloudPool.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

