# fortifyclientapi.sscclientapi.ProjectVersionOfAuthEntityControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                    | HTTP request                                                    | Description                                                               |
| ----------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- | ------------------------------------------------------------------------- |
| [**assign_project_version_of_auth_entity**](ProjectVersionOfAuthEntityControllerApi.md#assign_project_version_of_auth_entity)             | **POST** /authEntities/{parentId}/projectVersions/action/assign | Associate the specified application versions to the authentication entity |
| [**do_action_project_version_of_auth_entity**](ProjectVersionOfAuthEntityControllerApi.md#do_action_project_version_of_auth_entity)       | **POST** /authEntities/{parentId}/projectVersions/action        | doAction                                                                  |
| [**list_project_version_of_auth_entity**](ProjectVersionOfAuthEntityControllerApi.md#list_project_version_of_auth_entity)                 | **GET** /authEntities/{parentId}/projectVersions                | list                                                                      |
| [**multi_delete_project_version_of_auth_entity**](ProjectVersionOfAuthEntityControllerApi.md#multi_delete_project_version_of_auth_entity) | **DELETE** /authEntities/{parentId}/projectVersions             | multiDelete                                                               |
| [**update_project_version_of_auth_entity**](ProjectVersionOfAuthEntityControllerApi.md#update_project_version_of_auth_entity)             | **PUT** /authEntities/{parentId}/projectVersions                | update                                                                    |

# **assign_project_version_of_auth_entity**
> ApiResultVoid assign_project_version_of_auth_entity(body, parent_id)

Associate the specified application versions to the authentication entity

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfAuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ProjectVersionAuthEntityAssignRequest() # ProjectVersionAuthEntityAssignRequest | resource
parent_id = 789 # int | parentId

try:
    # Associate the specified application versions to the authentication entity
    api_response = api_instance.assign_project_version_of_auth_entity(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfAuthEntityControllerApi->assign_project_version_of_auth_entity: %s\n" % e)
```

### Parameters

| Name          | Type                                                                                  | Description | Notes |
| ------------- | ------------------------------------------------------------------------------------- | ----------- | ----- |
| **body**      | [**ProjectVersionAuthEntityAssignRequest**](ProjectVersionAuthEntityAssignRequest.md) | resource    |
| **parent_id** | **int**                                                                               | parentId    |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **do_action_project_version_of_auth_entity**
> ApiResultApiActionResponse do_action_project_version_of_auth_entity(body, parent_id)

doAction

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfAuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ApiCollectionActionlong() # ApiCollectionActionlong | collectionAction
parent_id = 789 # int | parentId

try:
    # doAction
    api_response = api_instance.do_action_project_version_of_auth_entity(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfAuthEntityControllerApi->do_action_project_version_of_auth_entity: %s\n" % e)
```

### Parameters

| Name          | Type                                                      | Description      | Notes |
| ------------- | --------------------------------------------------------- | ---------------- | ----- |
| **body**      | [**ApiCollectionActionlong**](ApiCollectionActionlong.md) | collectionAction |
| **parent_id** | **int**                                                   | parentId         |

### Return type

[**ApiResultApiActionResponse**](ApiResultApiActionResponse.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **list_project_version_of_auth_entity**
> ApiResultListProjectVersion list_project_version_of_auth_entity(parent_id, start=start, limit=limit, q=q, fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfAuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_project_version_of_auth_entity(parent_id, start=start, limit=limit, q=q, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfAuthEntityControllerApi->list_project_version_of_auth_entity: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                             | Notes                       |
| ------------- | ------- | ------------------------------------------------------------------------------------------------------- | --------------------------- |
| **parent_id** | **int** | parentId                                                                                                |
| **start**     | **int** | A start offset in object listing                                                                        | [optional] [default to 0]   |
| **limit**     | **int** | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200] |
| **q**         | **str** | A search query                                                                                          | [optional]                  |
| **fields**    | **str** | Output fields                                                                                           | [optional]                  |

### Return type

[**ApiResultListProjectVersion**](ApiResultListProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **multi_delete_project_version_of_auth_entity**
> ApiResultVoid multi_delete_project_version_of_auth_entity(parent_id, ids=ids)

multiDelete

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfAuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
ids = 'ids_example' # str | A comma-separated list of resource identifiers (optional)

try:
    # multiDelete
    api_response = api_instance.multi_delete_project_version_of_auth_entity(parent_id, ids=ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfAuthEntityControllerApi->multi_delete_project_version_of_auth_entity: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                    | Notes      |
| ------------- | ------- | ---------------------------------------------- | ---------- |
| **parent_id** | **int** | parentId                                       |
| **ids**       | **str** | A comma-separated list of resource identifiers | [optional] |

### Return type

[**ApiResultVoid**](ApiResultVoid.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_project_version_of_auth_entity**
> ApiResultListProjectVersion update_project_version_of_auth_entity(body, parent_id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ProjectVersionOfAuthEntityControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = [56] # list[int] | resources
parent_id = 789 # int | parentId

try:
    # update
    api_response = api_instance.update_project_version_of_auth_entity(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ProjectVersionOfAuthEntityControllerApi->update_project_version_of_auth_entity: %s\n" % e)
```

### Parameters

| Name          | Type                    | Description | Notes |
| ------------- | ----------------------- | ----------- | ----- |
| **body**      | [**list[int]**](int.md) | resources   |
| **parent_id** | **int**                 | parentId    |

### Return type

[**ApiResultListProjectVersion**](ApiResultListProjectVersion.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

