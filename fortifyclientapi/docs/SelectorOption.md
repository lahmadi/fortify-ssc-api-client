# SelectorOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | **str** | Option&#x27;s display name. | 
**guid** | **str** | Option&#x27;s unique GUID. | 
**value** | **str** | Option&#x27;s value that must be sent to backend when this option is selected. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

