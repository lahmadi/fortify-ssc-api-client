# fortifyclientapi.sscclientapi.UserIssueSearchOptionsOfProjectVersionControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                                                                                                                | HTTP request                                               | Description |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------- | ----------- |
| [**get_user_issue_search_options_of_project_version**](UserIssueSearchOptionsOfProjectVersionControllerApi.md#get_user_issue_search_options_of_project_version)       | **GET** /projectVersions/{parentId}/userIssueSearchOptions | get         |
| [**update_user_issue_search_options_of_project_version**](UserIssueSearchOptionsOfProjectVersionControllerApi.md#update_user_issue_search_options_of_project_version) | **PUT** /projectVersions/{parentId}/userIssueSearchOptions | update      |

# **get_user_issue_search_options_of_project_version**
> ApiResultUserIssueSearchOptions get_user_issue_search_options_of_project_version(parent_id, fields=fields)

get

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserIssueSearchOptionsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
parent_id = 789 # int | parentId
fields = 'fields_example' # str | Output fields (optional)

try:
    # get
    api_response = api_instance.get_user_issue_search_options_of_project_version(parent_id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserIssueSearchOptionsOfProjectVersionControllerApi->get_user_issue_search_options_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type    | Description   | Notes      |
| ------------- | ------- | ------------- | ---------- |
| **parent_id** | **int** | parentId      |
| **fields**    | **str** | Output fields | [optional] |

### Return type

[**ApiResultUserIssueSearchOptions**](ApiResultUserIssueSearchOptions.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **update_user_issue_search_options_of_project_version**
> ApiResultUserIssueSearchOptions update_user_issue_search_options_of_project_version(body, parent_id)

update

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.UserIssueSearchOptionsOfProjectVersionControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.UserIssueSearchOptions() # UserIssueSearchOptions | resource
parent_id = 789 # int | parentId

try:
    # update
    api_response = api_instance.update_user_issue_search_options_of_project_version(body, parent_id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling UserIssueSearchOptionsOfProjectVersionControllerApi->update_user_issue_search_options_of_project_version: %s\n" % e)
```

### Parameters

| Name          | Type                                                    | Description | Notes |
| ------------- | ------------------------------------------------------- | ----------- | ----- |
| **body**      | [**UserIssueSearchOptions**](UserIssueSearchOptions.md) | resource    |
| **parent_id** | **int**                                                 | parentId    |

### Return type

[**ApiResultUserIssueSearchOptions**](ApiResultUserIssueSearchOptions.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

