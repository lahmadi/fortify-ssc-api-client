# fortifyclientapi.sscclientapi.FeatureControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                   | HTTP request           | Description |
| -------------------------------------------------------- | ---------------------- | ----------- |
| [**list_feature**](FeatureControllerApi.md#list_feature) | **GET** /features      | list        |
| [**read_feature**](FeatureControllerApi.md#read_feature) | **GET** /features/{id} | read        |

# **list_feature**
> ApiResultListFeature list_feature(fields=fields, start=start, limit=limit, q=q, orderby=orderby, use_stable_api=use_stable_api)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FeatureControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)
start = 0 # int | A start offset in object listing (optional) (default to 0)
limit = 200 # int | A maximum number of returned objects in listing, if '-1' or '0' no limit is applied (optional) (default to 200)
q = 'q_example' # str | A search query (optional)
orderby = 'orderby_example' # str | Fields to order by (optional)
use_stable_api = true # bool | use_stable_api (optional) (default to true)

try:
    # list
    api_response = api_instance.list_feature(fields=fields, start=start, limit=limit, q=q, orderby=orderby, use_stable_api=use_stable_api)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FeatureControllerApi->list_feature: %s\n" % e)
```

### Parameters

| Name               | Type     | Description                                                                                             | Notes                        |
| ------------------ | -------- | ------------------------------------------------------------------------------------------------------- | ---------------------------- |
| **fields**         | **str**  | Output fields                                                                                           | [optional]                   |
| **start**          | **int**  | A start offset in object listing                                                                        | [optional] [default to 0]    |
| **limit**          | **int**  | A maximum number of returned objects in listing, if &#x27;-1&#x27; or &#x27;0&#x27; no limit is applied | [optional] [default to 200]  |
| **q**              | **str**  | A search query                                                                                          | [optional]                   |
| **orderby**        | **str**  | Fields to order by                                                                                      | [optional]                   |
| **use_stable_api** | **bool** | use_stable_api                                                                                          | [optional] [default to true] |

### Return type

[**ApiResultListFeature**](ApiResultListFeature.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

# **read_feature**
> ApiResultFeature read_feature(id, fields=fields)

read

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.FeatureControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
id = 'id_example' # str | id
fields = 'fields_example' # str | Output fields (optional)

try:
    # read
    api_response = api_instance.read_feature(id, fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling FeatureControllerApi->read_feature: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **id**     | **str** | id            |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultFeature**](ApiResultFeature.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

