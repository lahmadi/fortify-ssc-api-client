# IssueAssignment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**issue_count_all_project_ver** | **int** | total number of issues assigned to user across all application versions | 
**issue_count_certain_project_ver** | **int** | number of issues assigned to user in current application version | 
**project_version_id** | **int** |  | 
**user_name** | **str** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

