# CloudPool

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**child_of_global_pool** | **bool** |  | [optional] 
**description** | **str** | ScanCentral pool description | [optional] 
**is_deletable** | **bool** |  | [optional] 
**name** | **str** | ScanCentral pool name | 
**path** | **str** | ScanCentral pool path | [optional] 
**stats** | [**CloudPoolStats**](CloudPoolStats.md) |  | [optional] 
**uuid** | **str** | ScanCentral pool UUID | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

