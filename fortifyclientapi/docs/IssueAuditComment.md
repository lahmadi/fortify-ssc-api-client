# IssueAuditComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**audit_time** | **datetime** |  | 
**comment** | **str** |  | 
**issue_engine_type** | **str** |  | [optional] 
**issue_id** | **int** |  | 
**issue_instance_id** | **str** |  | [optional] 
**issue_name** | **str** |  | [optional] 
**project_name** | **str** |  | [optional] 
**project_version_id** | **int** |  | [optional] 
**project_version_name** | **str** |  | [optional] 
**seq_number** | **int** |  | 
**user_name** | **str** |  | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

