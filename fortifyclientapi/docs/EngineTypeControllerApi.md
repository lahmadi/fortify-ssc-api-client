# fortifyclientapi.sscclientapi.EngineTypeControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                              | HTTP request         | Description |
| ------------------------------------------------------------------- | -------------------- | ----------- |
| [**list_engine_type**](EngineTypeControllerApi.md#list_engine_type) | **GET** /engineTypes | list        |

# **list_engine_type**
> ApiResultListEngineType list_engine_type(fields=fields)

list

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.EngineTypeControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
fields = 'fields_example' # str | Output fields (optional)

try:
    # list
    api_response = api_instance.list_engine_type(fields=fields)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling EngineTypeControllerApi->list_engine_type: %s\n" % e)
```

### Parameters

| Name       | Type    | Description   | Notes      |
| ---------- | ------- | ------------- | ---------- |
| **fields** | **str** | Output fields | [optional] |

### Return type

[**ApiResultListEngineType**](ApiResultListEngineType.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

