# JobPriorityChangeCategoryWarning

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**warning_category** | **str** | Job priority change warning category. | 
**warning_list** | [**list[JobPriorityChangeInfo]**](JobPriorityChangeInfo.md) | Detailed information about job priority change. | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

