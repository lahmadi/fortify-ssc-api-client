# ApiBulkRequestItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**http_verb** | **str** |  | [optional] 
**post_data** | **object** |  | [optional] 
**uri** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

