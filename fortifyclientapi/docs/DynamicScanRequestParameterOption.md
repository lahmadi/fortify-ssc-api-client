# DynamicScanRequestParameterOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | [optional] 
**guid** | **str** | Unique string identifier for a Dynamic Scan Request Parameter Option | [optional] 
**id** | **int** | Unique ID for this parameter option | 
**index** | **int** | Index of this option in list of options | 
**name** | **str** | Name | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

