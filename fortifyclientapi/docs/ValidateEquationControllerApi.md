# fortifyclientapi.sscclientapi.ValidateEquationControllerApi

All URIs are relative to *//ssc.kube.agile4security.io/api/v1*

| Method                                                                            | HTTP request               | Description  |
| --------------------------------------------------------------------------------- | -------------------------- | ------------ |
| [**do_validate_equation**](ValidateEquationControllerApi.md#do_validate_equation) | **POST** /validateEquation | DoValidation |

# **do_validate_equation**
> ApiResultValidationStatus do_validate_equation(body)

DoValidation

### Example
```python
from __future__ import print_function
import time
import sscclientapi
from fortifyclientapi.sscclientapi.rest import ApiException
from pprint import pprint

# Configure API key authorization: FortifyToken
configuration = fortifyclientapi.sscclientapi.Configuration()
configuration.api_key['Authorization'] = 'YOUR_API_KEY'
# Uncomment below to setup prefix (e.g. Bearer) for API key, if needed
# configuration.api_key_prefix['Authorization'] = 'Bearer'

# create an instance of the API class
api_instance = fortifyclientapi.sscclientapi.ValidateEquationControllerApi(fortifyclientapi.sscclientapi.ApiClient(configuration))
body = fortifyclientapi.sscclientapi.ValidationRequest() # ValidationRequest | request

try:
    # DoValidation
    api_response = api_instance.do_validate_equation(body)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling ValidateEquationControllerApi->do_validate_equation: %s\n" % e)
```

### Parameters

| Name     | Type                                          | Description | Notes |
| -------- | --------------------------------------------- | ----------- | ----- |
| **body** | [**ValidationRequest**](ValidationRequest.md) | request     |

### Return type

[**ApiResultValidationStatus**](ApiResultValidationStatus.md)

### Authorization

[FortifyToken](../sscclientapi/README.md#FortifyToken)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to README]](../sscclientapi/README.md)

