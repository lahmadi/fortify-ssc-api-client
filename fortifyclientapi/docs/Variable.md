# Variable

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attribute_for_operation** | **str** |  | [optional] 
**description** | **str** |  | [optional] 
**folder_name** | **str** | Select a folder from the default filter set to associate with the variable. | 
**guid** | **str** |  | [optional] 
**id** | **int** |  | [optional] 
**in_use** | **bool** |  | [optional] 
**name** | **str** |  | 
**object_version** | **int** |  | [optional] 
**operation** | **str** |  | [optional] 
**publish_version** | **int** |  | [optional] 
**search_string** | **str** |  | 
**variable_type** | **str** |  | [optional] 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

