# AuthTokenRevokeRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**tokens** | **list[str]** | A list of tokens (atleast one) to revoke | 

[[Back to Model list]](../sscclientapi/README.md#documentation-for-models) [[Back to API list]](../sscclientapi/README.md#documentation-for-api-endpoints) [[Back to README]](../sscclientapi/README.md)

